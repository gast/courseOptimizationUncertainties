\input{latex_entete}

\title{Online Optimization and Bandits problems }%
\subtitle{A (short) introduction to reinforcement learning}
\author{Nicolas Gast}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}{Online Learning}
  Decisions are make sequentially but you can learn from your past
  decisions:
  \begin{align*}
    \text{Action} \leadsto \text{Observation} \leadsto \text{Action}
    \leadsto \text{Observation} \leadsto \dots 
  \end{align*}

  Up to now, we assumed that observations are random but that \alert{we know their probability distribution}. 
  \bigskip

  What if you do not? 
\end{frame}

% \begin{frame}{Outline}
%   \tableofcontents
% \end{frame}

\begin{frame}{Online learning is everywhere on the Internet}
  {example: maximize the click-through rate} \only<1>{
    Google search\\
    \includegraphics[width=.8\linewidth]{searchSki}\\
    
    \begin{itemize}
    \item How to decide what items to show? 
    \end{itemize}
  }
  \only<2->{
    From \url{http://www.lemonde.fr}:

    \begin{tabular}{@{}c@{}c@{}}
      \mpage{.4}{\includegraphics[width=\linewidth]{lemonde}}
      &
      \mpage{.5}{
        \only<3>{%
        \includegraphics[width=.8\linewidth]{lemonde_zoom}

        \begin{itemize}
        \item How to choose a good title? 
        \end{itemize}
        \vspace{2cm}
      }
    }
    \end{tabular}
  }
\end{frame}

\begin{frame}
  \frametitle{Online learning problems}

  \begin{align*}
    \text{Action} \leadsto \text{Observation} \leadsto \text{Action}
    \leadsto \text{Observation} \leadsto \dots 
  \end{align*}

  Optimize and learn at the same time.
  
\end{frame}
\begin{frame}
  \frametitle{Outline}

  \tableofcontents

\end{frame}

\section{The stochastic multi-armed bandit problem}

\subsection{Problem setup}

\begin{frame}{The Bernoulli multi-armed bandit}
  At each time step, you can pull an arm $A_t\in\{1\dots n\}$. 
  \begin{itemize}
    \item At time $t$, the arm $a$ gives you a reward $R_{t,a}$ that equals $1$ with
    probability $\mu_a$ and $0$ with probability $1-\mu_a$.
  \end{itemize}
  \bigskip

  We assume that:
  \begin{itemize}
  \item The rewards are all independent.
  \item The decision maker \alert{does not know} the $\mu_a$s. 
  \end{itemize}
  \bigskip
  
  % You would like to maximize:
  % \begin{align*}
  %   \esp{\sum_{t=1}^T R_{t,A_t}}.
  % \end{align*}
  There is a natural compromise between \textbf{exploration} and
  \textbf{exploitation}.
\end{frame}

\begin{frame}{Motivation}
  \begin{itemize}
  \item \textbf{Clinical trial}
    
    \begin{tabular}{cccc}
      $\mu_1$ & $\mu_2$ & $\mu_3$ & $\mu_4$\\
      
      \includegraphics[width=.16\linewidth]{drug1}
      &\includegraphics[width=.16\linewidth]{drug2}
      &\includegraphics[width=.16\linewidth]{drug3}
      &\includegraphics[width=.16\linewidth]{drug4}
    \end{tabular}
    \begin{itemize}
    \item Choose treatment $A_t$ for patient $t$
    \item Observe a response $X_t\in\{0,1\}$ with
      $P(X_t=1)=\mu_{A_t}$. 
    \item Goal: maximize the number of patients healed. 
    \end{itemize}\bigskip\pause
    
  \item \textbf{Online advertisement}, \emph{e.g.}, the choice of a
    title of a news article :
    
    \begin{tabular}{|c|c|}\hline
      Title
      & Click proba. \\\hline
      "Murder victim found in adult entertainment venue"
      & $\mu_1$ \\\hline
      "Headless Body found in Topless Bar"& $\mu_2$\\\hline
    \end{tabular}
    
    \begin{itemize}
    \item Choose which title $A_t$ to display to customer $t$
    \item Observe a response $X_t\in\{0,1\}$ (click or no click).
    \item Goal: maximize your number of clicks. 
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{Regret minimization}
  If you know the values of $\mu_a$s, you should pick $\argmax_{a}\mu_a$.

  We define the regret  of a sequence of action
  $\mathcal{A}=(A_1,A_2\dots)$ as
  \begin{align*}
    \text{Regret}(\mathcal{A},T)&=\max_{a} T\mu_a - \esp{\sum_{t=1}^T R_{t,A_t}}.
  \end{align*}\pause
  \bigskip

  Maximizing reward = minimizing regret.
  \begin{itemize}
  \item Goal : design strategies that have a small regret regardeless of $\mu$. 
  \end{itemize}
\end{frame}

\begin{frame}{Asymptotically optimal regret}
  % \begin{itemize}
  % \item
  A good policy has sub-linear regret:
  \begin{align*}
    \text{Regret}(\mathcal{A}, T) = o(T). 
  \end{align*}
  % \end{itemize}
  To achieve this, all the arms should be drawn infinitely
  often.\pause 
  
  \begin{theorem}[Lai and Robbins, 1985 (Asymptotically
      Efficient Adaptive Allocation Rules)]
    There exists a constant $c$ (that depend on $\mu$) such that any
    uniformly efficient\footnote{Meaning
      $\text{Regret}(\mathcal{I}, T)=o(T^\alpha)$ for all $\mu$ and $\alpha$.}
    strategy satisfies :
    \begin{align*}
      \text{Regret}(\mathcal{A}, T) \ge c \log T
    \end{align*}
  \end{theorem}
\end{frame}

\begin{frame}{Some ideas of policies}
  \begin{itemize}
  \item \textbf{Random} -- Draw each arm with probability $1/n$. 
    \begin{itemize}
    \item Exploration
    \end{itemize}\pause\bigskip
  \item \textbf{Greedy:} Always choose the empirical best arm:
    \begin{align*}
      A_{t+1} = \argmax_{a\in\{1\dots n\}} \hat{\mu}_a(t)
    \end{align*}
    \begin{itemize}
    \item Exploitation
    \end{itemize}
    \pause\bigskip
  \item \textbf{$\varepsilon$-greedy} : apply ``greedy'' with
    probability $1-\mathbf{\varepsilon}$ and ``random'' otherwise
    (each with probability $\varepsilon/n$)
    \begin{itemize}
    \item Exploration and exploitation. 
    \end{itemize}\pause\bigskip
  \end{itemize}
  \textbf{All have linear regrets}
\end{frame}

\begin{frame}{Analysis of the $\epsilon$-greedy policy ($\epsilon>0$)}
  \begin{itemize}
  \item As $\epsilon>0$, all arm will be chosen an infinite amount of
    time. 
  \item Hence, by the law of large number : $\hat{\mu}_a(t)$ converges
    to the true mean as $t\to\infty$. 
  \item Therefore, the asymptotic regret is 
    \begin{align*}
      \text{Regret}(\text{$\varepsilon$-greedy}, T)=T(\sum_{a=1}^n(\mu_*-\mu_a))
      \frac{\varepsilon}{n} + o(T) 
    \end{align*}
  \end{itemize}
\end{frame}

\begin{frame}{$\varepsilon$-greedy : Smaller or larger $\epsilon$ are
    not necessarily better}
  Consider 5 Bernoulli arms with success probabilities
  $\mu=[0.5,0.3,0.6,0.4,0.2]$.  The average reward as a function of
  time is :
  
  \includegraphics[width=.9\linewidth]{epsilon_greedy}
\end{frame}

\subsection{The UCB Algorithm}

\begin{frame}{(Remainders on) Concentration inequalities}
  For any random variable $X$: 
  \begin{itemize}
  \item Markov inequality : $\Proba{X\ge a}\le
    \frac1a\esp{|X|}$. \pause
  \item Chernov inequality:
    $\Proba{|X-\esp{X}|\ge a}\le \frac1{a^2}\var{X}$\pause \bigskip
    
    In particular, let $X_n$ be a sequence of \emph{i.i.d.} random
    variables and $S_n=\frac1n \sum_{k=1}^n X_k$:
    \begin{align*}
      \Proba{\abs{S_n-\esp{X_1}}\ge\epsilon}\le
      \frac{1}{n\epsilon^2}\var{X_1}. 
    \end{align*}\pause
  \item Hoeffding: if in addition $0\le X_n\le1$, then:
    \begin{align*}
      \Proba{S_n\ge \esp{X_1}+\epsilon}\le e^{-2n\epsilon^2}. 
    \end{align*}
  \end{itemize}
\end{frame}

\begin{frame}{UCB builds on Confidence Intervals}
  Consider a coin that gives ``Head'' with probability $\mu$.  Suppose
  that you draw a coin $N$ times and observe $K$ times ``head''. The
  natural estimator of $\mu$ is:
  \begin{align*}
    \hat{\mu} = \frac{K}{N}
  \end{align*}
  \pause
  Hoeffding inequality gives us 
  \begin{align*}
    \ProbaB{\mu \ge \hat{\mu} + \sqrt{\frac{\alpha}{2N}}} \le
    e^{-\alpha}
  \end{align*}
  The idea of UCB is to use the above bound with a growing $\alpha$. 
\end{frame}


\begin{frame}{The UCB algorithm}
  UCB computes a confidence bound $UCB_a(t)$ such that
  $\mu_a(t)\le UCB_a(t)$ with high probability.  Example : $UCB1$
  [Auer et al. 02] uses
  \begin{align*}
    UCB_a(t) = \hat{\mu}_a(t) + \sqrt{\frac{\alpha \log t}{2N_a(t)}}.
  \end{align*}
  
  \begin{itemize}
  \item Choose $A_{t+1}\in \argmax_{a\in\{1\dots n\}} UCB_a(t)$ (optimism principle).
  \end{itemize}
  
  \begin{center}
    \pdfmovie{simu/ucb.mp4}{6cm}{4cm}{\includegraphics[width=\linewidth]{ucb_image}}
  \end{center}
\end{frame}

\begin{frame}{Regret of UCB}
  \begin{theorem}
    The algorithm $UCB1$ has a logarithmic regret.  For all
    $\alpha>2$, there exists a constant $C_\alpha>0$ such that if $a$
    is a sub-optimal arm, then $N_a(T)$ (the number of time that this
    arm is chosen before $T$) satisfies
    \begin{align*}
      \esp{N_a(T)} \le \frac{2\alpha\log T}{(\mu_*-\mu_a)^2} +
      C_\alpha. 
    \end{align*}
  \end{theorem}
  As a consequence: $Regret_T(UCB, T)\le c\log(T)$. 
\end{frame}



\begin{frame}{Analysis of UCB (1/3)}
  \begin{tabular}{cc}
    \mpage{.6}{Using Hoeffding's inequality, one can show
    \begin{eqnarray*}
      \Proba{UCB_a(t)\le\mu_a}&\le& \frac{1}{t^{\alpha-1}}\\
      \Proba{LCB_a(t)\ge\mu_a}&\le& \frac{1}{t^{\alpha-1}},
    \end{eqnarray*}
                                    where
                                    $LCB_a(t) \le \hat{\mu}_a(t) - \sqrt{\frac{\alpha \log
                                    t}{2N_a(t)}}$.
                                    }
                              &\mpage{.4}{\includegraphics[width=\linewidth]{ucb_image}}
  \end{tabular}\bigskip
  
  \tiny
  Indeed\par 
  \begin{align*}
    \Proba{UCB_a(t)\le\mu_a}
    &\le \Proba{\exists s : \hat{\mu}_{a,s} +
      \frac{\alpha \log t}{2s}\le \mu_a}\\
    &\le \sum_{s=1}^t \Proba{\hat{\mu}_{a,s} +
      \frac{\alpha \log t}{2s}\le \mu_a}\\
    &\le \sum_{s=1}^t \frac{1}{t^{\alpha}}=\frac{1}{t^{\alpha-1}}. 
  \end{align*}
 
\end{frame}

\begin{frame}{Analysis of UCB (2/3)}
  Let us now consider arm $1$ is optimal and arm $2$ is not optimal
  ($\mu_2<\mu_1$). Let $N_2(t)$ be the number of times that arm $2$ is
  chosen between $0$ and $t-1$. Then for any stopping time $\tau$, we
  have:
  \begin{align*}
    &N_2(T)-N_2(\tau) = \sum_{t=\tau}^{T-1} \Ind{A_t=2}\\
    &=\sum_{t=\tau}^{T-1}  \Ind{A_t=2 \land UCB_1(t)\le \mu_1} +
      \Ind{A_t=2 \land UCB_1(t)>
      \mu_1}\\
    &\uncover<2->{=\sum_{t=\tau}^{T-1}  \Ind{A_t=2 \land UCB_1(t)\le \mu_1} +
      \Ind{A_t=2 \land UCB_2(t)>UCB_1(t)> \mu_1}}\\
    &\uncover<3->{\le \sum_{t=\tau}^{T-1}\Ind{UCB_1(t)\le \mu_1}+
      \Ind{UCB_2(t)>\mu_1}}\\
    &\uncover<4->{\le \sum_{t=\tau}^{T-1}\Ind{UCB_1(t)\le \mu_1}
      + \Ind{\mu_2<LCB_2(t)}
      + \Ind{ UCB_2(t)>\mu_1 > \mu_2>LCB_2(t)} }
  \end{align*}
\end{frame}

\begin{frame}{Analysis of UCB (3/3)}
  \begin{align*}
    \sum_{t=\tau}^{T-1}\underbrace{\Ind{UCB_1(t)\le \mu_1}
    + \Ind{\mu_2<LCB_2(t)}}_{\text{occurs with small probability (Hoeffding)}}
    + \underbrace{\Ind{ UCB_2(t)>\mu_1 >
    \mu_2>LCB_2(t)}}_{\text{possible only if $N_2$ is small}} 
  \end{align*}
  Indeed, for the last term:
  \begin{align*}
    \Ind{ LCB_2(t)<\mu_2 <\mu_1<UCB_2(t)}
    &\le\Ind{2\sqrt{\frac{\alpha\log
      t}{2N_2(t)}}>(\mu_1-\mu_2)}=\Ind{N_2(t) \le \frac{2\alpha\log
      T}{(\mu_1-\mu_2)^2}}
  \end{align*}\pause
  
  Let $x=\frac{2\alpha\log T}{(\mu_2-\mu_1)^2}$ and
  $X=\sum_{t=\tau}^{T-1}\Ind{UCB_1(t)\le \mu_1} +
  \Ind{\mu_2<LCB_2(t)}$. We have
  \begin{align*}
    N_2(T)-N_2(\tau) \le  X + \sum_{t=\tau}^T \Ind{N_2(t)\le x}. 
  \end{align*}
  where by Hoeffding's inequality,
  $\expect{X}\le2\sum_{t=1}^{\infty}1/t^{\alpha-1}=:C_\alpha$.\pause 

  By setting $\tau=\inf\{t>0 : N_2(t)\ge x\}$, this implies that:
  \begin{align*}
    \esp{N_2(T)} &\le \esp{N_2(\tau)}+\esp{X} \\
                 &\le C_\alpha + x. 
  \end{align*}
  \qed
\end{frame}


\begin{frame}{Numerical example (same parameter as before)}
  (Cumulative) regret as a function of the number of iterations
  ($\epsilon$-greedy has a linear but small regret)
  \includegraphics[width=.7\linewidth]{regret_comparison_UCB_epsilonGreedy.pdf}
\end{frame}

\subsection{Thomson Sampling}

\begin{frame}{Bayesian approach : Thomson sampling}
  
  \begin{exampleblock}{Algorithm ``Thomson sampling'' (1933)}
    \begin{itemize}
    \item For each arm $a$, you maintain a probability (posterior)
      distribution $\pi_a^t$ that represent your knowledge on $\mu_a$.
    \item At each time step, you draw $n$ independent samples
      $\theta_a\sim \pi^t_a$. You choose
      $A_{t+1} = \argmax_a \theta_a$.
    \item You update your knowledge $\pi^{t+1}_a$ by using Bayes rule.
    \end{itemize}
  \end{exampleblock}

  \begin{center}
    \pdfmovie{simu/thompson.mp4}{6cm}{4cm}{\includegraphics[width=\linewidth]{thompson_image}}
  \end{center}

\end{frame}

\begin{frame}
  \begin{theorem}[Kauffman,Korda,Munos 2012]
    Thompson Sampling is asymptotically
    optimal\footnote{\url{https://arxiv.org/pdf/1205.4217.pdf }},
    \emph{i.e.} it provides $O(\log T)$ regret with the smallest
    possible constant. 
  \end{theorem}
  It is one of the most efficient algorithm (note : the analysis is
  similar to the one of UCB1). 
\end{frame}

\begin{frame}{Bayesian update in more detail}
  Suppose that $X$ is a Bernoulli random variable with a random
  parameter $\mu$. Assume that you know $\Proba{\mu=\theta}$.
  
  \begin{itemize}
  \item If you draw one sample of $X$ and you observe the result, how
    does your knowledge on $\mu$ evolve?
  \end{itemize}\pause \bigskip
  
  The answer comes from Bayes rule : if you observe $X=1$, then : 
  \begin{align*}
    \Proba{\mu=\theta \mid X=1} &= \frac{\Proba{X=1 \mid \mu=\theta}
                                  \Proba{\mu=\theta}}{\Proba{X=1}}\\
                                &\propto \theta \Proba{\mu=\theta}
  \end{align*}
  Similarly, if you observe $X=0$, then :
  \begin{align*}
    \Proba{\mu=\theta \mid X=0} &= \frac{\Proba{X=1 \mid \mu=\theta}
                                  \Proba{\mu=\theta}}{\Proba{X=1}}\\
                                &\propto (1-\theta) \Proba{\mu=\theta}.
  \end{align*}
\end{frame}

\begin{frame}{Bayesian update}
  \textbf{Consequence} : Assume now that you start from a uniform
  prior on $\mu$ (\emph{i.e.}  you assume that $\mu$ is uniformly
  distributed on $[0,1]$ and that you draw $n$ samples of $X$.

  Then the distribution of $\mu$ conditioned on having observed $p$
  times the value $1$ is a beta distribution, with probability density
  function $f_{p,n-p}(.)$: 
  \begin{align*}
    f_{p,n-p}(\theta) = c\theta^p(1-\theta)^{n-p},
  \end{align*}
  where $c$ is a constant such that the probability sums to one. \bigskip

  % \begin{exampleblock}{Thompson algorithm rewritten}
  %   \begin{itemize}
  %   \item Let $P_a(t)$ (respectively $N_a(t)$) be the number of time
  %     that arm $i$ has been pulled with a positive (respectively
  %     zero) reward. 
  %   \item Draw $n$ random variables $\theta_1\dots\theta_n$ where
  %     $\theta_a\sim\text{Beta}(P_a(t),N_a(t))$. 
  %   \item Chooses the next arm to be $\argmax_a \theta_a$. 
  %   \end{itemize}
  % \end{exampleblock}
  \bigskip
  
  Note : can be easily adapted to more complicated prior. 
\end{frame}



\begin{frame}{Numerical example (same parameter as before)}
  (Cumulative) regret as a function of the number of iterations
  ($\epsilon$-greedy has a linear but small regret)
  \includegraphics[width=.95\linewidth]{regret_comparison_UCB_TS_epsilonGreedy}
\end{frame}

\section{Adversarial Bandits}

\begin{frame}{The Adversarial Bandit Model}
  At each time step you choose an action $A_t$ and obtain a reward
  $R_{t,A_t}$.  As before, you want to minimize the expected regret:
  \begin{align*}
    \max_{a\in\{1\dots n\}}\expect{\sum_{t=1}^TR_{t,a}} -
    \expect{\sum_{t=1}^T R_{t,A_t} }
  \end{align*}
  \bigskip
  
  Now, we assume that:
  \begin{itemize}
  \item As before, you do not know $R_{t,A_t}$.
  \item Contrary to before, the distribution of $R_{t,A_t}$ can change
    with time (and can depend on your past choices or rewards).
    \begin{itemize}
    \item \textbf{Adversarial case}
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{The Exp3 algorithm}

  \mpage{}{\begin{exampleblock}{}
    \uncover<2->{Start with $w_a(1)=1$.\bigskip}
    
    At each time step $t$:
    \begin{itemize}
    \item You choose an arm $A_t$ \uncover<2->{ such that
        $\Proba{A_t=i}=\frac{w_a(t)}{\sum_{j} w_j(t)}(1-\eta K)+\eta$}
    \item You observe a reward $R_{t,A_t}$
      
    \item \uncover<2->{Update $w_a(t+1)=w_a(t)e^{\eta \hat{R}_{t,A_t}}$,
        where
        \begin{align*}
          \hat{R}_{t,A_t}=\frac{R_{t,A_t}}{p_a(t)}
          \mathbf{1}_{\{a=A_t\}}. 
        \end{align*}
      }
    \end{itemize}
  \end{exampleblock}
}\bigskip
\pause

Remark: $\hat{R}_{t,A_t}$ is an \emph{unbiased} estimator of $R_{t,i}$
that can be computed with the information available at $t$. 
\end{frame}

\begin{frame}{Exp3: illustration}
  \includegraphics[width=\linewidth]{Exp3_illustration}
\end{frame}
\begin{frame}{Analysis of Exp3}
  \begin{theorem}
    For a fixed parameter $\eta$ and $K$ arms, Exp3 achieves a regret
    of:
    \begin{align*}
      \mathrm{Reg}(Exp3) \le 2K\eta T + \frac1{\eta} \log K
    \end{align*}
    In particular for $\eta=\sqrt{\log K / \eta T}$ this gives a
    square root regret: 
    \begin{align*}
      \mathrm{Reg}(Exp3) \le 3 \sqrt{TK\log K}. 
    \end{align*}
  \end{theorem}
  Note: one cannot do better than $O(\sqrt{T})$ regret.
\end{frame}

\begin{frame}{Analysis of Exp3 (1/3)}
  \label{slide:analysis1}
  Using that the $\hat{R_{t,a}}$ are unbiased, we show that
  $\esp{\log w_{a}(T+1)}$ is an unbiased estimator of the total reward
  of arm $a$:
  \begin{align*}
    \esp{\log w_{a}(T+1)}  =  \eta \esp{
    \sum_{t=1}^T \hat{R}_{a,t}} = \eta \sum_{t=1}^T \esp{R_{a,t}}.
  \end{align*}
  In particular, denoting $W(t)=\sum_a w_a(t)$ and using that
  $w_a(t)\ge0$, we get:
  \begin{align*}
    \esp{\log w(T+1)} \ge \max_a\esp{\log w_{a}(T+1)} = \eta \underbrace{\max_a
    \sum_{t=1}^T \esp{R_{a,t}}}_{\text{= Best static arm}}
  \end{align*}
\end{frame}

\begin{frame}{Analysis of Exp3 (2/3)}
  \begin{align*}
    \frac{W(t+1)}{W(t)}
    &= \sum_a\frac{w_a(t+1)}{W(t)}\\
    &= \sum_a \frac{p_a(t)-\eta}{1-\eta K} e^{\eta \hat{R_{a,t}}}\\
    &= \sum_a \frac{p_a(t)-\eta}{1-\eta K}( 1+\eta \hat{R}_{a,t}+ \eta^2
      (\hat{R}_{a,t})^2) & \text{\footnotesize Note:$e^x\le 1{+}x{+}x^2$ if $x\le1$}\\
    &\le1+\frac{\eta}{1-\eta K} R_{A_t,t} + \frac{\eta^2 K}{1-\eta K}
    & \text{ (in expectation)}
  \end{align*}
  The last line holds because $\sum_a p_a(t)=1$ and
  $\sum_ap_a(t)\hat{R}_{a,t}=R_{A_t,t}$.

  Using that $\log(1+a)\le a$, we have:
  \begin{align*}
    \log W_T(0) - \log W(0) &= \sum_{t=0}^{T-1}\log
                              \frac{W(t+1)}{W(t)} \le
                              \sum_t \frac{\eta}{1-\eta K} R_{A_t,t} + 
                              \sum_t \frac{\eta^2 K}{1-\eta K}\\
                            &= \frac1{1-\eta K}\mathrm{Exp3} +
                              \frac{\eta^2 KT}{1-\eta K}
  \end{align*}
\end{frame}
\begin{frame}{Analysis of Exp3 (3/3)}
  Using this and the result on Slide~\ref{slide:analysis1}, we have:
  \begin{align*}
    \mathrm{best arm} - \frac1{\eta}\log K
    & \le \frac1{\eta}\log W(T) - \frac1{\eta}\log W(0)\\
    &\le \frac{1}{1-\eta K} \mathrm{Exp3}
      + \frac{\eta}{1-\eta K} KT.
  \end{align*}

  Multiplying both sides by $1-\eta K$ and rearranging, this
  shows\footnote{One can replace $2$ by $(e-1)$ by using
    $e^x\le 1{+}x{+}(e-2)x^2$ in the previous slide.}
  \begin{align*}
    \mathrm{bestarm} - \mathrm{Exp3}
    &\le \eta K \mathrm{bestarm} +
      \frac{1-\eta K}{\eta}\log K + \eta K T (1-\eta K)\\
    &\le \eta K T+
      \frac1{\eta}\log K + \eta K T\\
    &\le 2\eta K T+ \frac1{\eta}\log K.
  \end{align*}
  The above bound is minimal when $\eta=\sqrt{\log K / KT}$.\bigskip

\end{frame}


\begin{frame}{Numerical example (same parameter as before)}
  {We look at a stochastic reward}
  (Cumulative) regret as a function of the number of iterations
  \includegraphics[width=.95\linewidth]{regret_comparison_UCB_TS_epsilonGreedy_exp3}
\end{frame}



\section{Reinforcement learning}

\begin{frame}
  \frametitle{Setup}

  You are given a MDP with expected reward $R(s,a)$ and transition matrices $P(s'\mid s,a)$. 

  \begin{itemize}
    \item $R$ and $P$ are unknown. 
    \item You can simulate the MDP and observe rewards and transitions. 
  \end{itemize}

  \begin{center}
    \includegraphics[width=.5\linewidth]{rl}
  \end{center}

  How to take good decisions? 

\end{frame}

\begin{frame}
  \frametitle{Toy example: the student MDP}

  \begin{center}
    \includegraphics[width=.65\linewidth]{student_mdp_unknown}  
  \end{center}
  
  \phantom{\footnote{\tiny Reference: \url{https://www.davidsilver.uk/wp-content/uploads/2020/03/MDP.pdf}}}
\end{frame}

\begin{frame}
  \frametitle{Full information problem}
  \begin{tabular}{cc}
    \mpage{.5}{
      \only<1>{\includegraphics[width=\linewidth]{student_mdp}}%
      \only<2>{\includegraphics[width=\linewidth]{student_mdp_optimal_values}}%
      \only<3>{\includegraphics[width=\linewidth]{student_mdp_unknown}}%
    }
    &\mpage{.45}{
      We consider that there exists an underlying model.

      \only<3>{\sout}{if you know all all parameter, you can compute an optimal policy.}
      \pause
      \pause{But what if
      \begin{itemize}
        \item \textbf{You do not know this model}?
        \item You can only simulate (or experiment) from this model.
      \end{itemize}
      }
    }
  \end{tabular}
  \begin{center}
  \end{center}
  \uncover<2>{
    Similar exploration / exploitation dilemma as in bandit. 
  }
\end{frame}


\begin{frame}
  \frametitle{Model free and model-based methods}

  There are (mainly) two types of methods: 
  \begin{itemize}
    \item Model-free: you directely learn the value function and policy ($Q(s,a)$ and/or $\pi(s)$). 
    \item Model-based: you learn $P$ and $R$. You use them to compute $\pi$.
  \end{itemize}

  \bigskip\bigskip

  In each case, you want to 
  \begin{itemize}
    \item Reinforce good decisions.
    \item Explore to see if there are better alternatives. 
  \end{itemize}
\end{frame}

\subsection{Monte Carlo methods}

\begin{frame}
  \frametitle{Outline}
  \tableofcontents[currentsubsection]
\end{frame}

\begin{frame}{Monte Carlo policy evaluation}
  \begin{itemize}
  \item Goal : evaluate $v_\pi(s)$. 
  \item Idea of Monte-Carlo algorithms : simulate a trajectory a
    compute the average returns. 
  \end{itemize}

  \begin{exampleblock}{General Monte Carlo policy evaluation}
    \begin{itemize}
    \item Reward(s) = $\emptyset$
    \item Repeat ``a large number of times'' N:
      \begin{itemize}
      \item Simulate a trajectory $S=S_1,S_2,S_3,\dots$ using $\pi$.
      \item For each $t$ such that $S_t=s$, computes $G_t$ and appends
        it to Reward(s).
      \end{itemize}
    \item Return Average(reward(s)).
    \end{itemize}
  \end{exampleblock}
\end{frame}

\begin{frame}{Monte Carlo optimization}
  \begin{center}
    \begin{tikzpicture}[scale=3]
      \node[circle,draw] at (0,0) (pi) {$\pi$};%
      \node[circle,draw] at (2,0) (Vpi) {$Q_\pi$};%
      
      \draw (pi) edge[->,bend left] node[above]{Evaluate $\pi$}
      node[below] {e.g., Monte Carlo}  (Vpi);% 
      
      \draw (Vpi) edge[->,bend left] node[above]{improve $\pi$}
      node[below] {e.g., policy improvement} (pi);%
    \end{tikzpicture}
  \end{center}

  General scheme :
  
  \begin{center}
    \mpage{.5}{%
      \begin{itemize}
      \item[Monte Carlo evaluation] evaluate $Q_\pi(s,a)$.
      \item[Policy improvement] greedy, $\varepsilon$-greedy, UCB or UCT.
      \end{itemize}      
    }
  \end{center}
  %\bigskip

  The trade-off between exploration and exploitation can be solved by using UCB-like algorithms. 
\end{frame}


\begin{frame}{Why are Monte Carlo methods useful?}
  \begin{itemize}
  \item Model-free : only needs access to a simulator 
  \item There is no need to explore the whole state space.
  \item It is exact as the number of samples $N$ goes to infinity.
  \item Can be used for non-Markovian settings
  \end{itemize}
  
  However,
  \begin{itemize}
  \item It is not exact and might miss some important states.
    \begin{itemize}
    \item \emph{e.g.}, rare events (plane crashes).
    \item Importance sampling might be needed. 
    \end{itemize}
  \item Can be very slow to converge.
  \item Depends on hyperparameters. 
  \end{itemize}
\end{frame}



% \begin{frame}{}
%   \begin{itemize}
%   \item Idea 1 : approximate policy evaluation
%   \item Idea 2 : iterate between evaluation and policy improvement 
%   \end{itemize}

%   Key advantages:
%   \begin{itemize}
%   \item Model free (only needs access to a simulator)
%   \item Can be used if problem is not Markovian 
%   \item Does not need to explore the whole state-space. 
%   \end{itemize}
% \end{frame}

% \subsection{TD learning and Q-learning}
% \begin{frame}{Outline}
%   \tableofcontents[currentsubsection]
% \end{frame}

% \begin{frame}{Policy evaluation}
%   We have seen two main algorithms to evaluate policies:\bigskip
  
%   \begin{tabular}{cl}
%     Dynamic programming
%     & $v(S_t) := \esp{R_{t+1} + \gamma v(S_{t+1})}$\\
%     Monte Carlo & $v(S_t) := v(S_t) + \alpha [G_t - v(S_t)]$\\
%     \uncover<2->{TD-learning & $v(S_t) := v(S_t) + \alpha [R_{t+1} + \gamma
%                                v(S_{t+1}) - v(S_t)]$}\\
%   \end{tabular}
%   \bigskip
  
%   \begin{itemize}
%   \item Dynamic programming requires \emph{a priori knowledge} of
%     $v(S_{t+1})$ and \emph{computes expectation}.
%   \item Monte Carlo uses \emph{samples} and needs to \emph{simulate
%       entire trajectories} to compute $G_t$. 
%   \item<2-> TD-learning uses \emph{a priori knowledge and sampling} to
%     update $v$.
    
%     \begin{align*}
%       \text{Idea} : v(S_t) = \esp{R_{t+1} + \gamma v(S_{t+1})}. 
%     \end{align*}
%   \end{itemize}
% \end{frame}

% \begin{frame}{Advantage of TD-learning}
%   \begin{itemize}
%   \item Only needs access to samples
%   \item Fully incremental compared to MC (you can learn without
%     knowing the final outcome)
%   \end{itemize}
% \end{frame}
\begin{frame}{Example of Monte-Carlo optimization: $Q$-learning}
  
  Recall that $Q(s,a) = R(s,a) + \gamma \max_a Q(s',a)P(s'\mid s,a)$. \pause

  \begin{exampleblock}{Q-learning algorithm}
    Initialize $Q(s,a)$ to any value and choose a starting state $S$.

    Repeat for $k=1, 2\dots$: 
    \begin{itemize}
    \item Choose $A$ from a policy derived from $Q$
      (e.g. $\varepsilon$-greedy).
    \item Take action $A$ and observe $S'$. 
    \item Update :
      \begin{align*}
        Q(S,A) = Q(S,A)(1-\alpha_k) + \alpha_k \left( R + \gamma \max_a Q(S',a) \right)
      \end{align*}
    \item $S := S'$. 
    \end{itemize}
  \end{exampleblock}
  where $\alpha_k$ is a decreasing sequence (e.g. $\alpha_k = O(1/k)$ or $O(1/\sqrt{k})$). 
\end{frame}

\newcommand\QSHOW[2]{
  \only<#1>{%
    Iteration $#2$\\%
    \begin{tikzpicture}%
      \node at (0,0)%
      {\includegraphics[width=\linewidth]{gridQlearning_#2}};%
      \node at (-3,-3.5) {Value function};%
      \node at (3,0) {\includegraphics[width=.5\linewidth]{gridQlearningVisitedStates_#2}};%
      \node at (3,-3.5) {Frequency of visit per state};%
    \end{tikzpicture}%
  }%
}
\begin{frame}{$Q$-learning : illustration}{On the same grid example
    with $\alpha=0.2$ and $\varepsilon=0.1$.}
  
  \QSHOW{2}{200}%
  \QSHOW{3}{1000}%
  \QSHOW{4}{5000}%
  \QSHOW{5}{10000}%
  \QSHOW{6}{20000}%
  \QSHOW{7}{30000}%
  \QSHOW{8}{40000}%
  \QSHOW{9}{50000}%
  \QSHOW{10}{60000}%
  \QSHOW{11}{70000}%
\end{frame}

\subsection{Model-based methods}
\begin{frame}
  \frametitle{Outline}
  \tableofcontents[currentsubsection]
\end{frame}

\begin{frame}
  \frametitle{Model based reinforcement learning}

  Idea:
  \begin{itemize}
    \item You collect observations of states and rewards. 
    \item You use it to compute estimates of $R(s,a)$ and of $P(s'\mid s,a)$
  \end{itemize}
  \bigskip\pause

  Naïve implementation: 
  \begin{itemize}
    \item you compute emprical means $\hat{R}(s,a)$ and $\hat{P}(s'\mid s,a)$ and compute the best policy corresponding to the MDP $\hat{P},\hat{R}$. 
  \end{itemize}
  
  What is the problem? \pause
  \begin{itemize}
    \item No exploration, pure exploitation. 
  \end{itemize}  
\end{frame}

\begin{frame}
  \frametitle{Possible solutions: $\epsilon$-greedy, UCRL2, PSRL}

  \begin{itemize}
    \item $\epsilon$-greedy : add exploration 
    \item UCRL2: based on optimism principle
    \item PSRL: Bayesian algorithm
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{UCRL2}  

  Compute emprical means $\hat{R}(s,a)$ and $\hat{P}(s'\mid s,a)$ by using past values. \bigskip

  For each pair $(s, a)$, you add a confidence bound of the forml $\delta(s,a) = C\sqrt{t / N_t(s,a)}$ where $N_t(s,a)$ is the number of time that you took action $a$ in state $s$ before time $t$:
  \begin{align*}
    \calR &= \{\text{vector }r \text{ such that for all $s,a$:} \abs{r(s,a)-\hat{r}(s,a)}\le \delta(s,a)\}\\
    \calP &= \{\text{transition matrix} P \text{ s.t. for all $s,a,a'$} \abs{P(s,a,a')-\hat{P}(s,a,a')}\le \delta(s,a)\}
  \end{align*}

  Optimism principle:

  \begin{itemize}
    \item  Apply $\pi$ that maximizes $V^\pi_{r,P\in\calR,\calP}$ (by using extended value iteration). 
  \end{itemize} 
  Re-update the policy periodically. 
\end{frame}

\begin{frame}
  \frametitle{PSRL (variation of Thompson sampling)}{PSRL stands for ``posterior sampling reinforcement learning''}

  Idea:
  \begin{itemize}
    \item Start with a prior distribution of $P$ and $R$.
    \item When you do an update, draw a \textbf{random} MDP $\tilde{R},\tilde{P}$ and compute the best policy $\tilde{\pi}$ for this MDP. 
    \item Apply $\tilde{\pi}$ to the "real" system. 
  \end{itemize}

  \pause\bigskip

  Example of prior: 
  \begin{itemize}
    \item Dirichlet for the transition matrix. 
    \item Normal-Gamma or Beta for the rewards.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Comparison of model-based and model-free methods}

  Model based methods
  \begin{itemize}
    \item perform usually better 
    \item have guaranteed regret
  \end{itemize}
  But:
  \begin{itemize}
    \item the problem needs to have a structure
    \item methods suffer from the curse of dimensionality. 
  \end{itemize}

\end{frame}
\subsection{Approximations and ``deep'' reinforcement learning}

\begin{frame}{Outline}
  \tableofcontents[currentsection,currentsubsection]
\end{frame}

\begin{frame}{Why do we need approximations?}
  \begin{center}
    \begin{tikzpicture}
      \uncover<1->{\node at (0,0)
        {\includegraphics[width=.2\linewidth]{tetris_init}};}
      \uncover<2->{\node at (5,0)
        {\includegraphics[width=.55\linewidth]{tetris}};}
    \end{tikzpicture}
    
    Tetris can be modeled as a MDP. 

    \tiny From
    \url{http://www.iecl.univ-lorraine.fr/~Bruno.Scherrer/hdr/hdr.pdf}
  \end{center}
  \pause 
  \begin{itemize}
  \item \alert{Problem : the number of configurations} is
    $\approx 2^{10\times 200}\approx 10^{60}$.
  \end{itemize}
\end{frame}

\begin{frame}{Solution : approximations}
  Bellman's equation $\displaystyle v = \max_{\pi} r_\pi + P_{\pi} v$
  tells you that the value function is sufficient to compute an
  optimal policy : 
  \begin{align*}
    q_*(s,a) &= r_a + P_a v\\
    \pi_*(s) &= \argmax Q(s,a)
  \end{align*}

  The general idea of \emph{approximate dynamic programming} is to
  solve Bellman equation approximately.

  For example, instead of learning the whole value function $q(s,a)$,
  one could assume that $q(s,a) = f(s,a,\Theta)$ where $f$ is a known
  function and $\Theta$ is a set of parameters. 
  
\end{frame}

\begin{frame}{There are many possible approximations}
  \begin{itemize}
  \item $f(s,a,.)$ could represent a \alert{linear regression} -- if
    the state is $x$ is a multidimensional vector, one could consider
    :
    \begin{align*}
      v_*(x) \approx \Theta^T x
    \end{align*}
    or a linear combinations of different features.
    \begin{align*}
      v_*(x) \approx \sum_{k=1}^K \theta_k f_k(x),
    \end{align*}
    where each $f_k$ is a predefined function
    \begin{itemize}
    \item For Tetris : $f_1(x)$ = shape, $f_2(x)$ = number of
      remaining lines, \dots
    \end{itemize}\pause 
    
  \item An other popular choice is to use \alert{neural networks} (see
    DQN and and DQN algorithms).
    
    \begin{tikzpicture}
      \node at (0,0){\includegraphics[width=.5\linewidth]{neuralN}};
      \node[fill=white] at (-3,0) {input:$(s,a)$};
      \node[fill=white] at (2.4,.7) {output:$v(s,a)$};
    \end{tikzpicture}
  \end{itemize}

  
  \bigskip
  
  
  {\small See also Bertsekas' book and
  \url{http://www.iecl.univ-lorraine.fr/~Bruno.Scherrer/hdr/hdr.pdf}}


\end{frame}




\section{Conclusion}


\begin{frame}{What did we learn?}

  \begin{itemize}
  \item In online learning, there is a tradeoff between exploration and
    exploitation.
  \item In the stochastic setting, there are two frameworks: optimism-based (UCB, UCRL) or Bayesian (Thompson sampling, PSRL). 
  \end{itemize}\bigskip 

  % To go further : 
  % \begin{itemize}
  % \item Contextual bandits
  % \item Adversarial bandits 
  % \item Markov bandits
  % \end{itemize}

  %  Multi-Armed bandit is a general framework with many variants
  % \begin{itemize}
  % \item Good example of exploration--exploitation tradeoff
  % \item Probabilistic model (Bayesian) or model-free
  % \item Many variants not covered : Adversarial, contextual,
  %   time-varying,...
  % \end{itemize}
  % \bigskip

  Multi-armed bandits have many variants and applications
  \begin{itemize}
  \item Online problems (e.g. contextual bandits)
  \item Reinforcement learning (e.g., UCT, DQN,...)
  \end{itemize}

  \bigskip
  \footnotesize 
  
  Some references: 
  
  \centering 
  \begin{tabular}{ccc}
    \includegraphics[width=.2\linewidth]{book_suttonBarto}
    &\includegraphics[width=.15\linewidth]{bandit_BCB}\\
    Stochastic bandits&Stochastic \& Adversarial
    (Chapter 1)
  \end{tabular}  
\end{frame}

\end{document}

% \begin{frame}{Going further}
%   \begin{itemize}
%   \item Markovian bandit problem
%     \begin{itemize}
%     \item Index policies (Gittins and Whittle) 
%     \end{itemize}
%   \item Combinatorial bandits
%     \begin{itemize}
%     \item UCB or Exp3-like algorithm.
%     \end{itemize}
%   \item Continuous actions
%     \begin{itemize}
%     \item Online convex optimization 
%     \end{itemize}
%   \item Mixing bandit problem and Markov decision processes 
%     \begin{itemize}
%     \item Reinforcement learning
%     \end{itemize}
%   \end{itemize}

  
%   \bigskip
%   Some references: 
  
%   \centering 
%   \begin{tabular}{ccc}
%     \includegraphics[width=.3\linewidth]{book_suttonBarto}
%     &\includegraphics[width=.2\linewidth]{bandit_BCB}\\
%     Stochastic bandits&Stochastic \& Adversarial
%     (Chapter 1)
%   \end{tabular}  
% \end{frame}