\documentclass{article}

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{tikz}
\usepackage[margin=1.1cm]{geometry}
\usepackage{graphicx}
\usepackage[T1]{fontenc}

\usepackage{pgffor}
\newcommand\resp[1]{
  ~\\[-10pt]\par\nobreak
  \noindent
  \foreach \n in {1,...,#1}{\makebox[\linewidth]{\dotfill}\\[10pt]}%
}%

\newcommand\queue[4]{
  % Adapted from
  % http://tex.stackexchange.com/questions/119597/how-can-i-create-a-queuing-node-using-tikz 
  % the rectangle with vertical rules
  \draw (#1,#2) -- ++(2cm,0) -- ++(0,-1.5cm) -- ++(-2cm,0);
  \foreach \i in {1,...,4} {\draw (#1+2-\i*0.3,#2) -- +(0,-1.5cm);}
  
  % the circle
  \draw (#1+2.75,#2-0.75) circle [radius=0.75cm];
  
  % the arrows and labels
  \draw[->] (#1+3.5,#2-0.75) -- +(20pt,0);
  \draw[->] (#1-1,#2-.75) node[left] {$#3$} -- +(20pt,0);
  \node at (#1+2.75,#2-0.75) {$#4$};
}


\title{Optimization under uncertainty} %
\author{Nicolas Gast \and Vincent Jost} %
\date{January 26, 2018} 

\begin{document}

%\maketitle 

\begin{center}
  \Large Master ORCO -- Optimization under uncertainty -- January 26, 2018
\end{center}
% {\noindent\large Firstname : \dots\dots\dots\dots\dots\dots\\
% Lastname : \dots\dots\dots\dots\dots\dots}

Use \textbf{two separates sheets} for ``Exercises 1 and 2'' and
``Exercises 3 and 4''.

\bigskip

Please be \textbf{concise} and \textbf{precise}.  Only document
allowed : 1 A4 page (1 sided, not recto-verso), hand
written. Electronic devices are not allowed.

Duration : 3h -- the notation scale is given as an indication. 

\noindent\makebox[\linewidth]{\rule{\paperwidth}{0.4pt}}

\section{Queuing With Loop (30 min, 4 points)}

Consider the following queuing model. Jobs arrive according to a
Poisson process with intensity $\lambda$ to be served at a server. The
service time is exponentially distributed of mean $1/\mu_1$. We
consider that with probability $p$, the service of a job is successful
in which case the job goes to the second server. Otherwise (with
probability $1-p$) the job has to be reprocessed (in which case the
service time is again exponentially distributed of mean $1/\mu_1$,
independently of the previous one). The execution of a job can fail
multiple times (the successive success probability are independent). 

This situation is represented on the following picture.
  
\begin{center}
  \begin{tikzpicture}
    \queue{0}{0}{\lambda}{\mu_1}%
    \draw (4.25,-.75) edge[-] node[above]{$p$} (5,-.75); %
    \draw (4.25,-.75) edge node[right]{$1-p$} (4.25,-2) (4.25,-2) edge
    (-1,-2) (-1,-2) edge (-1,-1) (-1,-1) edge[->] (0,-1);%
    \queue{6}{0}{}{\mu_2}%
  \end{tikzpicture}
\end{center}


\begin{enumerate}
\item What are the stability conditions of the system? 
\item Let $X_1(t)$ and $X_2(t)$ be the number of jobs in
  (respectively) the queue $1$ and the queue $2$.
  \begin{enumerate}
  \item Explain why $X_1$ is a continuous time Markov chain and
    describe its generator (for example by drawing a transition
    graph).
  \item Explain why $X_2$ is not a Markov chain. 
  \end{enumerate}
\item Compute the stationary measure of $X_1$ as a function of the
  parameters $\lambda$, $p$ and $\mu_1$. 
\item Explain why $(X_1,X_2)$ is a Markov chain and compute its
  stationary distribution.
\item Numerical application.
  \begin{enumerate}
  \item[(a)] Let $p=1/2$, $\lambda=2$, $\mu_1=3$ and $\mu_2=3$. Is the
    system stable? If yes, compute the mean sojourn time of a job in
    the system (in steady-state).
  \item[(b)] Same question with $p=2/3$, $\lambda=2$, $\mu_1=3$ and
    $\mu_2=3$.
  \end{enumerate}
\end{enumerate}

\section{Optimal Gambling (1h30, 8 points)}

You borrowed some money from a terrible gangster and you need to repay
$10 000$ euros to him by tomorrow morning. Since you only have $4000$
euros in your pocket, you decide to play at the Casino.

Let $X_t$ be the money that you have in your pocket after $t$ bets.
Each time, you can bet a quantity of money $u\le X_t$. If you bet a
quantity $u$ :
\begin{itemize}
\item with probability $p\in(0,1)$, you win $u$ : $X_{t+1}=X_t+u$
\item with probability $1-p$, you loose $u$: $X_{t+1}=X_t-u$. 
\end{itemize}
You can bet as many time as you need as long as $X_t>0$. The Casino
only allows you to bet multiple of $1000$ euros.  You want to maximize
the probability of being able to reimburse the $10000$ euros.

\begin{enumerate}
\item Compute the optimal strategy and compute your probability of reaching $10 000$ (as a function of $p$). Comment your result. 
  (\textbf{Hint 1} : formulate the problem as a Markov decision
  process and use value iteration to give you an intuition of the
  optimal policy.  \textbf{Hint 2} : the optimal strategy depends on $p$, so try to find
  the intervals of value of $p$ for which the optimal strategy is constant.  
  \textbf{Hint 3} : compute the value function of the policy suggested by your intuition and show that no policy can do
  better. \textbf{Hint 4} : if you do not know how to solve the question on paper, at least propose an algorithm that would solve the problem for you.).
\end{enumerate}

\section{On-line Algorithm (30min, 4 points)}

The co-chromatic number $\bar{\chi}(G)$ of a graph $G$ is the minimum
number of cliques needed to partition the vertices of the graph.
(said otherwise, if $\bar{G}$ denotes the complement of the graph $G$, then $\bar{\chi}(G)$ is the chromatic number of $\bar{G}$).

Consider the greedy algorithm for partitioning a graph into cliques. Given a graph $G$ and an order on the vertices of $G$:
\begin{itemize}
\item Assign label $1$ to the first vertex
\item For each vertex $v$: Assign to $v$ the smallest label $k$ such
  that $v$ is adjacent to all vertices already labeled $k$.
\end{itemize}
Note : during this algorithm, all the vertices that have the same label form a clique. 
The algorithm builds the cliques by trying to assign each vertex to an already existing clique.

The co-Grundy number $\bar{\Gamma}(G)$ of a graph $G$ is the maximum  
(over all possible order on the vertices) number of labels that the greedy algorithm can use on graph $G$.


\begin{enumerate}

\item Prove that the co-chromatic of the graph below is exactly $4$. 
\item Show that the co-Grundy number of the graph below is at least $7$.

  \begin{center}
    \begin{tikzpicture}[scale=.7,auto=left,every
      node/.style={draw,circle,inner sep=0.35cm}]
      
      \node (n1) at (1,0) {};%
      \node (n2) at (3,0) {};%
      \node (n3) at (5,0) {};%
      \node (n4) at (7,0) {};%
      \node (n5) at (9,0) {};%
      \node (n6) at (11,0) {};%
      \node (n7) at (13,0) {};%
      \node (n8) at (15,0) {};%
      \node (n9) at (2,1.5) {};%
      \node (n10) at (6,1.5) {};%
      \node (n11) at (10,1.5) {};%
      \node (n12) at (14,1.5) {};%
  

      \foreach \from/\to in
      {n1/n2,n2/n3,n3/n4,n4/n5,n5/n6,n6/n7,n7/n8,n1/n9,n2/n9,n3/n10,n4/n10,n5/n11,n6/n11,n7/n12,n8/n12}
      \draw (\from) -- (\to);%
    \end{tikzpicture}
  \end{center}

  Remind that, given a set of intervals of the real line, the
  associated interval graph has one vertex for each interval. Two
  intervals are linked by an edge if they intersect. An \emph{interval
    graph} is a graph that is associated to a set of intervals.



\item  Draw a set of intervals whose associated interval graph is the above graph.

\item Show that, for any $k\geq 1$ there is an interval graph with $\bar{\chi}(G)=k$ and $\bar{\Gamma}(G)\ge2k-1$.
\end{enumerate}





\section{Blood Testing (30min, 4 points)}

You are in charge of public health in a country and you have to find
out, for each individual in a large population of $N=1,000,000$
persons whether the person has a precise disease $D$. You can perform
blood tests on one or multiple person at the same time. If you perform
the test on a single person, the tell is positive if this person has
the disease $D$. If you mix the blood of multiple persons and do the
blood test, the test will be positive if any of these persons has the
disease $D$.

We assume that the probability that each person has the disease $D$ is $p \in [0,1]$ and that all events are independent.

\begin{enumerate}
\item What is the entropy of the $D$-status of the whole population of the $N$ persons ?

 \item Assume that $p=0.01$. The naive strategy is to test each person individually, which requires $N=1.000.000$ tests.
  \begin{enumerate}
  \item Using the previous question, find a lower bound on the expected number of blood tests required to test the whole population.
  \item Find a strategy that requires significantly less than $N$
    tests in expectation. Provide an estimation of how many tests your
    strategy requires in expectation. 
  \end{enumerate}

\end{enumerate}
















\end{document}
