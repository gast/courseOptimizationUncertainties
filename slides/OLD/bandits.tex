\input{latex_entete}

\title{Master ORCO : Optimization under uncertainty : Bandits problems}%
\author{Nicolas Gast}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

% \begin{frame}{Outline}
%   \tableofcontents
% \end{frame}

\begin{frame}{References}
  \centering 
  \begin{tabular}{ccc}
    \includegraphics[width=.3\linewidth]{book_suttonBarto}
    &\includegraphics[width=.3\linewidth]{bandit_BCB}
    &\includegraphics[width=.26\linewidth]{book_bandits}\\
    Stochastic bandit
    &Stochastic \& Adversarial
    &Markov bandits\\
    (Chapter 1)&(Technical)&(index policies)
  \end{tabular}

  + many recent research papers. 
\end{frame}

\begin{frame}{The Multi-Armed Bandit Problem}

  \begin{center}
    \includegraphics[width=.5\linewidth]{Las_Vegas_slot_machines.jpg}
  \end{center}
  
  A decision-maker chooses one of $n$ actions (=``arms'') at each time
  step.
  \begin{itemize}
  \item Each arm produces random payoff from a (known or unknown)
    distribution.
  \end{itemize}
  
  The goal is to maximize expected payoff (discounted or not). 
\end{frame}

\begin{frame}{MAB are everywhere}

  \begin{itemize}
  \item Historically : sequential clinical trials (1933)\bigskip
    
  \item Now on the web :
    \begin{itemize}
    \item Ad placement
    \item Price experimentation 
    \item Search
    \end{itemize}\bigskip
    
  \item Other examples:
    \begin{itemize}
    \item Research project allocation
    \end{itemize}

  \end{itemize}
\end{frame}

\begin{frame}{General framework}
  At each time, you can pull an arm $I_t\in\{1\dots n\}$. This leads
  to a reward $R_{I_t}$. Your objective is to maximize
  \begin{align*}
    \esp{\sum_{t=0}^T \gamma^t R_{I_t}}.
  \end{align*}
  
  We can consider different settings :
  \begin{itemize}
  \item I.i.d stochastic bandit : for each arm $i$, $R_i\sim p_i$
    (i.i.d).
  \item Markovian bandit : the reward of arm $i$ depends on an
    internal state of this arm $S_i$ for which you know the
    probability evolution of $S_i$.
  \item Adversarial bandits : the reward is chosen by an adversary. 
  \end{itemize}
  (or any mix of the above : contextual bandit, time-varying
  bandits,...)
\end{frame}

\begin{frame}{Table of Contents}
  \tableofcontents
\end{frame}

\section{Stochastic Bandits}

\begin{frame}{The Bernoulli multi-armed bandit}
  The reward of arm $i$ is \emph{i.i.d.} but with an \emph{unknown}
  distribution. We will mostly assume that there is no discount
  ($\gamma=1$) and we will focus on Bernoulli bandits. \bigskip
  
  You would like to maximize:
  \begin{align*}
    \esp{\sum_{t=0}^T R_{I_t}}.
  \end{align*}
  \begin{itemize}
  \item There is a natural compromise between \textbf{exploration} and
    \textbf{exploitation}.
  \end{itemize}
\end{frame}

\begin{frame}{Motivation}
  \begin{itemize}
  \item \textbf{Clinical trial}
    
    \begin{tabular}{cccc}
      $\mu_1$ & $\mu_2$ & $\mu_3$ & $\mu_4$\\
      
      \includegraphics[width=.16\linewidth]{drug1}
      &\includegraphics[width=.16\linewidth]{drug2}
      &\includegraphics[width=.16\linewidth]{drug3}
      &\includegraphics[width=.16\linewidth]{drug4}
    \end{tabular}
    \begin{itemize}
    \item Choose treatment $A_t$ for patient $t$
    \item Observe a response $X_t\in\{0,1\}$ with
      $P(X_t=1)=\mu_{A_t}$. 
    \item Goal: maximize the number of patients healed. 
    \end{itemize}\bigskip\pause
    
  \item \textbf{Online advertisement}, \emph{e.g.}, the choice of a
    title of a news article :
    
    \begin{tabular}{|c|c|}\hline
      Title
      & Click proba. \\\hline
      "Murder victim found in adult entertainment venue"
      & $\mu_1$ \\\hline
      "Headless Body found in Topless Bar"& $\mu_2$\\\hline
    \end{tabular}
    
    \begin{itemize}
    \item Choose which title $A_t$ to display to customer $t$
    \item Observe a response $X_t\in\{0,1\}$ (click or no click).
    \item Goal: maximize your number of clicks. 
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{Regret minimization}
  We define the regret of a sequence of action $\calA$ as 
  \begin{align*}
    \text{Regret}(\calA)&=\max_{i}\esp{\sum_{t=0}^T R_i} - \esp{\sum_{t=0}^T R_{I_t}}
                           \\
                         &\uncover<2->{=\esp{r_* T - \sum_{t=0}^T R_{I_t}},}
  \end{align*}\pause
  where $r_*=\max_i \esp{R_i}$. 
  
  Maximizing reward = minimizing regret.
  \bigskip
  
  \begin{itemize}
  \item Goal : design strategies that have a small regret for all
    distribution $\mu$. 
  \end{itemize}
\end{frame}

\begin{frame}{Asymptotically optimal regret}
  \begin{itemize}
  \item A good policy has sub-linear regret:
    \begin{align*}
      \text{Regret}(\calA) = o(T). 
    \end{align*}
  \end{itemize}
  To achieve this, all the arms should be drawn infinitely
  often.\pause 
  
  \begin{theorem}[Lai and Robbins, 1985 (Asymptotically
      Efficient Adaptive Allocation Rules)]
    There exists a constant $c$ (that depend on $\mu$) such that any
    uniformly efficient\footnote{Meaning
      $\text{Regret}(\calA)=o(T^\alpha)$ for all $\mu$ and $\alpha$.}
    strategy satisfies :
    \begin{align*}
      \text{Regret}(\calA) \ge c \log T
    \end{align*}
  \end{theorem}
\end{frame}

\begin{frame}{Some ideas of policies}
  \begin{itemize}
  \item \textbf{Random} -- Draw each arm with probability $1/n$. 
    \begin{itemize}
    \item Exploration
    \end{itemize}\pause\bigskip
  \item \textbf{Greedy:} Always choose the empirical best arm:
    \begin{align*}
      A_{t+1} = \argmax_a \hat{\mu}_a(t)
    \end{align*}
    \begin{itemize}
    \item Exploitation
    \end{itemize}
    \pause\bigskip
  \item \textbf{$\varepsilon$-greedy} : apply ``greedy'' with
    probability $1-\mathbf{\varepsilon}$ and ``random'' otherwise
    (each with probability $\varepsilon/n$)
    \begin{itemize}
    \item Exploration and exploitation. 
    \end{itemize}\pause\bigskip
  \end{itemize}
  \textbf{All have linear regrets}
\end{frame}

\begin{frame}{Analysis of the $\epsilon$-greedy policy ($\epsilon>0$)}
  \begin{itemize}
  \item As $\epsilon>0$, all arm will be chosen an infinite amount of
    time. 
  \item Hence, by the law of large number : $\hat{\mu}_a(t)$ converges
    to the true mean as $t\to\infty$. 
  \item Therefore, the asymptotic regret is 
    \begin{align*}
      \text{Regret}(\text{$\varepsilon$-greedy})=T(\sum_{a}(\mu_*-\mu_a))
      \frac{\varepsilon}{n} + o(T) 
    \end{align*}
  \end{itemize}
\end{frame}

\begin{frame}{$\varepsilon$-greedy : Smaller or larger $\epsilon$ are
    not necessarily better}
  Consider two Bernoulli arms with success probabilities $\mu_0=0.5$
  and $\mu_1=0.3$.  The average reward as a function of time is :
  
  \includegraphics[width=.9\linewidth]{../simulations/epsilon_greedy}
\end{frame}

\begin{frame}{The UCB algorithm}
  UCB computes a confidence bound $UCB_a(t)$ such that
  $\mu_a(t)\le UCB_a(t)$ with high probability.  Example : $UCB1$
  [Auer et al. 02] uses
  \begin{align*}
    UCB_a(t) = \hat{\mu}_a(t) + \sqrt{\frac{\alpha \log t}{2N_a(t)}}.
  \end{align*}
  
  \begin{itemize}
  \item Choose $A_{t+1}\in UCB_a(t)$ (optimism principle). 
  \end{itemize}
 
  \pause

  \begin{theorem}
    The algorithm $UCB1$ has a logarithmic regret.  For all
    $\alpha>2$, there exists a constant $C_\alpha>0$ such that if $a$
    is a sub-optimal arm, then $N_a(T)$ (the number of time that this
    arm is chosen before $T$) satisfies
    \begin{align*}
      \esp{N_a(T)} \le \frac{2\alpha\log T}{(\mu_*-\mu_a)^2} +
      C_\alpha. 
    \end{align*}
  \end{theorem}
\end{frame}

\begin{frame}{Analysis of UCB (1/3)}
  Hoeffding inequality : Let $\hat{\mu}_{a,s}$ be the mean of $n$
  \emph{i.i.d.} Bernoulli random variables of parameter $\mu_a$. Then
  for $\varepsilon\ge0$:
  \begin{align*}
    \Proba{ \hat{\mu}_{a,s}-\mu_a &< - \epsilon)} \le \exp(-2\epsilon^2s)\\
    \Proba{ \hat{\mu}_{a,s}-\mu_a &> \epsilon} \le \exp(-2\epsilon^2s)
  \end{align*}\pause
  
  As a consequence, $UCB_a(t)$ is a reasonable upper bound for
  $\mu_a$:
  \begin{align*}
    \Proba{UCB_a(t)\le\mu_a}
    &\le \Proba{\exists s : \hat{\mu}_{a,s} +
      \frac{\alpha \log t}{2s}\le \mu_a}\\
    &\le \sum_{s=1}^t \Proba{\hat{\mu}_{a,s} +
      \frac{\alpha \log t}{2s}\le \mu_a}\\
    &\le \sum_{s=1}^t \frac{1}{t^{\alpha}}=\frac{1}{t^{\alpha-1}}. 
  \end{align*}
  Similarly $\Proba{LCB_a(t)\ge\mu_a}\le 1/t^{\alpha-1}$ where
  $LCB_a(t) \le \hat{\mu}_a(t) - \sqrt{\frac{\alpha \log t}{2N_a(t)}}$.
\end{frame}

\begin{frame}{Analysis of UCB (2/3)}
  Let us now consider arm $1$ is optimal and arm $2$ is not optimal
  ($\mu_2<\mu_1$). Let $N_2(t)$ be the number of times that arm $2$ is
  chosen between $0$ and $t-1$. Then for any stopping time $\tau$, we
  have:
  \begin{align*}
    &N_2(T)-N_2(\tau) = \sum_{t=\tau}^{T-1} \Ind{A_t=2}\\
    &=\sum_{t=\tau}^{T-1}  \Ind{A_t=2 \land UCB_1(t)\le \mu_1} +
      \Ind{A_t=2 \land UCB_1(t)>
      \mu_1}\\
    &\uncover<2->{=\sum_{t=\tau}^{T-1}  \Ind{A_t=2 \land UCB_1(t)\le \mu_1} +
      \Ind{A_t=2 \land UCB_2(t)>UCB_1(t)> \mu_1}}\\
    &\uncover<3->{\le \sum_{t=\tau}^{T-1}\Ind{UCB_1(t)\le \mu_1}+
      \Ind{UCB_2(t)>\mu_1}}\\
    &\uncover<4->{\le \sum_{t=\tau}^{T-1}\Ind{UCB_1(t)\le \mu_1}
      + \Ind{\mu_2<LCB_2(t)}
      + \Ind{ UCB_2(t)>\mu_1 > \mu_2>LCB_2(t)} }
  \end{align*}
\end{frame}

\begin{frame}{Analysis of UCB (3/3)}
  Let us study the last term: 
  \begin{align*}
    \Ind{ LCB_2(t)<\mu_2 <\mu_1<UCB_2(t)}
    &\le\Ind{2\sqrt{\frac{\alpha\log
      t}{2N_2(t)}}>(\mu_1-\mu_2)}=\Ind{N_2(t) \le \frac{2\alpha\log
      T}{(\mu_1-\mu_2)^2}}
  \end{align*}\pause
  
  Let $x=\frac{2\alpha\log T}{(\mu_2-\mu_1)^2}$ and
  $X=\sum_{t=\tau}^{T-1}\Ind{UCB_1(t)\le \mu_1} +
  \Ind{\mu_2<LCB_2(t)}$. We have
  \begin{align*}
    N_2(T)-N_2(\tau) \le  X + \sum_{t=\tau}^T \Ind{N_2(t)\le x}. 
  \end{align*}
  where by Hoeffding's inequality, the expectation of $X$ is smaller
  than $2\sum_{t=1}^{\infty}1/t^{\alpha-1}=:C_\alpha$.\pause

  By setting $\tau=\inf\{t>0 : N_2(t)\ge x\}$, this implies that:
  \begin{align*}
    \esp{N_2(T)} &\le \esp{N_2(\tau)}+\esp{X} \\
                 &\le C_\alpha + x. 
  \end{align*}
  \qed
\end{frame}

\begin{frame}{Bayesian approach : Thomson sampling}
  
  \begin{exampleblock}{Algorithm ``Thomson sampling'' (1933)}
    \begin{itemize}
    \item For each arm $i$, you maintain a probability (posterior)
      distribution $\pi_i^t$ that represent your knowledge on $\mu_i$.
    \item At each time step, you draw $n$ independent samples
      $\theta_i\sim \pi^t_i$. You choose
      $A_{t+1} = \argmax_i \theta_i$.
    \item You update your knowledge $\pi^{t+1}_i$ by using Bayes rule.
    \end{itemize}
  \end{exampleblock}
  \pause
  
  \begin{theorem}[Kauffman,Korda,Munos 2012]
    Thompson Sampling is asymptotically
    optimal\footnote{\url{https://arxiv.org/pdf/1205.4217.pdf }},
    \emph{i.e.} it provides $O(\log T)$ regret with the smallest
    possible constant. 
  \end{theorem}
  It is one of the most efficient algorithm (note : the analysis is
  similar to the one of UCB1). 
\end{frame}

\begin{frame}{Bayesian update in more detail}
  Suppose that $X$ is a Bernoulli random variable with a random
  parameter $\mu$. Assume that you know $\Proba{\mu=\theta}$.
  
  \begin{itemize}
  \item If you draw one sample of $X$ and you observe the result, how
    does your knowledge on $\mu$ evolve?
  \end{itemize}\pause \bigskip
  
  The answer comes from Bayes rule : if you observe $X=1$, then : 
  \begin{align*}
    \Proba{\mu=\theta \mid X=1} &= \frac{\Proba{X=1 \mid \mu=\theta}
                                  \Proba{\mu=\theta}}{\Proba{X=1}}\\
                                &\propto \theta \Proba{\mu=\theta}
  \end{align*}
  Similarly, if you observe $X=0$, then :
  \begin{align*}
    \Proba{\mu=\theta \mid X=0} &= \frac{\Proba{X=1 \mid \mu=\theta}
                                  \Proba{\mu=\theta}}{\Proba{X=1}}\\
                                &\propto (1-\theta) \Proba{\mu=\theta}.
  \end{align*}
\end{frame}

\begin{frame}{Bayesian update}
  \textbf{Consequence} : Assume now that you start from a uniform
  prior on $\mu$ (\emph{i.e.}  you assume that $\mu$ is uniformly
  distributed on $[0,1]$ and that you draw $n$ samples of $X$.

  Then the distribution of $\mu$ conditioned on having observed $p$
  times the value $1$ is a beta distribution, with probability density
  function $f_{p,n-p}(.)$: 
  \begin{align*}
    f_{p,n-p}(\theta) = c\theta^p(1-\theta)^{1-p},
  \end{align*}
  where $c$ is a constant such that the probability sums to one. \pause\bigskip

  \begin{exampleblock}{Thompson algorithm rewritten}
    \begin{itemize}
    \item Let $P_i(t)$ (respectively $N_i(t)$) be the number of time
      that arm $i$ has been pulled with a positive (respectively
      zero) reward. 
    \item Draw $n$ random variables $\theta_1\dots\theta_n$ where
      $\theta_i\sim\text{Beta}(P_i(t),N_i(t))$. 
    \item Chooses the next arm to be $\argmax_i \theta_i$. 
    \end{itemize}
  \end{exampleblock}
  Note : can be easily adapted to more complicated prior. 
\end{frame}

\begin{frame}{Numerical example (same parameter as before)}
  (Cumulative) regret as a function of the number of iterations
  ($\epsilon$-greedy has a linear but small regret)
  \includegraphics[width=.95\linewidth]{../simulations/regret_comparison_UCB_TS_epsilonGreedy}
\end{frame}

\begin{frame}{Conclusion and further reading}
  What did we learn :
  \begin{itemize}
  \item A way to quantify the tradeoff between exploration and
    exploitation.
  \item An algorithm that achieve this optimal tradeoff (Thompson
    sampling) and that uses a Bayesian framework.  
  \end{itemize}\bigskip

  To go further : 
  \begin{itemize}
  \item Contextual bandits
  \item Adversarial bandits 
  \item Markov bandits
  \end{itemize}
\end{frame}

\section{Markovian Bandits and Index Policies}

\begin{frame}{Markovian bandit model}
  Each arm is a Markov reward process
  \begin{itemize}
  \item For each arm : The reward $R:\{\text{States}\}\to\R$.
  \item Policy : $\{\text{State-tuples}\}\to\{\text{arm to pull
      next}\}$
  \item Choosing an arm $i$ corresponds to making one
    transition\footnote{Note : if you choose an arm $i$, the other
      arms do not move (we will relax this assumption later, see
      \emph{restless bandits}).} of the Markov chain $i$. 
  \end{itemize}\bigskip
  
  Objective : maximize the expected discounted reward.
  \begin{align*}
    \esp{\sum_{t=0}^\infty \gamma^t R_{I_t}(S_t)}. 
  \end{align*}
\end{frame}

\begin{frame}{MAB problem as a MDP}
  Problem: If you have $n$ arms and each arm has $S$ possible states,
  there are $S^n$ possibles states (tuples $(S_1,\dots,S_n)$).
  \bigskip
  
  Fortunately : (Gittins 74, 79, 89)
  \begin{itemize}
  \item The special structure allows us to decompose the problem. 
  \item You can compute an index for each arm (independently of the
    other arms).
  \item The optimal policy is to choose the arm with the highest
    index.
  \end{itemize}
\end{frame}

\subsection{Index policies}

\begin{frame}{Gittins index}
  Consider the two arm bandit problem:
  \begin{itemize}
  \item One arm is a stochastic arm starting in $s$, with transition
    matrix $P$ and reward function $R$. 
  \item One arm has a deterministic reward $\lambda$. 
  \end{itemize}\bigskip
 
  The optimal policy is an optimal stopping time :
  \begin{itemize}
  \item Pull arm $1$ until some stopping time $\tau$. Pull arm $2$
    forever after ($\tau$ might be equal to $+\infty$).
  \end{itemize}
  
  \pause 
  \begin{exampleblock}{The Gittin's index}
    \begin{align*}
      G_i(x_i) %
      &=\sup\{\lambda \mid \text{ optimal policy pulls arm $1$ when
        starting in $x_i$} \}\\
      &=\sup_{\tau\ge 1 \text{stopping time}}\left\{
        \frac{\esp{\sum_{t=0}^{\tau-1} \gamma^tr_i(x_i(t)) | x_i(0)=x_i}}%
        {\esp{\sum_{t=0}^{\tau-1} \gamma^t | x_i(0)=x_i}}
        \right\}
    \end{align*}
  \end{exampleblock}
\end{frame}

\begin{frame}{What is a stopping time? }
  
  A stopping times is a (random)-time such that the occurrence or
  non-occurrence of the event $\tau=t$ depends only on the information
  available at time $t$ (e.g. the values of $X_1, X_2, ..., X_t$).
  \bigskip
  
  Counter example : How do you make perfect toast?
  \begin{tabular}{cc}
    \mpage{.7}{
    There is a rule for timing toast, One never has to guess, Just wait
    until it starts to smoke, then 7 seconds less. (David Kendall)
    }
    &\mpage{.2}{\includegraphics[width=\linewidth]{toaster}}
  \end{tabular}
\end{frame}

\begin{frame}{Optimality of index policies}
  \begin{theorem}[Gittins, 74 79, 89]
    For any multi-armed bandit problem with finitely many arms and
    bounded rewards, a policy is optimal if and only if it always
    selects an arm with highest Gittins index.
  \end{theorem}

\end{frame}

\begin{frame}{Proof of Gittins index theorem (Weber 1992) -- 1/3}
  Consider a single arm where at each time step a player that
  observe a state $x$ can either
  \begin{itemize}
  \item Stop 
  \item Pay $\lambda$, receive a reward $R(x)$ and observe the next
    transition.
  \end{itemize}
  By definition of $G(x)$, the optimal policy is to continue playing
  if and only if $G(x)<\lambda$.\bigskip\pause

  \begin{exampleblock}{The prevailing charge}
    To encourage someone playing, you can lower the charge each time
    $G(x)<\lambda$. We call it the prevailing charge :
    \begin{align*}
      \mu(t) = \min\{G(x(0)), G(x(1)),\dots,G(x(t))\}. 
    \end{align*}
  \end{exampleblock}
  Note : $\mu(t)$ is non-increasing. 
\end{frame}

\begin{frame}{Proof of Gittins index theorem (Weber 1992) -- 2/3 }
  Now, consider a single arm where at each time step $t$ a player
  either stop or pays $\mu(t)$ to continue. We call the payoff the
  reward minus the paid prevailing charges.
  \begin{lemma}
    \begin{itemize}
    \item The expected payoff of any policy is non positive.
    \item The expected payoff of a policy that never stops when
      $G(t)>\mu(t)$ is zero.
    \end{itemize}
  \end{lemma}
  (Proof : break into intervals on which $\mu(t)$ is
  constant). \bigskip \pause
  
  Suppose $\pi$ is the Gittins index policy and $\sigma$ is any other
  policy.
  
  By the previous lemma, we have :
  \begin{align*}
    \esp{\text{Reward}(\sigma)} &\le \esp{\text{PrevalingCharge}(\sigma)}\\
    \esp{\text{Reward}(\pi)} &= \esp{\text{PrevalingCharge}(\pi)}
  \end{align*}\pause 
  To end the proof, it remains to show that 
  \begin{align*}
    \esp{\text{PrevalingCharge}(\sigma)}\le\esp{\text{PrevalingCharge}(\pi)}
  \end{align*}
\end{frame}

\begin{frame}{Proof of Gittins index theorem  (Weber 1992) -- 3/3 }
  To prove this, we couple the execution of $\pi$ and $\sigma$ by
  assuming that each arm goes through the same sequence of states for
  each policy.

  For example, assumes that the prevailing charges sequence are:
  \begin{align*}
    \text{Arm 1}: 3,2,1,1,1,1,\dots\\
    \text{Arm 2}: 6,2,0,0,0,0,\dots\\
    \text{Arm 3}: 5,4,3,3,1,1,\dots
  \end{align*}
  Gittins index policy sorts the prevailing charges into decreasing
  order :
  \begin{align*}
    6,5,4,3,3,3,2,2,1,1,\dots
  \end{align*}
  This sequence maximizes the discounted sum of prevailing charges. \qed
\end{frame}

\begin{frame}{Examples : Pandora box}
  Pandora has $n$ boxes.
  \begin{itemize}
  \item Box $i$ contains an item of value $x_i\in\R$ distributed with
    known c.d.f. $F_i$.
  \item At (known) cost $c_i$, she can open box $i$ and discover
    $x_i$. 
  \item Pandora may open boxes in any order and stop at any time. 
  \end{itemize}
  
  The goal is to open a subset of the boxes $S\subseteq\{1\dots n\}$
  in order to maximize the expected value of
  \begin{align*}
    \max_{i\in\calS} x_i - \sum_{i\in S}c_i. 
  \end{align*}
\end{frame}

\begin{frame}{What makes Pandora's box interesting?}
  \begin{itemize}
  \item It has many applications
    \begin{itemize}
    \item Hunting for a house, a job
    \item Choosing on which you want to work. 
    \end{itemize}\pause
  \item It has a nice solution, based on index:
    \begin{itemize}
    \item Compute a ``reservation prize'' for each box
    \item Open boxes in descending order of reservation prizes until a
      an item whose value exceeds the reservation prizes of any
      unopened box.
    \end{itemize}
  \end{itemize}\pause 

  The reservation prize is: 
  \begin{align*}
    x_i^* = \inf\{y \text{ such that } y\ge -c_i + \esp{\max(x_i,y)}\}
  \end{align*}

  Exercise : derive this optimal policy by using index policies. 
\end{frame}

\subsection{Restless Bandits}

\begin{frame}{Restless Bandits}
  \tableofcontents[currentsubsection]
  
  \begin{center}
    \includegraphics[width=.5\linewidth]{spinningPlates}
  \end{center}
\end{frame}

\begin{frame}{Restless Bandits}
  We consider a Bandit problem with $n$ arms.
  \begin{itemize}
  \item For each arm two actions are available : $a=1$ (active) or
    $a=0$ (passive). 
  \item There are two\footnote{In the classical (non-restless) bandit,
      choosing $a=0$ meant $P=Id$ and $R=0$.} transitions matrices and
    reward functions depending on $a$.
  \end{itemize}
  Objective : maximize the time-average reward from $n$ restless
  bandits while choosing exactly $m$ active arms out of $n$ at each
  time. 

  \bigskip
  Examples:
  \begin{itemize}
  \item Allocation of jobs to different users / machines
  \item Opportunistic channel selection 
  \end{itemize}
\end{frame}

\begin{frame}{Whittle index policy}

  Similarly to the classical index problem, we look at the problem
  where the controller pays a price $\lambda$ each time he chooses the
  action $1$.

  Consider the problem of maximizing the average reward~:
  \begin{align*}
    \lim_{T\to\infty}\frac1T\esp{\sum_{t=0}^{T}
    (\lambda\mathbf{1}_{\text{action at time $t$ is 1}} + r_i(x_i(t))) \mid x_i(0)=x_i}%
  \end{align*}\pause

  For many problems, as $\lambda$ increases from $-\infty$ to
  $+\infty$ the set of states for which the optimal action is $1$
  decreases from the total state space to $\emptyset$.

  If this is the case, the bandit is called \textbf{indexable} and we
  can define~: 

  \begin{center}
    \mpage{.8}{%
      \begin{exampleblock}{Whittle index}
        The Whittle index $W(x)$ is the largest cost for which taking
        action $1$ is optimal when starting in $x$.
      \end{exampleblock}
    }
  \end{center}
  This motivates the following heuristics:
  \begin{itemize}
  \item Activates the arms with the largest index. 
  \end{itemize}
\end{frame}

\begin{frame}{Whittle index is often viewed as a Lagrangian
    decomposition}

  The original problem is : 
  \begin{align*}
    \min &\lim_{T\to\infty}\frac1T\esp{\sum_{i=1}^n\sum_{t=0}^{T}r_i(x_i(t))}\\ 
         &\text{ such that for all $t$: $\sum_{i=1}^na_i(t)=m$}
  \end{align*}
  This problem is complicated because $\sum_{i=1}^na_i(t)=m$ couples
  all problems. \pause 

  Now, replace the above constraint by a ``lighter'' constraint
  $\lim_{T\to\infty}\frac1T\sum_{t=1}^T\sum_{i=1}^na_i(t)=m$. The
  Lagrangian decomposition is: 
  \begin{align*}
    \esp{\sum_{t=0}^{T}\sum_{i=1}^nr_i(x_i(t))}
    &+\lambda\left(\lim_{T\to\infty}\frac1T\sum_{t=1}^T\sum_{i=1}^na_i(t) - m\right)\\
    &=\sum_{i=1}^n\esp{\sum_{t=0}^{T}(\lambda a_i(t) + r_i(x_i(t)))}-\lambda m. 
  \end{align*}
  which can be decomposed in $n$ independent problems if $\lambda$ is
  fixed. 
\end{frame}

\begin{frame}{Open questions}
  \begin{itemize}
  \item Under what assumptions is a restless bandit indexable? \pause

    The answer is not obvious:
    \begin{itemize}
    \item Special classes of restless bandits are indexable: such as
      'dual speed', Glazebrook, Nino-Mora, Ansell (2002), W. (2007).
    \item Indexability can be proved in some problems (such as the
      opportunistic spectrum access problem, Liu and Zhao (2009)).
    \end{itemize}
    \pause\bigskip
  \item How good are index policies for restless bandits? \pause

    It is not in general optimal but:
    \begin{itemize}
    \item It may be
      optimal. (opportunistic spectrum access — identical channels,
      Ahmad, Liu, Javidi, and Zhao (2009)). 
    \item It works very well in practice.
    \item It is often asymptotically optimal, W. and Weiss (1990). 
    \end{itemize}
  \end{itemize}
\end{frame}

% \section{Extensions : adversarial bandit? }

% \begin{frame}{}
% \end{frame}

% \begin{frame}{}
% \end{frame}


\section{Conclusion}

\begin{frame}{Conclusion}
  Multi-Armed bandit is a general framework with many variants
  \begin{itemize}
  \item Good example of exploration--exploitation tradeoff
  \item Probabilistic model (Bayesian) or model-free
  \item Many variants not covered : Adversarial, contextual,
    time-varying,...
  \end{itemize}
  \bigskip

  It has a many application
  \begin{itemize}
  \item Online problems
  \item Reinforcement learning
  \end{itemize}
  
\end{frame}


\end{document}