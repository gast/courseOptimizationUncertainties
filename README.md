This repository contains document relative to the cours of the master ORCO "optimization under uncertainty". 

# Organization 

* week 1 : Introduction [slides](https://gitlab.inria.fr/gast/courseOptimizationUncertainties/raw/master/slides/slides_intro.pdf) and [exercises](https://gitlab.inria.fr/gast/courseOptimizationUncertainties/raw/master/exercises/exo_intro.pdf).
* week 2 : Markov chains : [slides](https://gitlab.inria.fr/gast/courseOptimizationUncertainties/raw/master/slides/markov_chains.pdf) and [exercises about Markov chain](https://gitlab.inria.fr/gast/courseOptimizationUncertainties/raw/master/exercises/exo_markovChains.pdf)
* week 3 and 4 : Markov decision processes : [slides](https://gitlab.inria.fr/gast/courseOptimizationUncertainties/raw/master/slides/markov_decision_processes.pdf) and [exercises about Markov decision processes](https://gitlab.inria.fr/gast/courseOptimizationUncertainties/raw/master/exercises/exo_MDP.pdf)
* week 5 : Bandits algorithms and introduction to reinforcement learning [slides](https://gitlab.inria.fr/gast/courseOptimizationUncertainties/raw/master/slides/bandits.pdf). [exercises on Bandits](https://gitlab.inria.fr/gast/courseOptimizationUncertainties/raw/master/exercises/exo_bandits.pdf)


* week 6 and 7: Jérome Malick: Chance-constraints et distributionnally robust optimization.

* week 8-12: Moritz Muhlenthaler: Robust optimization.


# Past Exams

## 2020-2021

* [December 2020](https://gitlab.inria.fr/gast/courseOptimizationUncertainties/blob/master/past_exams/examDec2020.pdf) ([ideas of a correction for Ex1](https://gitlab.inria.fr/gast/courseOptimizationUncertainties/blob/master/past_exams/examDec2020_correction1.pdf), [ideas of a correction for Ex2](https://gitlab.inria.fr/gast/courseOptimizationUncertainties/blob/master/past_exams/examDec2020_correction2.pdf))

## 2019-2020

* [Final exam](https://gitlab.inria.fr/gast/courseOptimizationUncertainties/blob/master/past_exams/examJan2020.pdf)
* [December 2019](https://gitlab.inria.fr/gast/courseOptimizationUncertainties/blob/master/past_exams/examDec2019.pdf)
* [November 2019](https://gitlab.inria.fr/gast/courseOptimizationUncertainties/blob/master/past_exams/examNov2019.pdf), 30 min exam ([ideas of a correction](https://gitlab.inria.fr/gast/courseOptimizationUncertainties/blob/master/past_exams/examNov2019_correction.pdf))
* [October 2019](https://gitlab.inria.fr/gast/courseOptimizationUncertainties/blob/master/past_exams/examOct2019.pdf), 30 min exam ([ideas of a correction](https://gitlab.inria.fr/gast/courseOptimizationUncertainties/blob/master/past_exams/examOct2019_correction.pdf))

## 2018-2019

* [Final exam](https://gitlab.inria.fr/gast/courseOptimizationUncertainties/blob/master/past_exams/examJan2019.pdf), 30 min exam ([ideas of a correction of exercices 1 and 2](https://gitlab.inria.fr/gast/courseOptimizationUncertainties/blob/master/past_exams/examJan2019_correctionEx-1-2.pdf)
* [Novembre 30 2018](https://gitlab.inria.fr/gast/courseOptimizationUncertainties/blob/master/past_exams/exam30Nov2018.pdf), 30 min exam ([ideas of a correction](https://gitlab.inria.fr/gast/courseOptimizationUncertainties/blob/master/past_exams/exam30Nov2018_correction.pdf))
* [Novembre 2018](https://gitlab.inria.fr/gast/courseOptimizationUncertainties/blob/master/past_exams/examNov2018.pdf), 30 min exam ([ideas of a correction](https://gitlab.inria.fr/gast/courseOptimizationUncertainties/blob/master/past_exams/examNov2018_correction.pdf))
* [October 2018](https://gitlab.inria.fr/gast/courseOptimizationUncertainties/blob/master/past_exams/examOct2018.pdf), 30min exam ([ideas of a correction](https://gitlab.inria.fr/gast/courseOptimizationUncertainties/blob/master/past_exams/examOct2018_correction.pdf))


## 2017-2018

* [Novembre 2017](https://gitlab.inria.fr/gast/courseOptimizationUncertainties/blob/master/past_exams/examNov2017.pdf) ([ideas of a correction](https://gitlab.inria.fr/gast/courseOptimizationUncertainties/blob/master/past_exams/examNov2017_correction.pdf))
* [Jannuary 2018](https://gitlab.inria.fr/gast/courseOptimizationUncertainties/blob/master/past_exams/examJan2018.pdf) ([ideas of a correction for the first two exercices](https://gitlab.inria.fr/gast/courseOptimizationUncertainties/blob/master/past_exams/examJan2018_correction.pdf))
