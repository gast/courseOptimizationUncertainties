# Exams

## 2018--2019

* (First in class exam)[https://gitlab.inria.fr/gast/courseOptimizationUncertainties/raw/master/past_exams/examOct2018.pdf] (October 2018)
(Ideas of correction)[https://gitlab.inria.fr/gast/courseOptimizationUncertainties/raw/master/past_exams/examOct2018_correction.pdf]

## 2017--2018

* (Final exam)[https://gitlab.inria.fr/gast/courseOptimizationUncertainties/raw/master/past_exams/examJan2018.pdf] (January 2018)
(Some ideas for the correction)[https://gitlab.inria.fr/gast/courseOptimizationUncertainties/raw/master/past_exams/examJan2018_correction.pdf]
* (In class exam)[https://gitlab.inria.fr/gast/courseOptimizationUncertainties/raw/master/past_exams/examNov2017.pdf] (November 2017)
(Ideas of correction)[https://gitlab.inria.fr/gast/courseOptimizationUncertainties/raw/master/past_exams/examNov2017_correction.pdf]
