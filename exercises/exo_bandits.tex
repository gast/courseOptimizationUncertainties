\documentclass{article}

\newcommand\titre{Exercise on Bandits}%
\newcommand\annee{2018-2019}%
\newcommand\auteur{Nicolas Gast}%

\usepackage{tikz}%
\usepackage[bottom=2cm]{geometry}%
\usepackage{amsmath,amsthm,amsfonts}
\newcommand\N{\mathbb{N}}
\newcommand\R{\mathbb{R}}
\newcommand\calS{\mathcal{S}}

\title{Optimization under uncertainties : \titre}
\author{\auteur}
\date{Master ORCO -- \annee}

\usepackage{lastpage}
\usepackage{fancyhdr}
\pagestyle{fancy}
\fancyhead[LO,LE]{\auteur}
\fancyhead[RO,RE]{Master ORCO : \titre}
\fancyfoot[CO, CE]{Page \thepage\ of \pageref*{LastPage}}
\usepackage{hyperref}

\begin{document}


\maketitle

\section{Stochastic bandits}

\subsection{Warm-up (from Sutton and Barto)}

In $\varepsilon$-greedy action selection, for the case of two actions
and $\varepsilon=0.5$, what is the probability that the greedy action
is selected?

\subsection{Bandit example (from Sutton and Barto)}

Consider a $k$-armed bandit problem with $k=4$ actions $\{1\dots3\}$.
Consider applying to this problem a bandit algorithm using
$\varepsilon$-greedy action selection in which our estimated value of
an arm is the average of the past reward observed (or $+\infty$ if no
action has been observed).  Suppose that the sequence of action is :
$A_1=1, R_1=1, A_2=2, R_2=1, A_3=2, R_3=2,
A_4=2,R_4=2,A_5=3,R_5=0,A_6=2,R_6=0$.

\begin{enumerate}
\item On some of these time steps the $\varepsilon$ case may have
  occurred, causing an action to be selected at random. On which time
  steps did this definitely occur? On which time steps could this
  possibly have occurred?
\item What is the probability distribution according to which the next
  action will be chosen?
\end{enumerate}

\subsection{Performance of $\epsilon$-greedy}
\label{sec:eps-greedy}

Consider a 10-arm problem in which the reward of an arm $i$ is a
Bernoulli random variable of parameter $p_i=0.3+i/20$ for
$i\in\{1\dots 10\}$.

Consider that you use the $\epsilon$-greedy policy. 
\begin{enumerate}
\item Compute the long-run average reward of any $\epsilon$-greedy
  policy (for which $\epsilon>0$). 
\item What does it change when $\epsilon=0$?
\item By means of a numerical method:
  \begin{itemize}
  \item Plot the long term average reward as a function of $\epsilon$
    for various values of $\epsilon$.
  \item Plot the average reward of the $\epsilon$-reward policy
    as a function of time (for $\epsilon=0$, $\varepsilon=0.01$ and
    $\varepsilon=0.1$). 
  \end{itemize}
\item What can you conclude? 
\end{enumerate}

\subsection{Thomson sampling}

\subsubsection{Bayesian learning}

Consider a $1$-arm bandit problem in which you want to estimate a
success probability of $q$.
\begin{itemize}
\item Implement a Bayesian learning algorithm that starts with a Beta
  prior and update the distribution according to the samplings that
  you observe.
\item Test with $q=0.01$ and observe how your posterior distribution
  evolve as a function of the number of samplings.  Compare the case
  with a uniform prior and a prior biased towards $0$. 
\end{itemize}

\subsubsection{Thomson sampling}

Implement your version of Thomson sampling and compare the average
regret that you obtain with Thomson sampling and with an
$\epsilon$-greedy algorithm (for example on the
example~\ref{sec:eps-greedy}). 

%\newpage

\section{Index policies}

\subsection{Pandora's box}

There are $n$ boxes in front of you. The box $i$ contains an item of
value $x_i\in\R$ distributed with known c.d.f. $F_i$. At (known) cost
$c_i$, you can open box $i$ and discover $x_i$. You can open as many
boxes as you want (in any order) and stop at any time.

Your goal is to open a subset of the boxes $S\subseteq\{1\dots n\}$ to
maximize the expected value of
\begin{align*}
  \max_{i\in\calS} x_i - \sum_{i\in S}c_i. 
\end{align*}

Let $\gamma\in(0,1)$. Box $i$ is associated with Bandit $B_i$ that
starts in state $0$. Then:
\begin{itemize}
\item First time $B_i$ is continued, the reward is $-c_i$, and the
  state becomes $x_i$ (picked from $F_i$).
\item At subsequent time, $B_i$, the reward is $x_i(1-\gamma)$ and the
  state remains $x_i$. 
\end{itemize}

\begin{enumerate}
\item For a given $i$, consider the following Bandit : the Bandit
  starts in state $0$.
\end{enumerate}

\subsection{(*) Object searching}

A stationary object is hidden in one of $n$ boxes. When you look in
box $i$, there is a probability $q_i$ that you find this object (if it
is in the box). Each time you search a box $i$, it costs you $c_i$.

Before starting the search, the object is put in box $i$ with
probability $p_i$. How should the boxes be sequenced for search so as
to minimize the expected cost of finding the object?

\subsection{Numerical computations}

Let $P$ be the transition matrix of a finite-state Bandit, $r$ its
reward vector and $\gamma\in(0,1)$ the discount factor.

\begin{enumerate}
\item Justify that the greatest Gittins index is $\max_ir_i$ and that
  the least Gittins index is $(1-\gamma)\min_i\left((I-\gamma
    P)^{-1}r\right)_i$. 
  
\item Does the following algorithm compute the Gittins indices
  corresponding to this Bandit?

\begin{verbatim}
N = dimension of matrix (P)
Pp = zero-matrix of size NxN
G = zero-vector of size N
already_computed = {}
for i from 1 to N:
  reward = (I-gamma Pp)^{-1} R
  time   = (I-gamma Pp)^{-1} 1   # 1 = vector of 1
  mu = reward / time             # element-wise division
  Let j = \argmax_{j \not\in already_computed} mu
  G[j] = mu[j]
  Add j to "already_computed"
  Pp[j,:] = P[j,:]
\end{verbatim}

\item Implement this algorithm in Python. What is its complexity? 
\end{enumerate}

% \newpage

\section{(*) Whittle index (and job allocation)}

We consider the following queuing model of multi-server system. 
\begin{itemize}
\item At each time step, a new job arrive to a central broker with
  probability $\lambda$. It is routed to one of the $n$ servers. 
\item Each server serves jobs one at a time.  The other jobs are
  stored in a local queue. The queue of server $i$ can store at most
  $Q_i$ jobs.
\item If the server $i$ has one or more jobs to serve, it serves one
  job with probability $\mu_i$.
\end{itemize}

The goal of this exercise is to program and evaluate a policy to
allocate incoming jobs to the various servers.

\begin{enumerate}
\item Program a simulator of this system that you will use to test
  different allocation policies.

\item Use your simulator to plot the total number of jobs in the
  system as a function of time for $\lambda=1$ and $\mu_1=\mu_2=0.2$,
  $\mu_3=0.3$, $\mu_4=0.4$ and $\mu_5=0.4$ and $Q_i=100$. For the
  allocation policies, you will compare :
  \begin{itemize}
  \item[(a)] Random : each job are allocated to any servers at random.
  \item[(b)] JSQ : each job is allocated to the server that has the
    shortest queue (i.e., the least number of jobs)
  \end{itemize}
  
\item Program a function that computes the average number of jobs in
  steady state of a policy.
\item Formulate the problem as a MDP. Why does it seem difficult to
  apply dynamic programming to solve the problem? 
\item Write a program that computes Whittle indices for each queue. 
\item Consider a case with two servers and $\lambda=1$, $\mu_1=0.3$
  and $\mu_2=0.8$. Compare numerically the performance (= average
  number of jobs) of
  \begin{itemize}
  \item[(a)] JSQ
  \item[(b)] The index policy
  \item[(c)] The optimal policy (obtained using policy iteration). 
  \end{itemize}
\item Can you find parameters for which the optimal policy outperforms
  significantly the optimal policy? 
\end{enumerate}

\end{document}