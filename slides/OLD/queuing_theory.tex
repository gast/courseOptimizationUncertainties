\input{latex_entete}

\title{Master ORCO : Optimization under uncertainty : Queuing Theory}%
\author{Nicolas Gast}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}{Reference ($\approx$ 500 pages!)}
  \centering  \includegraphics[width=.4\linewidth]{harchol}

  (disclaimer : many figures from these slides are taken from the
  book). 
\end{frame}

\section{What is Queuing Theory? }
\subsection{Generalities, Little's Law}

\begin{frame}{Where do I find queues?}
  \begin{tabular}{ccc}
    \includegraphics[width=.3\linewidth]{queue_supermarket}
    &\includegraphics[width=.3\linewidth]{queue_computer}
    &\includegraphics[width=.3\linewidth]{queue_tesla}\\
    Supermarket & Computers & Production line
  \end{tabular}
  \vspace{1cm}
  
  Objective of Queueing theory : 
  \begin{itemize}
  \item Predict performance 
  \item Optimize (\emph{e.g.} reorganization / scheduling)
  \end{itemize}
  Means:
  \begin{itemize}
  \item Analytical methods
  \item Numerical methods 
  \item Simulation 
  \end{itemize}
\end{frame}


\begin{frame}{The beginning of queuing theory : Erlang's model}
  \begin{tabular}{cc}
    
    \begin{tabular}{c}
      \includegraphics[width=0.3\linewidth]{callCenter.jpg}
    \end{tabular}
    &
      \begin{minipage}{0.6\linewidth}
        
        Model for arrivals : Poisson process
        \begin{equation*}
          \Proba{\text{A call during $dt$}}=\lambda dt + o(dt). 
        \end{equation*}
        Temporal independence. 
        \bigskip
        
        Time to treat a call : random duration. 
        
      \end{minipage}
  \end{tabular}
\end{frame}

\begin{frame}{A first example}
  \begin{center}
    \includegraphics[width=0.9\linewidth]{queue}
    % \includegraphics[width=0.5\linewidth]{discrete_system}
    
    {\tiny (Source:
      \url{http://www.cs.cmu.edu/~harchol/PerformanceModeling/}) }
  \end{center}
  \bigskip

  A queue is described by:
  \begin{itemize}
  \item A process of arrivals
  \item A waiting room 
  \item One or multiple server 
  \item A service discipline (FCFS, FCLS,\dots)
  \item \dots 
  \end{itemize}
  % Travail d'Erlang: 
  % \begin{itemize}
  % \item Probabilité de rejet d'un appel et dimensionnement
  % \end{itemize}

\end{frame}

\begin{frame}{Open and closed queuing networks}
  \begin{tabular}{cc}
    \mpage{.45}{\centering\includegraphics[width=\linewidth]{harchol_fig24}

    {\tiny (Source:
    \url{http://www.cs.cmu.edu/~harchol/PerformanceModeling/}) }
    }&\uncover<2>{\mpage{.45}{\centering\includegraphics[width=\linewidth]{closed_network}}}\\
    \mpage{.45}{An \alert{open network} has arrivals and departures}
     &\uncover<2>{\mpage{.45}{A \alert{closed network} has no external
       arrivals or departure}} 
  \end{tabular}
\end{frame}

\begin{frame}{Metrics for open networks}
  When studying a queuing system, one is often interested in
  characterizing or optimizing one of the following metric :
  \begin{itemize}
  \item Response time / waiting time 
  \item Number of jobs in the system 
  \item Throughput 
  \item Utilization 
  \end{itemize}

  \begin{center}
    \begin{tikzpicture}
      \node at (0,0){\includegraphics[width=0.5\linewidth]{queue}};
      \node at (-2,-.5) {$\lambda$};
      \node at (2.5,-.5) {$\mu$};
    \end{tikzpicture}
  \end{center}
  The \alert{load} is $\rho=\lambda/\mu$, where 
  \begin{itemize}
  \item $\lambda$ is the mean arrival rate. 
  \item $1/\mu$ is the mean service time. 
  \end{itemize}
\end{frame}

\begin{frame}{Little's Law}
  \begin{theorem}{Little's Law}
    For any open system, we have :
    \begin{align*}
      \bar{N}^{\text{Time average}} = \lambda
      \bar{T}^{\text{Time average}}, 
    \end{align*}
    provided that the following quantities exists :
    \begin{itemize}
    \item $\bar{N}^{\text{Time average}}$ : the ``time average''
      number of jobs in the system
    \item $\lambda$ is the average arrival \emph{and} departure rate
    \item $\bar{T}^{\text{Time average}}$ the ``time average''
      time that jobs spend in the system.
    \end{itemize}
  \end{theorem}
  
  \begin{center}
    \includegraphics[width=.7\linewidth]{little_law}
  \end{center}
\end{frame}
\begin{frame}{Little's Law : examples}
  \begin{itemize}
  \item A professor takes $2$ new PhD students every year. If it takes
    $3.5$ years on average for students to graduate, how many students
    does the professor has on average?

    \uncover<2>{$$2 (students/year) \times 3.5 year = 7 students$$}
    
  \item The population of France is $67$ millions, the expected
    lifetime is $82$ years. Can you estimate how many babies are born
    every year (assuming stationarity)?

    \uncover<2>{$$67M pers /82 year \approx 817k pers/year$$}

  \item You observe that, on average, people spend $1h$ in your
    supermarket. On average, $10$ persons per minute enter the
    supermarket. What is the average number of people in the
    supermarket?

    \uncover<2>{$$10 (pers/min) 60 min = 600 pers$$}
  \end{itemize}
\end{frame}

\begin{frame}{Little's Law : proof}
  Let $A(t)$ be the number of arrivals between $0$ and $t$ and $D(t)$
  be the number of departure between $0$ and $t$. Assume that :
  \begin{align*}
    \lim_{t\to\infty}\frac{A(t)}{t} = \lim_{t\to\infty}\frac{D(t)}{t}
    = \lambda.
  \end{align*}
  \begin{tabular}{@{}c@{}c}
    \mpage{.45}{\includegraphics[width=\linewidth]{little_proof}}
    &\pause\mpage{.5}{Let $N(s)$ be the number of jobs at time
      $s$. The size of the Grey area is :
      \begin{align*}
        \calA rea(t) = \int_0^t N(s)ds. 
      \end{align*}
      Moreover :
      \begin{align*}
        \sum_{i\in D(t)} T_i \le \calA rea(t) \le \sum_{i\in A(t)} T_i. 
      \end{align*}
      }
  \end{tabular}\pause
  
  \begin{align*}
    \lim_{t\to\infty}\left( \sum_{\sum_{i\in
    D(t)}}\frac{T_i}{D(t)}\frac{D(t)}{t}
    \le \frac1t\int_0^t N(s)dt
    \le  \sum_{\sum_{i\in
    A(t)}}\frac{T_i}{A(t)}\frac{A(t)}{t} \right)
  \end{align*}
\end{frame}

\subsection{The Single Queue Model}
\begin{frame}{Outline}
  \tableofcontents[currentsubsection]
\end{frame}

\begin{frame}{Kendall's notation}
  Notation $A/B/x$ (arrival process / size of jobs / number of
  servers)
  
  \begin{itemize}
  \item M/M/1 queue :
    \begin{itemize}
    \item Jobs arrive according to a Poisson process
    \item Each jobs has an exponential service time
    \end{itemize}
  \item M/D/1 : deterministic service time
  \item M/M/k : $k$ servers 
  \item M/G/1 : general service time 
  \end{itemize}
\end{frame}

%\begin{frame} 
%\end{frame}

\begin{frame}{How to simulate a single queue : Lindley's formula
    [1952]}

  Let : 
  \begin{itemize}
  \item $T_n$ be the time between the arrival of the job $n-1$ and
    $n$. 
  \item $S_n$ the service time of the $n$th client. 
  \item $W_n$ the \emph{waiting time} of the client $n$.
  \end{itemize}
  \bigskip
  
  Assume that the service discipline is FCFS (first come first
  serve). Then :
  \begin{equation*}
    \red{W_{n+1} = \max ( 0, W_n + S_n - T_{n+1} )}
  \end{equation*}

\end{frame}
\begin{frame}{Lindley's formula : explanation}

  \begin{equation*}
    \red{W_{n+1} = \max ( 0, W_n + S_n - T_{n+1} )}
  \end{equation*}
  \pause 
  
  \begin{center}
    \tikz{%
      \node at (0,0)
      {\includegraphics[width=.8\linewidth]{poisson_05}};%
      \uncover<2->{\node[fill=blue!10] at (-2.1,.6) {\small $W_1=0$};}%
      \uncover<2->{%
        \draw[<->](-3.75,-1.7)--node[below]{\small$T_1{=}1.2$}(-2.1,-1.7);% 
        \draw[<->](-2.1,-1.7)--node[below]{\small$T_2{=}0.7$}(-1.1,-1.7);% 
        \draw[<->](-1.1,-1.7)--node[below]{\small$T_3{=}2.5$}(2.3,-1.7);
        \draw[<->](2.3,-1.7)--node[below]{\small$T_4{=}1.3$}(4.2,-1.7);
      }% 
      \uncover<3->{\node[fill=blue!10] at (-1.1,1.5) {\small $W_2=\max(0,1-0.7)=0.3$};}
      \uncover<4->{\node[fill=blue!10] at (2.3,.5) {\small $W_3=\max(0,1.3-2.5)=0$};}%
    }
  \end{center}
\end{frame}

\begin{frame}{Using Lindley's formula to simulate a stochastic
    system}{Stochastic and deterministic models are different}
  
  Assume that $S_n=1$. 
  \begin{tabular}{@{}c|c@{}}
    Deterministic arrivals : $T_n=1.25$
    & Poisson arrival of intensity $\lambda=1/1.25$\\
    &$P(T_n\ge x)=e^{-x/1.25}$\\
    \begin{tabular}{@{}c@{}}
      \includegraphics[width=.48\linewidth]{deter}
    \end{tabular}
    &\uncover<2>{
      \begin{tabular}{@{}c@{}}
        \includegraphics[width=.48\linewidth]{poisson_08}
      \end{tabular}
      }
    \\
    Expected waiting time : $\esp{W_n}=0$
    &\uncover<2>{
      $\esp{W_n}=\frac{\lambda}{2(1-\lambda)}=2$
      % Temps de réponse moyen : $3$ secondes. 
      }
  \end{tabular}
  \bigskip
  
  % Pollaczek–Khinchine formula
\end{frame}

\begin{frame}{Pollaczek–Khinchine Formula\footnote{\tiny
      \url{https://en.wikipedia.org/wiki/Pollaczek-Khinchine_formula}}
    [1930]}{The variability of the service time plays a big role. }
  
  Assume that : 
  \begin{itemize}
  \item Clients arrive according to a Poisson process of rate
    $\lambda$. 
  \item The service time has mean $1$ and variance $V$.
  \item Jobs are scheduled according to any non-preemptive
    non-size-dependent service discipline. 
  \end{itemize}
  \bigskip
  
  Let $W$ be the average waiting time : 
  \begin{equation*}
    W = \frac{\lambda}{2(1-\lambda)}(1 + \alert{V}).
  \end{equation*}
  \bigskip
  
  \textbf{Note}: $W$ goes to infinity if $\lambda\to1$ or $V\to+\infty$.
\end{frame}

\begin{frame}{Note on scheduling (see also Harchol-Balter's book, Part
    VII.)}
  Assume that the jobs are treated according to a \alert{processor
    sharing} (PS) discipline and that the mean service time is
  $1$. Then, the average waiting time of a job is:
  \begin{align*}
    \esp{W^{PS}(x)} = \frac{\lambda}{1-\lambda}
  \end{align*}
  The average service time of a jobs of size $x$ is
  \begin{align*}
    \esp{T^{PS}(x)} = \frac{x}{1-\lambda}
  \end{align*}
  The term $1/(1-\lambda)$ is the called the \alert{slowdown}.  \bigskip
  
  % (if the scheduling discipline is non-preemptive and size-independent
  % like FCFS, then the average waiting time is
  % $\displaystyle\esp{W} = \frac{\lambda}{2(1-\lambda)}(1 +
  % \alert{V})$).

  \begin{itemize}
  \item This (partly) explains why CPU scheduling uses PS.
  \item It also explains why the load of a server is usually smaller
    than $80\%$.
  \end{itemize}
  
  

\end{frame}


\begin{frame}{Where do we stand?}
  \begin{itemize}
  \item Little's law
    \begin{itemize}
    \item Extremely general and useful result. 
    \end{itemize}
    \bigskip
  \item Lindley's formula
    \begin{itemize}
    \item Makes simulation easy
    \end{itemize}\bigskip
  \item The waiting time sometimes goes to infinity
  \end{itemize}
  \bigskip
  
  Next: a bit of theory (continuous time Markov chains)
\end{frame}

% \subsection{The M/G/1 queue and the impact of scheduling}

\section{Continuous Time Markov Chains}

% \begin{frame}{Outline}
%   \tableofcontents[currentsubsection]
% \end{frame}

\subsection{Markov chains with countable (infinite) state-space}

\begin{frame}{Example of a countable state-space}
  \begin{center}
    \includegraphics[width=.6\linewidth]{queue}
  \end{center}
  
  Let $X_n$ be the number of jobs waiting at time $n$.

  Consider that at each time step on the two following events occur:
  \begin{itemize}
  \item one job arrive in the system with probability $1/3$,
  \item one job is served with probability $2/3$. 
  \end{itemize}

  Is $X_n$ a Markov chain? 
  \pause
  \bigskip
  
  Yes but with an infinite (countable) state-space :
  \begin{center}
    \begin{tikzpicture}[auto,node distance=1.3cm,semithick,shorten
      >=1pt]
      \tikzstyle{noeud}=[circle,draw]; \foreach \y in {0,...,4}
      \node[noeud] (\y) at (1.3*\y,0) {\y}; \node[circle,dashed] (5)
      at (5*1.3,0) {...};
    
      \foreach \x/\y in {1/2,2/3,3/4,4/5} \draw (\x) edge[->,bend
      left] node[above] {$1/3$} (\y) (\y) edge[->,bend left]
      node[below] {$2/3$} (\x);
    
      \draw (0) edge[->,bend left] node[above] {$1/3$} (1) (1)
      edge[->,bend left] node[below] {$2/3$} (0); 
    \end{tikzpicture}
  \end{center}
\end{frame}

\begin{frame}{Stationary distributions for infinite state-space}

  Consider these two Markov chains :
  
  \begin{tabular}{cc}
    \mpage{.45}{\begin{tikzpicture}[auto,node distance=1.3cm,semithick,shorten
      >=1pt,xscale=.7]
      \tikzstyle{noeud}=[circle,draw]; \foreach \y in {0,...,4}
      \node[noeud] (\y) at (1.3*\y,0) {\y}; \node[circle,dashed] (5)
      at (5*1.3,0) {...};
    
      \foreach \x/\y in {1/2,2/3,3/4,4/5} \draw (\x) edge[->,bend
      left] node[above] {$1/3$} (\y) (\y) edge[->,bend left]
      node[below] {$2/3$} (\x);
    
      \draw (0) edge[->,bend left] node[above] {$1/3$} (1) (1)
      edge[->,bend left] node[below] {$2/3$} (0); 
    \end{tikzpicture}}
    &\mpage{.45}{\begin{tikzpicture}[auto,node distance=1.3cm,semithick,shorten
      >=1pt,xscale=.7]
      \tikzstyle{noeud}=[circle,draw]; \foreach \y in {0,...,4}
      \node[noeud] (\y) at (1.3*\y,0) {\y}; \node[circle,dashed] (5)
      at (5*1.3,0) {...};
    
      \foreach \x/\y in {1/2,2/3,3/4,4/5} \draw (\x) edge[->,bend
      left] node[above] {$2/3$} (\y) (\y) edge[->,bend left]
      node[below] {$1/3$} (\x);
    
      \draw (0) edge[->,bend left] node[above] {$2/3$} (1) (1)
      edge[->,bend left] node[below] {$1/3$} (0); 
    \end{tikzpicture}}\\
    $X_n$&$Y_n$
  \end{tabular}
  \bigskip
  
  What is the fundamental difference between $\lim_{n\to\infty}X_n$?
  and $\lim_{n\to\infty}Y_n$?
  \pause\bigskip
  
  Answer :
  \begin{itemize}
  \item $X_n$ will stay close to $0$.
  \item $Y_n$ will diverge to infinity. 
  \end{itemize}
\end{frame}

\begin{frame}{Transient and recurrent state}
  Consider a Markov chain $(X_n)$ (recall that
  $P_{ij}=\Proba{X_{n+1}=j | X_n=i}$) and define $m_{jj}$ as the
  probability of returning to a state $j$ :
  \begin{align*}
    m_{jj} = \Proba{ \exists n>0 : X_n = j \mid X_0 = j}
  \end{align*}

  \textbf{Definition}. 
  \begin{itemize}
  \item A state $j$ is recurrent if $m_{jj}=1$;
  \item A state $j$ is transient if $m_{jj}<1$.
  \end{itemize}
  \bigskip\pause
  \textbf{Theorem}. In an irreducible chain, either all states are
  transient or all states are recurrent.
\end{frame}

\begin{frame}{Transient and recurrent Markov chains}
  If a chain is recurrent, we say that it is : 
  \begin{itemize}
  \item Null recurrent if
    $m_{jj}:=\esp{\inf\{n>0 : X_n = j \mid X_0 = j\}}=\infty$ for all
    $j$.
  \item Positive recurrent if
    $m_{jj}:=\esp{\inf\{n>0 : X_n = j \mid X_0 = j\}} < +\infty$ for all
    $j$.
  \end{itemize}
  \bigskip\pause

  \begin{tabular}{cc}
    \mpage{.65}{\begin{tikzpicture}[auto,node distance=1.3cm,semithick,shorten
        >=1pt]
        \tikzstyle{noeud}=[circle,draw]; \foreach \y in {0,...,4}
        \node[noeud] (\y) at (1.3*\y,0) {\y}; \node[circle,dashed] (5)
        at (5*1.3,0) {...};
        
        \foreach \x/\y in {1/2,2/3,3/4,4/5} \draw (\x) edge[->,bend
        left] node[above] {$1-p$} (\y) (\y) edge[->,bend left]
        node[below] {$p$} (\x);
        
        \draw (0) edge[->,bend left] node[above] {$1-p$} (1) (1)
        edge[->,bend left] node[below] {$p$} (0);
        \draw (0) edge[->, loop left] node[above] {$p$} (0); 
      \end{tikzpicture}}
    &\mpage{.35}{Is this chain:
      \begin{itemize}
      \item Transient ? 
      \item Positive recurrent ? 
      \item Null recurrent ? 
      \end{itemize}
      }
  \end{tabular}
  \pause

  \textbf{Answer} : it depends on $p$ :
  \begin{itemize}
  \item $p<1/2$ : transient
  \item $p>1/2$ : positive recurrent 
  \item $p=1/2$ : null recurrent 
  \end{itemize}
\end{frame}

\begin{frame}{Fundamental theorem}
  \begin{theorem}
    An irreducible and aperiodic Markov chain belongs to one of the
    following two classes:
    \begin{itemize}
    \item All states are transient or are null recurrent, in which
      case there is no stationary distribution.
    \item All states are positive recurrent, in which case the
      stationary distribution is the unique solution of
      \begin{align*}
        \pi P = \pi  \text{ and } \sum_{i}\pi_i = 1.
      \end{align*}
      In this case : $\pi_i=1/m_{ii}$ (defined on previous slide).
    \end{itemize}
  \end{theorem}
\end{frame}
\begin{frame}{Example}
  \begin{center}
    \mpage{.65}{\begin{tikzpicture}[auto,node distance=1.3cm,semithick,shorten
        >=1pt]
        \tikzstyle{noeud}=[circle,draw]; \foreach \y in {0,...,4}
        \node[noeud] (\y) at (1.3*\y,0) {\y}; \node[circle,dashed] (5)
        at (5*1.3,0) {...};
        
        \foreach \x/\y in {1/2,2/3,3/4,4/5} \draw (\x) edge[->,bend
        left] node[above] {$1-p$} (\y) (\y) edge[->,bend left]
        node[below] {$p$} (\x);
        
        \draw (0) edge[->,bend left] node[above] {$1-p$} (1) (1)
        edge[->,bend left] node[below] {$p$} (0);
        \draw (0) edge[->, loop left] node[above] {$p$} (0); 
      \end{tikzpicture}}
  \end{center}\bigskip
  
  Let $\rho=(1-p)/p$ and $\pi_i = \rho^i$. Then, if a stationary
  measure $\pi$ exists, it must satisfy
  \begin{itemize}
  \item $\pi P = \pi$. 
  \item $\sum_i \pi_i = \sum_i \rho^i = 1/(1-\rho)$. 
  \end{itemize}\bigskip
  
  Thus : if $\rho<1$, the chain is positive recurrent and the
  unique stationary measure satisfies $\pi_i=(1-\rho)\rho^i$. 
\end{frame}

\begin{frame}{Positive recurrent and ergodicity}
  \begin{theorem}
    A positive recurrent Markov chain satisfies the ergodic theorem,
    \emph{i.e.}, for any bounded function $f$: 
    \begin{align*}
      \underbrace{\lim_{n\to\infty}\frac1n \sum_{k=1}^n
      f(X_k)}_{\text{Time average}}= \underbrace{\sum_i
      \pi_if(i)}_{\text{ensemble average}}. 
    \end{align*}
  \end{theorem}
\end{frame}

\subsection{Continuous time Markov chains (CTMC)}

\begin{frame}
  \tableofcontents[currentsection,currentsubsection]
\end{frame}

\begin{frame}{Definition}
  A time-homogeneous continuous-time Markov chain (CTMC) is a
  stochastic process such that for all $t,s>0$ and all $i,j,x(u)$:
  \begin{align*}
    \Proba{X(t+s) = j \mid X(s)=i, X(u)=x(u),0\le u\le s} \\
    =\Proba{X(t+s) = j \mid X(s)=i}\\
    =\Proba{X(t) = j \mid X(0)=i}
  \end{align*}

  \pause\bigskip

  Let $(X_n)$ be a CTMC. There exists a matrix $Q$ such that for all
  $i\ne j$:
  \begin{align*}
    \lim_{dt\to0}\frac1{dt}\Proba{X(t+dt)=j \mid X(t)=i} = Q_{ij}    
  \end{align*}
  By convention, we write $Q_{ii}=-\sum_{j\ne i}Q_{ij}$.

  $Q$ is called the \textbf{generator} of the Markov chain. 
\end{frame}

\begin{frame}{Stationary distribution}
  A stationary distribution for the CTMC $\pi$ is such that :
  \begin{itemize}
  \item $\pi Q =0$ 
  \item $\sum_i \pi_i = 1$. 
  \end{itemize}
  \bigskip

  Except if the chain explodes in finite time, CTMC are essentially
  equivalent to DTMC.
\end{frame}

\begin{frame}{Where do we stand?} 
  \begin{itemize}
  \item Countable state-space makes things more complicated because
    the chain can ``go to infinity''.\bigskip
  \item Fundamental theorem : 
    \begin{itemize}
    \item Existence of a stationary distribution = positive recurrent
      = ergodic (= good case).
    \item No stationary distribution = unstable system (= bad case). 
    \end{itemize}\bigskip
  \item Continuous-time MC $\approx$ discrete-time MC
  \end{itemize}

  More details : see Harchol-Balter's book, Chapter $8$, $9$ and
  $12$. 
\end{frame}

\section{Markov Models of Queuing Networks}

\subsection{M/M/1 and M/M/k queues}

\begin{frame}{The M/M/1 queue}
  \begin{center}
    \includegraphics[width=.5\linewidth]{mm1}
  \end{center}
  \pause\bigskip
  
  Let $X_n$ be the number of jobs waiting in the queue. $X_n$ is a
  continuous time Markov chain whose generator is: 
  \begin{center}
    \includegraphics[width=.85\linewidth]{mm1_generator}
  \end{center}

\end{frame}

\begin{frame}{M/M/1 queue : the balance equations}
  \begin{center}
    \includegraphics[width=.7\linewidth]{mm1_generator}
  \end{center}
  
  The equation $\pi Q = 0$ means ``what goes out = what comes in''\pause
  \begin{align*}
    \mu\pi_1  &= \lambda\pi_0  & \text{(goes in = goes out of state 0)}\\
    \lambda \pi_0 + \mu \pi_2 &= (\mu+\lambda)\pi_1 &\text{(goes in =
                                goes out of state 0)} \\
    \vdots\\
    \lambda p_1 &= \mu \pi_2 & \text{(goes in = goes out of states \{0,1\})}
  \end{align*}
  \pause
  Solution : let $\rho = \lambda/\mu$. We have : 
  \begin{align*}
    \pi_i = \pi_0\rho^i.
  \end{align*}
\end{frame}

\begin{frame}{Stability of the M/M/1 queue}
  The exists a probability distribution on $\{0,1\dots\}$ such that
  $\pi_i = \pi_0\rho^i$ if and only if $\rho<1$. In which case:
  \begin{align*}
    \pi_i = (1-\rho)\rho^i.
  \end{align*}\pause

  The mean number of jobs in the system is:
  \begin{align*}
    \esp{N} &= \sum_{i=0}^\infty i \pi_i = \frac{\rho}{1-\rho}.
            % &=(1-\rho) \sum_{i=0}^\infty i\rho^i \\
            % &=\rho(1-\rho) \sum_{i=0}^\infty i\rho^{i-1}\\
            % &=\rho(1-\rho)
            %   \frac{\partial}{\partial\rho}\sum_{i=0}^\infty
            %   i\rho^{i-1}\\ 
            % &=\rho(1-\rho)
            %   \frac{\partial}{\partial\rho}\sum_{i=0}^\infty
            %   i\rho^{i-1}\\ 
  \end{align*}\pause
  The average waiting time is (by Little's Law):
  \begin{align*}
    \esp{T} = \frac{\esp{N}}{\lambda} = \frac{\rho/\lambda}{1-\rho} =
    \frac{1}{\mu-\lambda}. 
  \end{align*}
\end{frame}

\begin{frame}{Stability of the M/M/1 : mean waiting time}
  % f = figure(); x = linspace(0,1.5,1000); y = 1/(1.5-x); plot(x,y);
  % xlim([0,2]); ylim([0,10]);  xlabel('arrival rate $\lambda$');
  % ylabel('Average waiting time'); plot([1.5,1.5],[0,10],'--')
  % f.savefig('figs/mm1_waitingTime.png',bbox_inches='tight')
  Assume that $\mu=1.5$. 
  \begin{center}
    \includegraphics[width=.6\linewidth]{mm1_waitingTime}
  \end{center}
  
  \begin{itemize}
  \item Doubling the arrival rate \emph{more than} doubles the mean
    waiting time. 
  \item Doubling the server speed \emph{more than} halves the mean
    waiting time. 
  \end{itemize}
\end{frame}

\subsection{More Queues : M/M/K/K, Jackson Networks, \dots}
\begin{frame}
  \tableofcontents[currentsubsection]
\end{frame}

\begin{frame}{The M/M/k/k loss system}
  \only<1-2>{\begin{center}
      \includegraphics[width=.8\linewidth]{mmkk}
    \end{center}
  }
  \pause
  \begin{center}
    \includegraphics[width=.6\linewidth]{mmkk_generator}
  \end{center}
  \pause

  Balance equation ($\pi Q=0$) gives :
  \begin{align*}
    \pi_{i}\lambda = \pi_{i+1}(i+1)\mu
  \end{align*}
  which gives $\pi_i=\frac{\rho^i}{i!}\pi_0$.

  The blocking probability is the \emph{Erlang-B formula} : 
  \begin{align*}
    \pi_k = \frac{\rho^k/k!}{\sum_{i=0}^k \rho^i/i!}
  \end{align*}
  \textbf{Note : } this formula is insensitive to distribution of
  service times.
\end{frame}


\begin{frame}{Burke's theorem}
  \begin{center}
    \begin{tikzpicture}
      \node at (0,0) {\includegraphics[width=.6\linewidth]{mm1}};
      \node[fill=white,inner sep=1pt] at (.68,0) {$X_t$};
      \node at (4,0) {$Y_t$};
    \end{tikzpicture}
  \end{center}
  Consider the departure process $Y_t$.
  \bigskip
  
  \begin{theorem}[Burke's theorem]
    Assume that $\lambda<\mu$. Then, in steady-state,
    \begin{itemize}
    \item the departure process $Y_t$ is a Poisson process of
      intensity $\lambda$.
    \item at any time $t$, the number of jobs in the system ($X_t$) is
      independent of the sequence of departure times prior to $t$
      ($Y_s$, $s\le t$). 
    \end{itemize}

  \end{theorem}
\end{frame}
\begin{frame}{Application : Jackson network}
  Consider the following Markov chain:
  
  \begin{center}
    \includegraphics[width=.8\linewidth]{acyclic}
  \end{center}

  \begin{itemize}
  \item What is the stability condition? 
  \item Can you compute the stationary distribution? 
  \end{itemize}
\end{frame}
\begin{frame}{Answer \mpage{.3}{\includegraphics[width=\linewidth]{acyclic}}}
  
  \begin{itemize}
  \item The load is $\rho_1=\lambda/\mu_1$ at server $1$,
    $\rho_2=\lambda/(2\mu_2)$ at server $2$, $\rho_3=\lambda/(3\mu_3)$
    at server $3$, $\rho_4=\lambda/(3\mu_4)$ at server $4$ and
    $\rho_5=2\lambda/(2\mu_5)$ at server $5$.
  \item The stability condition is ``load < 1'' for all servers:
    \begin{align*}
      \lambda < \min(\mu_1,2\mu_2,2\mu_3,3\mu_4,2\mu_5/3). 
    \end{align*}\pause
  \item One can verify that the stationary distribution has a
    \alert{product form}:
    \begin{align*}
      \pi_{n_1,n_2\dots,n_5} &= \Proba{n_1 \text{ jobs at server
                               $1$},\dots n_5 \text{ at server $5$} }\\
                             &=\Proba{n_1 \text{ jobs at server
                               $1$}}\dots \Proba{n_5 \text{ at server
                               $5$} }\\
                             &= (1-\rho_1)\rho_1^{n_1}\dots
                               (1-\rho_5)\rho_5^{n_5}. 
    \end{align*}
  \end{itemize}

  The \alert{product form} is valid for much more general networks
  (Jackson networks, BCMP).
\end{frame}

\begin{frame}{Going further: a brief history of queuing theory}
  
  \begin{itemize}
  \item A.K. Erlang (1909)
  \item D. Kendall (53) -- Queuing theory (Kendall's notation)
  \item L. Kleinrock (60s-70s)
    \begin{center}
      \it \color{black!50} "Basically, what I did for my PhD research
      in 1961–1962 was to establish a mathematical theory of packet
      networks..."
    \end{center}
  \item Jackson networks (70s), BCMP networks (90)
  \item Approximations (90s,00s)
  \end{itemize}
  \bigskip
  
  Still used today in OR (telecommunication networks, hospitals,
  production lines,\dots)
\end{frame}

\section{Approximations}


\begin{frame}{We study models of interacting objects}
  %$K$ objects. \bigskip

  \begin{tikzpicture}
    \node at (6,-1) {\includegraphics[width=.4\linewidth]{bss}};
    \node at (7.5,-3) {object = station}; 
    \node at (0,-1) {\includegraphics[width=.4\linewidth]{cluster}};
    \node at (0,-3) {Cluster: object = server};
  \end{tikzpicture}
  \pause \vspace{.6cm}
  
  \centering \mpage{.65}{
    \begin{exampleblock}{}
      \centering Problem: state-space explosion
    \end{exampleblock}
  }
\end{frame}

\begin{frame}{State space explosion and decoupling method}
  \begin{tabular}{cc}
    \mpage{.3}{\includegraphics[width=\linewidth]{manySIR}\\
    $3^{13}\approx 10^6$ states.}
    &\mpage{.6}{
      We need to keep track of $S^K$ states
      \begin{equation*}
        \mathbb{P}(Z_1(t)=i_1,\dots,Z_n(t)=i_n)
      \end{equation*}
      }
  \end{tabular}
  \bigskip
  
  \pause
  \begin{alertblock}{The decoupling assumption is}
    \begin{equation*}
      \underbrace{\mathbb{P}(Z_1(t)=i_1,\dots,Z_n(t)=i_n)}_{S^{K}\text{
          variables}}
      \approx 
      \underbrace{\mathbb{P}(Z_1(t)=i_1)\dots\mathbb{P}(Z_n(t)=i_n)}_{K\times S \text{ variables}}
    \end{equation*}
  \end{alertblock}
\end{frame}

\begin{frame}{These models correspond to \alert{distributed} systems}
  {Each object interacts with the mass -- McKean–Vlasov process}

  \begin{center}
     \begin{tikzpicture}
       \node at (-1,0) (1) {\includegraphics[scale =
         0.5]{00AnObject.pdf}};
       \uncover<1>{\node at (5,0) (I) {\includegraphics[scale = 0.5]{02ManyObjects.pdf}};}
       \uncover<2>{\node at (5,0) (I) {\includegraphics[scale =
           0.5]{03ACloudOfManyObjects.pdf}};}
       \draw[<->,line width=2pt] (1) -- (I);
       \node[text width=1cm] at (5,0){\centering 
         \only<3>{\includegraphics[scale = 0.3]{Graph1.png}}
         \only<4>{\includegraphics[scale = 0.3]{Graph2.png}}
         \only<5>{\includegraphics[scale = 0.3]{Graph3.png}}\\
       };
       \only<3-5>{\node at (7,1){$X(t)$};}
       
     \end{tikzpicture}
     
   \end{center}
   \uncover<2->{We view the population of objects more abstractly,
     assuming that individuals are indistinguishable.}\\%
   \uncover<3->{ An \alert{occupancy measure} records the proportion of
     agents that are currently exhibiting each possible state.}
\end{frame} 


\begin{frame}{Example : the \alert{power of two-choice}}
  \begin{tabular}{cc}
    \mpage{.4}{
    Example: $K$ servers\\
    \includegraphics[width=\linewidth]{twoChoiceModel}}
    &\mpage{.55}{
      Let $S_i(t)$ be the number of servers with $i$ or more jobs. If
      we zoom on one server : 
      
    \begin{tikzpicture}[auto,node distance=1.3cm,semithick,shorten
      >=1pt,xscale=1.4]
      \tikzstyle{noeud}=[circle,draw]; \foreach \y in {0,...,3}
      \node[noeud] (\y) at (1.3*\y,0) {\y}; \node[circle,dashed] (4)
      at (4*1.3,0) {...};
    
      \foreach \x/\y in {0/1,1/2,2/3,3/4} \draw (\y) edge[->,bend
      left] node[below] {$1$} (\x);

      \draw (0) edge[->,bend left] node[above]
      {\tiny $\lambda(S_0(t)+S_1(t))$} (1);
      \draw (1) edge[->,bend left] node[above]
      {\tiny $\lambda(S_1(t)+S_2(t))$} (2);
      \draw (2) edge[->,bend left] node[above]
      {\tiny $\lambda(S_2(t)+S_3(t))$} (3);
      \draw (3) edge[->,bend left,dashed] (4);
    \end{tikzpicture}
      }
  \end{tabular}
  \bigskip
  
  Using this approximation, we can show that:
  \begin{align*}
    \esp{N} = \sum_{i\ge0}\rho^{2^i-1} + \frac{1}{2K(1-\rho)} + O(1/K^2)
  \end{align*}

  For $\lambda=0.9$: 
  \begin{tabular}{|c|c|c|c|}
    \hline
    &$K=1$&$K=10$&$K=\infty$\\\hline
    Mean number of jobs & $9$ & $2.8$ & $2.35$\\\hline
  \end{tabular}
\end{frame}

\begin{frame}{More on the subject}
  \begin{itemize}
  \item A class of mean field interaction models for computer and
    communication systems by M. Benaïm and J.-Y. Le Boudec
    \url{http://members.unine.ch/michel.benaim/perso/pe-mf-tr.pdf}
  \item Incentives and redistribution in homogeneous bike-sharing
    systems with stations of finite capacity by C. Fricker and N. Gast
    \url{https://hal.archives-ouvertes.fr/hal-01086009/document}
  \item Expected Values Estimated via Mean-Field Approximation are
    1/N-Accurate by N. Gast \url{https://hal.inria.fr/hal-01553133}
  \end{itemize}
\end{frame}

\section{Conclusion}
\begin{frame}{Where do we stand?}
  \begin{itemize}
  \item Queuing theory regroup a class of models to reason about
    system of interacting component that perform and exchange tasks.
    \bigskip
    
  \item Simple queuing systems can be modeled by Markov chains and
    continuous time Markov chains
    \begin{itemize}
    \item They allow to define stability 
    \item They are interesting because they give closed-form results. 
    \end{itemize}
    \bigskip
    
  \item Except for particular cases, often needs
    \emph{approximations}.
  \end{itemize}
  \bigskip
  
  \textbf{Next} : Markov decision processes.
  
\end{frame}

\end{document}
