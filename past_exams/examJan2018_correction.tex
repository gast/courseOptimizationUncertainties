\documentclass{article}

\usepackage{geometry}
\usepackage{amsmath,amsthm}
\usepackage{url}
\usepackage{hyperref}

\title{optimization under uncertainty : ideas for a correction} %
\author{Nicolas Gast} %
\date{Final exam, January 2018, Master ORCO -- Inria}

\begin{document}
\maketitle

\section{Exercice 1 : queuing with loops}

\begin{enumerate}
\item Let $\lambda+x$ be the input traffic of the first queue. If the
  first queue is stable, then the stability condition is
  $x<\mu_1$. Moreover, the output of this queue is $x$ (Burke's
  theorem). This shows that $(\lambda+x)(1-p)=x$ which shows that
  $x=\lambda(1-p)/p$. Hence, the total input in the first queue is
  $\lambda+x = \lambda/p$ and the total input in the second queue is
  $p(\lambda+x)=\lambda$.  Combining the two gives the stability
  conditions:
  \begin{align*}
    \lambda &< p\mu_1\\
    \lambda &< \mu_2
  \end{align*}
\item
  \begin{enumerate}
  \item $X_1$ is a continuous time Markov chain whose generator is the
    one of a M/M/1 queue with parameters $\lambda$ and $\mu_1p$.
  \item $X_2$ is not a Markov chain because the rate of transition
    from $X_2=0$ to $X_2=1$ depends on whether the queue $1$ is empty
    or not. 
  \end{enumerate}
\item $(X_1,X_2)$ is a continuous time Markov chain whose generator is
  the one of tandem queue with arrival rate $\lambda$ and service time
  $p\mu_1$ and $\mu_2$ (remember exercise $2$ of the exercise sheet
  \url{https://gitlab.inria.fr/gast/courseOptimizationUncertainties/blob/master/exercises/exo_queues.pdf}. Its
  stationary distribution is therefore a product form:
  \begin{align*}
    P(X_1=i,X_2=j) = (\rho_1)^i(\rho_2)^j(1-\rho_1)(1-\rho_2),
  \end{align*}
  where $\rho_1=\lambda/(p\mu_1)$ and $\rho_2=\lambda/\mu_2$. 
\item
  \begin{enumerate}
  \item The parameters $p=1/2$, $\lambda=1$ and $\mu_1=\mu_2=3$
    satisfy the stability conditions. Applying the formula of the
    M/M/1, the mean sojourn time in the system is 
    \begin{align*}
      \frac{1}{p\mu_1-\lambda} + \frac{1}{\mu_2-\lambda}%
      &= \frac{1}{1.5-1} + \frac{1}{3-1}\\
      &= 2 + 1/2
      &= 2.5
    \end{align*}
  \item With the parameters $p=2/3$, $\lambda=2$ and $\mu_1=3$, we get
    $\lambda /p = 3 = \mu_1$. Therefore the system is unstable. 
  \end{enumerate}
\end{enumerate}

\section{Optimal Gambling}

The problem can be viewed as a Markov decision process where the
states are $\{0,1,\dots,10\}$. In a state $x\in\{1,\dots,9\}$, the
set of possible actions are $u\in\{1,\dots\min(x,10-x)\}$.

We will first compute the value of two policies :
\begin{itemize}
\item Policy ``min'' : bet $\min(x,10-x)$. 
\item Policy ``max'' : bet $1$. 
\end{itemize}
For example, if one uses the policy $\max$ and is in state $X_t=4$,
then at the next time step, $X_{t+1}=8$ with probability $p$ and to
$X_{t}=0$ with probability $1-p$. With the policy $\min$,
$X_{t+1}=X_t+1$ with probability $p$ and $X_{t+1}=X_t-1$ with
probability $1-p$. In the reminder of the exercise, we will prove
that:
\begin{itemize}
\item When $p<1/2$, the policy $\max$ is optimal
\item When $p>1/2$, the policy $\min$ is optimal
\item When $p=1/2$, all policy are equivalent. 
\end{itemize}
Moreover, the winning probability (when starting in $4$) is:
\begin{align*}
  \left\{
  \begin{array}{ll}
     \frac{(2-p)p^2}{1-p^2(1-p)^2}&\text{ if $p<1/2$}\\
    \frac12&\text{ if $p=1/2$}\\
    \frac{1-(p/(1-p))^5}{1-(p/(1-p))^{10}} & \text{ if $p>1/2$}
  \end{array}
  \right.
\end{align*}

\subsection{Approach 1 : ``convexity / concavity''}

The idea of this approach is to show that
\begin{itemize}
\item if $p\le1/2$ and $V$ is a convex value function, then one
  iteration of the algorithm ``value iteration'' produces again a
  convex value function 
\item if $p\ge1/2$, the same holds for a concave value function.
\end{itemize}


\subsection{Approach 2 : ``brute-force''}

An ``easier'' but more computationally intensive approach is to
compute the value function and then show that this function is convex
or concave.

\subsubsection{Case $p>1/2$}
\textbf{Step 1. Computation of the value function}. Let us first
compute the value function $V_{\min}$ of the policy $\min$. This
function satisfies:
\begin{align*}
  V_{\min} (i) = pV_{\min}(i+1) + (1-p)V_{\min}(i-1),
\end{align*}
with the border conditions $V_{\min}(0)=0$ and $V_{\min}(10)=1$.

It can be shown that the only solution (when $p\ne1/2$) of the above
equation is
\begin{align*}
  V_{\min}(i) = \frac{1-(\frac{p}{1-p})^i}{1-(\frac{p}{1-p})^{10}}. 
\end{align*}

\textbf{Step 2. Why is the policy $\min$ optimal when $p>1/2$?}  The
idea is to apply one step of value iteration with the initial
condition $V_{\min}$. We get:
\begin{align*}
  &\max_{u\in\{1,\dots,\min(x,u)\}} p V_{\min}(x+u) + (1-p) V_{\min}(x+u)
  \\
  &=\max_{u\in\{1,\dots,\min(x,u)} p
    \frac{1-(p/(1-p))^{x-u}}{1-(p/(1-p))^{10}} +
    \frac{1-(p/(1-p))^{x+u}}{1-(p/(1-p))^{10}}\\
  &=\max_{u\in\{1,\dots,\min(x,u)}
    \frac{1}{1-(p/(1-p))^{10}}\left( 1 - (\frac{p}{1-p})^x(1-p)\left[ (\frac{p}{1-p})^{1-u} +
     (\frac{p}{1-p})^{u}\right] \right)
\end{align*}
Let $x:=p/(1-p)>1$.  The above quantity is maximized when
$x^{1-u}+x^u$ is minimized, which occurs when $u=1$.

This shows that the policy ``$\min$'' cannot be improved and therefore
is the optimal policy. 

\subsubsection{Case $p=1/2$}

One can easily verify that the value function of any policy is
$V(i) = i/10$.

\subsubsection{Case $p<1/2$}

For $V_{\max}(x)$ the computation is more complicated. We have:
\begin{align*}
  V_{\max}(1) = p V_{\max}(2) + (1-p) V_{\max}(0) = p V_{\max}(2) \\
  V_{\max}(2) = p V_{\max}(4)\\
  V_{\max}(3) = p V_{\max}(6)\\
  V_{\max}(4) = p V_{\max}(8)
  V_{\max}(5) = p \\
  V_{\max}(6) = p + (1-p) V_{\max}(2)\\
  V_{\max}(7) = p + (1-p) V_{\max}(4)\\
  V_{\max}(8) = p + (1-p) V_{\max}(6)\\
  V_{\max}(9) = p + (1-p) V_{\max}(8)
\end{align*}
This gives:
\begin{align*}
  V_{\max}(4) &= p V_{\max}(8) = p^2 + p(1-p)V_{\max}(6)\\
              &= p^2 + p(1-p)( p + (1-p)V_{\max}(2))\\
              &= p^2 + p^2(1-p) + p^2(1-p)^2 V_{\max}(4),
\end{align*}
which implies
\begin{align*}
  V_{\max}(4) &= p^2\frac{2-p}{1-p^2(1-p)^2}
\end{align*}
Using the above equation, we get :
\begin{align*}
  V_{\max}(1) &= p^4\frac{2-p}{1-p^2(1-p)^2}\\
  V_{\max}(2) &= p^3\frac{2-p}{1-p^2(1-p)^2}\\
  V_{\max}(6) &= p + (1-p)p^3\frac{2-p}{1-p^2(1-p)^2}\\
  V_{\max}(3) &= p^2 + (1-p)p^4\frac{2-p}{1-p^2(1-p)^2}\\
  V_{\max}(7) &= p + (1-p)p^2\frac{2-p}{1-p^2(1-p)^2} \\
  V_{\max}(8) &= p\frac{2-p}{1-p^2(1-p)^2}\\
  V_{\max}(9) &= p + (1-p) p\frac{2-p}{1-p^2(1-p)^2}
\end{align*}

One then has to show that the policy ``$\max$'' cannot be improved
when $p<1/2$. 


\end{document}
