 \input{latex_entete}

\title{Master ORCO : Optimization under uncertainty : Markov Decision Processes}%
\author{Nicolas Gast}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}{Outline}
  \tableofcontents
\end{frame}

\begin{frame}{References}
  \centering 
  \begin{tabular}{ccc}
    \includegraphics[width=.3\linewidth]{puterman}
    &\includegraphics[width=.3\linewidth]{bertsekas}
    &\includegraphics[width=.3\linewidth]{book_suttonBarto}\\
    Very detailed&More modern / quicker. & From MDP to RL\\
    Only about MDPs & Lot about approximation & Not very formal
  \end{tabular} 
\end{frame}

\begin{frame}{Remainder : two-stage problems}
  \begin{tabular}{cc}
    \mpage{.4}{\includegraphics[width=\linewidth]{twoStage}}
    &\mpage{.5}{
      \begin{align*}
        \min_U \esp{L(\xi,U)}
      \end{align*}
      \begin{itemize}
      \item $U=(U_0,U_1)$ : control
      \item $\xi$ : noise
      \end{itemize}
      $2$ stage decision : 
      \begin{align*}
        U_0 \leadsto \xi \leadsto U_1
      \end{align*}
      }
  \end{tabular}
\end{frame}
\begin{frame}{Can we extend it to multi-stage problem? }
  \begin{tabular}{cc}
    \mpage{.4}{\includegraphics[width=\linewidth]{multiStage}}
    &\mpage{.5}{
      \begin{align*}
        \min_U \esp{L(\xi,U)}
      \end{align*}
      \begin{itemize}
      \item $U=(U_0\dots U_T)$ : control
      \item $\xi=(\xi_1\dots \xi_T)$ : noise
      \end{itemize}
      $T$ stage decision : 
      \begin{align*}
        U_0 \leadsto \xi_1 \leadsto U_1 \dots \leadsto \xi_T
        \leadsto U_T 
      \end{align*}
      }
  \end{tabular}
  \pause 
  \centering
  \mpage{.95}{
    \begin{exampleblock}{Non-anticipativity constraint}
      The control at time $t$ can only depend on the information at
      time $t$:
      \begin{align*}
        U_t = \pi_t(\xi_1,\dots,\xi_t). 
      \end{align*}
    \end{exampleblock}
  }
\end{frame}

\begin{frame}{}%Markov decision processes}
  \begin{tabular}{cc}
    \mpage{.8}{Problem of the scenario decomposition approach : the
    number of scenarios grows exponentially with the number of
    stages.}
    &\mpage{.15}{\includegraphics[width=\linewidth]{multiStage}}
  \end{tabular}
  \pause\bigskip
  
  \begin{exampleblock}{\textbf{MDP approach} : compress the information
      inside a state}
    A discrete time controlled stochastic system is defined by its
    dynamics:
    \begin{align*}
      X_{t+1} = f( X_t, U_t, \xi_{t+1}),
    \end{align*}
    and initial state $X_0 = x_0$, where $\xi_{t+1}$ is an
    \emph{i.i.d.} sequence.
    
    We will consider the policies of the form:
    \begin{align*}
      U_t = \pi_t(X_t). 
    \end{align*}
  \end{exampleblock}

\end{frame}

\section{Markov decision processes}

\begin{frame}{A Markov decision process in brief}
  \begin{center}
    \includegraphics[width=.5\linewidth]{mdp_schema}
  \end{center}

  Agent : 
  \begin{itemize}
  \item Observes a state $X_t\in\calS$;
  \item Takes an action $A_t$ according to some policy;
  \item Gets a (possibly random) reward $R_{t+1}$;
  \item Moves to a (possibly random) next state $X_{t+1}$.
  \end{itemize}\bigskip
  
  \begin{equation*}
    X_t \mapsto A_t \mapsto R_{t+1} \mapsto X_{t+1} \mapsto
    A_{t+1} \mapsto \dots 
  \end{equation*}
\end{frame}

\begin{frame}{Definition of Markov decision processes (MDP)}

  If the evolution of the process has the \emph{Markov property}, we
  can define a \textbf{Markov decision process} (MDP).
  \begin{itemize}
  \item If the state space is finite, it is a finite MDP. 
  \end{itemize}\pause

  \begin{exampleblock}{To define an MDP, you need: }
    \begin{itemize}
    \item State and action spaces. 
    \item The evolution is described by a family of transition
      matrices:
      \begin{align*}
        P(s',r | s,a) = \Proba{X_{t+1} = s' \land R_{t+1} = r| X_t = s, A_t
        = a}
      \end{align*}
    \end{itemize}
  \end{exampleblock}\pause 
  We often define decompose these into two functions : 
  \begin{align*}
    P(s' \mid s, a) &= \Proba{X_{t+1} = s' \mid X_t = s, A_t
                 = a} \\
    R(s, a) &= \esp{ R_{t+1} \mid X_t=s, A_t=a}
  \end{align*}
\end{frame}

\begin{frame}{Policy and objective}
  %\begin{exampleblock}{
  A \emph{markovian} policy is a function that maps state to actions
  \begin{align*}
    \pi_t(a\mid s) = \Proba{A_t = a \mid X_t = s}
  \end{align*}
  By abuse of notation, if a policy is deterministic, then we often
  denote by $\pi_t(s)$ the action $s$ that is such that
  $\pi_t(a\mid s)=1$.
  
  \bigskip \pause
  
  The goal of the agent is to maximize an expected reward : 
  \begin{center}
    \mpage{.5}{
      \begin{exampleblock}{}
        \begin{equation*}
          \max_{\pi}\quad\esp{\sum_{t=1}^T \gamma^t R_t\mid A_{t:\infty}\sim \pi},
        \end{equation*}          
      \end{exampleblock}
    }
  \end{center}
  where $\gamma\in[0,1]$ and $T=\{1,2\dots\}\cup\{\infty\}$. 
\end{frame}

\begin{frame}{Example: inventory problem.}
  \begin{itemize}
    \item You own a warehouse that stores identical products.
    \item On day $t$, the demand is equal to $D_t$. It equals $d$ with probability $p_d$. You gain $1$ euro per sold product. 
    \item At the end of each day, you can order product. If you order $n$ products, it costs you $a + b n$ euros.  
  \end{itemize}
  You want to maximize the average reward. \bigskip

  Exercise: Formulate the problem as a MDP. 
  \begin{itemize}
    \item What are states, rewards, objective function? 
  \end{itemize}
\end{frame}

\section{Policy evaluation}

\begin{frame}{Gain and computation of rewards}
  We define the gain starting from time $t+1$ as $G_t$:
  \begin{equation*}
    G_t = R_{t+1} + \gamma R_{t+2} + \dots + \gamma^T R_T.
  \end{equation*}
  
  \begin{center}
    \mpage{.7}{%
    \begin{exampleblock}{The \alert{fundamental} equation is}
      \centering $\displaystyle G_t = R_{t+1} + \gamma G_{t+1}$. 
    \end{exampleblock}
  }
  \end{center}
\end{frame}


\begin{frame}{Value functions are defined as the expected returns}
  
  Given a policy $\pi$, its value starting in $s$ is the expected
  reward:
  \begin{align*}
    v_{\pi,t}(s) = \esp{ G_t \mid X_t = s, A_{t:\infty}\sim \pi}. 
  \end{align*}\pause
  The optimal value of a state is
  \begin{align*}
    v_{*,t}(s) = \max_\pi v_{\pi,t}(s). 
  \end{align*}
  A policy $\pi$ is optimal if and only if $v_{\pi,t}(s)=v_{*,t}(s)$
  for all $s$ and $t$. 
\end{frame}

\begin{frame}{How can we compute the value function of a policy? }
  Recall the fundamental equation : 
  \begin{align*}
    G_t = R_{t+1} + \gamma G_{t+1}. 
  \end{align*}\pause 
  
  Hence :
  \begin{align*}
    v_{\pi,t}(s)
    &= \esp{G_{t} | X_t=s \land A\sim\pi}\\
    &= \esp{ R_{t+1} + \gamma G_{t+1} | X_t=s \land A\sim\pi}\\
    &\uncover<3->{
      = \esp{ R_{t+1} | X_t=s \land A_t=\pi_t(s)} \\
    &\quad + \gamma
      \esp{ R_{t+1} | X_{t+1}=s'\land A\sim\pi} \Proba{X_{t+1}=s' |
      X_t=s \land A_t=\pi_t(s)}} 
  \end{align*}
  \pause\pause

  \begin{exampleblock}{Bellman's equation}    
    Hence, assuming that $a=\pi_t(s)$, $v_\pi$ satisfies : 
    \begin{align*}
      v_{\pi,t}(s) = \sum_{s'\in\calS} P(s',r|s,a)\left(
      r + \gamma v_{\pi,t+1}(s')\right). 
    \end{align*}
  \end{exampleblock}
  
  % This is a \alert{particular case of Bellman's equation}. 
\end{frame}

%\subsection{Finite horizon v.s. infinite discounted}

\begin{frame}{Finite horizon and discounted problems}
  Recall that we defined the discounted reward as : 
  \begin{align*}
    G_t = R_{t+1} + \gamma R_{t+2} + \gamma^2 R_{t+3}+\dots
    \text{\small (up to time $T$)}
  \end{align*}
 
  In what follows, we will assume that either
  \begin{itemize}
  \item $\gamma=1$ and $T<\infty$ in which case we talk of
    \emph{finite-horizon} problems {\small(here, the reward and
      transition often depend on time).}
  \item $\gamma<1$ and $T=\infty$ in which case we talk of
    \emph{discounted infinite horizon} {\small(here, the reward and
      transition are often time-independent).}
  \end{itemize}
  \pause \bigskip
  
  \begin{exampleblock}{One of the main difference is that : }
    \begin{itemize}
    \item When $T<\infty$, we look for time-dependent policies
    \item When $T=\infty$, we look for time-independent policies.
    \end{itemize}
  \end{exampleblock}
\end{frame}

\begin{frame}{Algorithmic implementation}

  \textbf{1.} \emph{Finite-horizon} policy evaluation -- By abuse of
  notation, write $r_{\pi,t+1}$ be the vector whose $s$th component is
  $\esp{R_{t+1}|S_t=s,A_t=\pi_t(s)}$ and $P_{\pi,t}$ the transition
  matrix whose $(s,s')$ entry is $\Proba{s'| s,\pi_t(s)}$.
  
  Then the vector $v_{\pi,t}(.)$ satisfies
  \begin{align*}
    v_{\pi,t} = r_{\pi,t+1} + \gamma P_{\pi,t} v_{\pi,t+1},
  \end{align*}
  \pause
  
  \textbf{2.} \emph{Discounted-infinite-horizon} When policies and
  transition are time-independent, then the vector $v_{\pi}(.)$
  satisfies
  \begin{align*}
    v_{\pi} = r_{\pi} + \gamma P_{\pi} v_{\pi},
  \end{align*}\pause
  which can be solved as :
  \begin{align*}
    v_{\pi} = (I-\gamma P_{\pi})^{-1}r_{\pi}.
  \end{align*}
\end{frame}


\begin{frame}{Exercise : inventory problem}
  Recall that the demand at day $t$ is $D_t$, the selling price is $1$ and the cost to buy $n$ copies is $a+bn$. 
  \begin{itemize}
    \item Suppose that $D_t\in\{1, 2\}$ (with equal probability), $a=1$ and $b=0.4$. 
  \end{itemize}

  We consider the policy that order $2$ items as soon as the stock is empty. 
  \begin{itemize}
    \item Write a Markov chain corresponding to this problem. 
    \item Assume that $T=2$ (and $\gamma=1$). What is the value function ?
    \item Assume that $T=\infty$ and $\gamma=0.5$. What is the value function ? 
  \end{itemize}
  Is this policy optimal? 
\end{frame}

\section{Finite horizon problem: Bellman's equation}

\begin{frame}{Bellman's equation for finite-horizon problem}
  Recall that for a given policy $\pi$, we have : 
  \begin{align*}
    v_{\pi,t}(s) = \esp{R_{t+1} + \gamma v_{\pi,t+1}(S_{t+1}) \mid 
    S_t=s,A_t\sim \pi_t(s)}
  \end{align*}\pause 
  
  \begin{exampleblock}{}
    Let $v_{*,t}(s)=\sup_{\pi} v_{\pi,t}(s)$. The optimal value
    function satisfies
    \begin{align*}
      v_{*,t}(s) &= \max_a \esp{ R_{t+1} + \gamma v_{*,t+1}(S_{t+1}) \mid  
                   S_t=s,A_t=a} \\
                 &=\max_a \sum_{s',r}(r+\gamma
                   v_{*,t+1}(s'))P(s',r|s,a),
    \end{align*}
    with $v_{*,T}(s)=0$.
  \end{exampleblock}
  Note :
  \begin{itemize}
  \item This equation is \alert{fundamental}! You have to
    \emph{understand} it.
  \item The proof is direct by induction of $t$ (starting from $T$).
  \end{itemize}

\end{frame}

% \begin{frame}{Where does this comes from? }
%   \begin{tabular}{cc}
%     \mpage{.35}{\centering \includegraphics[width=\linewidth]{bellman}

%     Richard
%     Bellman (1920--1984), inventor of dynamic programming}
%     &\mpage{.6}{
%       \alert{Bellman's principle of optimality}: An optimal policy has
%       the property that 
%       whatever the initial state and initial decision are, the
%       remaining 
%       decisions must constitute an optimal policy with regard to the state
%       resulting from the first decision. (See Bellman, 1957,
%       Chap. III.3)

%       \begin{center}
%         \mpage{.7}{\centering
%           \includegraphics[width=\linewidth]{shortestPath}}
%       \end{center}
%       }
%   \end{tabular}
% \end{frame}

% \begin{frame}{Illustration of Bellman's principle of optimality}
%   \begin{tabular}{cc}
    
%     &\mpage{.6}{
%       Suppose that the shortest path from LA to Boston passes through
%       Chicago.\bigskip

%       The principle of optimality states that the portion of this path
%       from Chicago to Boston is the shortest path from Chicago to
%       Boston. 
%       }
%   \end{tabular}
% \end{frame}

\begin{frame}{Why is Bellman's equation that important?}
  
  A policy that is greedy with respect to the value function is
  optimal :
  \begin{align*}
    \pi_{*,t}(s) &\in \argmax_a  \underbrace{R(s,a)}_{\text{instantaneous reward}}
    +\underbrace{\gamma \sum_{s'}v_{*,t+1}(s'))P(s'\mid s,a)}_{\text{"reward to go"}},
  \end{align*}
  
  By knowing $V$, optimal policies can be constructed by only looking
  at \alert{one step ahead}.
  \bigskip

  \begin{itemize}
  \item When the problem is small enough, $V$ can be computed
    backward. 
  \item When the problem is too large, find an approximation of $V$.
  \end{itemize}
\end{frame}

\begin{frame}{Value of a state v.s. value of a state-action}

  It is often useful to use the value of a state-action (denoted $Q$): 
  \begin{align*}
    Q_{*,t}(s,a) &= R(s,a) + \gamma \sum_{s'}v_{*,t+1}(s'))P(s'\mid s,a),\\
    v_{*,t}(s) &= \max_a Q_{*,t}(s,a)
  \end{align*}
  
\end{frame}

\begin{frame}{Exercise}
  (circle nodes = states. \textbullet-nodes = actions. Dashed arrow
  indicate actions that you can take from a given state).\bigskip
  
  
  \begin{tikzpicture}[scale=1.3]
    \tikzstyle{state}=[circle,draw,inner sep=0pt,minimum size=.6cm]
    \tikzstyle{proba}=[pos=0.3,inner sep=1pt,fill=white]
    \foreach \x/\y in {0/a,2/b,4/c,6/d}{
      \node[state] at (\x,0) (t\x) {\y};
      \node[state] at (\x,-1) (b\x) {\y'};
      \node[circle,fill] at (\x+0.8,0) (at\x) {};
      \node[circle,fill] at (\x+0.8,-1) (ab\x) {};
      \draw[dashed] (t\x) edge[->] (at\x);
      \draw[dashed] (t\x) edge[->] (ab\x);
      \draw[dashed] (b\x) edge[->] (ab\x); 
    }
    \node[circle,fill] at (-1,0) (at-2) {};
    \node[circle,fill] at (-1,-1) (ab-2) {};
    \foreach \x/\y in {-2/0,0/2,2/4,4/6}{
      \draw (at\x) edge[->] node[above]{\tiny $0.5$, +0} (t\y);
      \draw (at\x) edge[->] node[proba]{\tiny $0.5$, +0} (b\y);
      \draw (ab\x) edge[->] node[proba]{\tiny $0.1$, +1} (t\y);
      \draw (ab\x) edge[->] node[below]{\tiny $0.9$, +1} (b\y);
    }
    \node[state] at (-1.5,-.5) (S) {S};
    \node[state] at (7.5,-.5) (D) {D};
    \draw[dashed] (S) edge[->] (at-2) (S) edge[->] (ab-2);
    \draw (at6) edge[->] node[proba]{\tiny$1,+10$} (D);
    \draw (ab6) edge[->] node[proba]{\tiny$1,+0$} (D);
  \end{tikzpicture}
  \bigskip
  
  You want to maximize your expected total reward while going from $S$
  to $D$.
  \begin{itemize}
  \item Computes the optimal functions $V$ and $Q$. 
  \item What is the optimal policy? 
  \end{itemize}
\end{frame}

\begin{frame}{Algorithmic resolution 1: backward induction}
  To solve the previous example, we applied a simple algorithm: 
  \begin{itemize}
  \item $v_T = $ final reward (\emph{e.g.}, 0)
  \item For $t$ from $T-1$ to $0$, do:
    \begin{itemize}
    \item For each state $s\in\calS$, do:
      \begin{align*}
        v_{t}(s) = \max_{a} \sum_{r,s'}\left( r + 
        v_{t+1}(s')\right)P(s',r|s,a).
      \end{align*}
    \end{itemize}
  \end{itemize}

  \begin{center}
    Complexity : $O(T \times |\calS| \times |\calA| \times |noise|)$.
  \end{center}
  Works well for small problems\pause{} but one often talks of the
  \red{three curses of dimensionality} because the algorithm is linear
  in :
  \begin{itemize}
  \item The size of the state space
  \item The number of control variables
  \item The noise
  \end{itemize}
\end{frame}

\begin{frame}{Algorithmic resolution 2: linear programming}
  Note: the problem can also be solved by linear programming. 
  \begin{align*}
    v_0(s_0) =& \max_\rho \sum_{t=1}^T R(s,a)\rho_{s,a,t}\\
    \text{such that }&\forall s, t: \sum_{s'}\rho_{s',a,t}P(s|s',a) = \sum_{a} \rho_{s,a,t+1}\\
    &\forall s\ne s_0, a: \rho_{s,a,0} = 0\\
    &\sum_{a}\rho_{s_0,a,0} = 1\\
    &\forall s,a,t: \rho_{s,a,t}\ge0.
  \end{align*}
  where $\rho_{s,a,t} = \Proba{X_t=s\land A_t=a}$. 
\end{frame}

\begin{frame}{Illustration of the curse of dimensionality : the dam example}{(Or an inventory problem with multiple products).}
  \begin{tabular}{cc}
    \mpage{.4}{\includegraphics[width=\linewidth]{dam}}
    &\mpage{.55}{We are in dimension $5$ ($5$ dams) with $52$ time
      steps (weeks) plus $5$ control and $5$ independent noise.

      If we discretize our problem: 
      \begin{itemize}
      \item Discretization of each dam in $100$ levels :
        $|\calS|=100^5=10^{10}$. 
      \item Discretization of each control in $100$ values :
        $|\calU|=100^5=10^{10}$.
      \item Say that there are 10 possible values for the noise.
      \end{itemize}
      Total : $52\times 10^{10}\times 10^{10}\times 10 = 10^{23}$
      operations...
      }
  \end{tabular}
  \vfill
  
  {\tiny (credit: Vincent Leclère
    \url{http://cermics.enpc.fr/~leclerev/})}
\end{frame}

\begin{frame}{A classical example of finite horizon problem are
    optimal stopping problems}

  Two exercises : 
  \begin{itemize}
  \item Wheel of fortune
  \item ``Secretary'' problem 
  \end{itemize}
\end{frame}

\begin{frame}{Where do we stand?}
  We saw Bellman's equation : 
  \begin{align*}
    v_t(x) &= \min_{a}
             \esp{\underbrace{R_{t+1}}_{\text{instantaneous
             reward}} +
             \underbrace{v_{t+1}(S_{t+1})}_{\text{future cost}} \mid 
             S_t=s,A_t=a}
  \end{align*}
  This equation is \alert{fundamental} (recall that this the result
  that you \emph{have to understand and remember} from this course).
  \bigskip
  
  \begin{itemize}
  \item In what follows, we will see variations of this equation and
    method to solve it or some variations.
    \begin{itemize}
    \item Dynamic programming and value iteration. 
    \item Policy iteration. 
    \item Learning 
    \end{itemize}
  \item It is at the core of many optimization tools (reinforcement
    learning, games, finance,\dots)
  \end{itemize}
\end{frame}

% \begin{frame}{Examples}
%   Wheel of fortune :
%   \begin{align*}
%     \calS &=\{1,\dots,10\}\\
%     P^{t}_{i,j,u} &= \frac1{10}\\
%     \calU(x)&=\{\text{stop},\text{continue}\}.\\
%     c^t_{i,u}&=100i\\
%     K_i &=100i.
%   \end{align*}

%   Inventory problem (news-vendor in which you can buy more during the
%   day) : Stock of newspaper = $X_t$, order = $U_t$, demand =
%   $D_t$. Buying cost = $b$, selling cost = $s$.  Maximum that you can
%   carry : $M$.
%   \begin{align*}
%     X_{t+1} &= \max( X_t+U_t-D_t,0)\\
%     \calU(x)&=\{0,1,\dots,M-x\}\\
%     c^t_{x,u}&= b u - s \esp{\min(x+u,D_t)}\\
%     K_x &= 0. 
%   \end{align*}
% \end{frame}


% \begin{frame}{Exercise}
%   You own a quantity of money $X_0\in\{1,\dots,K-1\}$. Each time, you
%   can bet a quantity smaller or equal to $X_t$. If you bet $u$:
%   \begin{itemize}
%   \item with probability $p\in(0,1)$, you win $u$ : $X_{t+1}=X_t+u$
%   \item with probability $1-p$, you loose $u$: $X_{t+1}=X_t-u$. 
%   \end{itemize}
%   You goal is to maximize the probability of attaining $K$ knowing
%   that you can play at much $T$ times.  How do you play?

%   \bigskip
%   \begin{itemize}
%   \item Formulate the problem has an MDP
%   \item Write down Bellman's equation
%   \item We assume that $p=1/2$, $K=8$ and $T=10$. What is your
%     strategy? 
%   \item Restart the question with $p=1/3$ and $p=2/3$.
%   \end{itemize}
% \end{frame}


\section{Infinite-horizon problems}

\subsection{Discounted problems}

\begin{frame}{Outline}
  \tableofcontents[currentsubsection]
\end{frame}

\begin{frame}{Bellman's equation for infinite-horizon discounted
    problem}
  In this part, we assume that all parameters are time-invariant and
  that $\gamma<1$.
  
  Recall that for a given policy $\pi$, we have : 
  \begin{align*}
    v_{\pi}(s) = \esp{R_{t+1} + \gamma v_{\pi}(S_{t+1}) \mid 
    S_t=s,A_t\sim \pi(s)}
  \end{align*}\pause 
  
  \begin{exampleblock}{}
    Let $v_{*}(s)=\sup_{\pi} v_{\pi}(s)$ be the optimal value
    function. It satisfies
    \begin{align*}
      v_{*}(s) &= \max_a \esp{ R_{t+1} + \gamma v_{*}(S_{t+1}) \mid
                 S_t=s,A_t=a} \\
               &=\max_a \underbrace{R(s,a)+\gamma
                 v_{*}(s')P(s'|s,a),}_{=:Q_*(s,a)}.
    \end{align*}
  \end{exampleblock}

  Note :
  \begin{itemize}
  \item This equation is a \emph{fixed point} equation. 
  \item It has a unique solution.
  \end{itemize}
\end{frame}

\begin{frame}{How to find $\pi_*$? Idea 1: linear programming}
  \begin{align*}
    \max_{v} & (anything)\\
    \text{such that }&\forall s, a: v_s \ge Q_{s,a}\\
    &\forall s, a: Q_{s,a} = R_{s,a} + \sum_{s'} v_{s'}P(s'\mid s,a).
  \end{align*}
\end{frame}

\begin{frame}{How to find $\pi_*$? Idea 2: value / policy iteration}
  Idea : proceed by iterations :
  \begin{align*}
    \pi_0 \underbrace{\leadsto}_{\text{evaluation}} v_0
    \underbrace{\leadsto}_{\text{policy improvement}} \pi_1
    \underbrace{\leadsto}_{\text{evaluation}} v_1
    \underbrace{\leadsto}_{\text{policy improvement}} \pi_2  \leadsto \dots 
  \end{align*}
  \pause\bigskip
  
  We define two operators $T_\pi$ and $T_*$ that associate to a value
  function $v$ :
  \begin{align*}
    T_\pi v &= r_\pi + \gamma P_{\pi} v \\
    T_* v &= \argmax T_\pi v &\text{(where $\argmax$ = component
                               wise)}.
  \end{align*}

  The optimal value function $v_*$ is the (unique) solution of
  \begin{align*}
    v_* = T_* v_* 
  \end{align*}
  Remark :
  \begin{itemize}
  \item $T_\pi$ is a linear operator but not $T_*$.
  \item $T_\pi$ and $T_*$ are monotone and contracting. 
  \end{itemize}

\end{frame}

\begin{frame}{Value iteration : a first algorithm}
  It should be clear that for a given value function $v$, it is easy
  (numerically) to compute $T_\pi v$ and $T_*v$:
  
  \begin{align*}
    (T_*v)(s) = \max_a \sum_{s',r} \left(r+\gamma v(s')\right)P(s',r|s,a)
  \end{align*}
  \pause
  
  \begin{exampleblock}{Value iteration}
    Start with any $v_0$ (for example $v_0=0$)

    For $k=1,2,3,\dots$, repeat 
    \begin{itemize}
    \item $v_{k} := T_* v_{k-1}$
    \end{itemize}
    while $\norm{v_{k} - v_{k-1}}\ge \varepsilon$.
  \end{exampleblock}
\end{frame}

\begin{frame}{Asymptotic convergence of Value iteration}
  \begin{theorem}
    \begin{itemize}
    \item After $k$ iterations :
      \begin{align*}
        |v_k - v_*| \le \gamma^k | v_0-v_*|
      \end{align*}
    \item If $v$ is the output of the algorithm, then
      \begin{align*}
        |v_*-v|\le \frac{\varepsilon \gamma}{1-\gamma}
      \end{align*}
    \end{itemize}
  \end{theorem}
  
  (proof : Bertsekas proposition 2.2.1).
  
\end{frame}

\begin{frame}{Illustration of the algorithm}
  {VI is one of the simplest algorithm to implement}

  \only<1>{
    \begin{tabular}{cc}
      \mpage{.45}{\includegraphics[width=\linewidth]{setting_gridRW}}
      &\mpage{.45}{You want to go from $0$ to $D$ while avoiding $T$. 
        \begin{itemize}
        \item Each move costs $1$. 
        \item Discount is $\gamma=0.95$.
        \item Actions = $\{N,S,E,W\}$.
          \begin{itemize}
          \item You go in the direction of the action with probability
            $p=0.7$
          \item Otherwise, you move randomly.
          \item Ending in the trap costs $2$ forever. %/(1-\gamma)$. 
        \end{itemize}
      \end{itemize}
        }                                                      
    \end{tabular}
  }
  
  \only<2>{\includegraphics[width=\linewidth]{gridValueIteration_0}\\
    Iteration $0$}%
  \only<3>{\includegraphics[width=\linewidth]{gridValueIteration_1}\\
    Iteration $1$}%
  \only<4>{\includegraphics[width=\linewidth]{gridValueIteration_2}\\
    Iteration $2$}%
  \only<5>{\includegraphics[width=\linewidth]{gridValueIteration_3}\\
    Iteration $3$}%
  \only<6>{\includegraphics[width=\linewidth]{gridValueIteration_5}\\
    Iteration $5$}%
  \only<7>{\includegraphics[width=\linewidth]{gridValueIteration_10}\\
    Iteration $10$}%
  \only<8>{\includegraphics[width=\linewidth]{gridValueIteration_25}\\
    Iteration $25$}%
  \only<9>{\includegraphics[width=\linewidth]{gridValueIteration_50}\\
    Iteration $50$}%
  
  
\end{frame}

\begin{frame}{Policy iteration}
  Value iteration only converges to asymptotically.

  \begin{exampleblock}{Policy iteration}
    Start with any initial $v_0$.

    For $k=1,2,3,\dots$, repeat :
    \begin{itemize}
    \item $\pi_k\in \argmax T_{\pi} v_{k-1}$. 
    \item Let $v_k$ be the solution of $v=T_{\pi_k}v$. 
    \end{itemize}
    while $v_k\ne v_{k-1}$. 
  \end{exampleblock}\bigskip
  
  The equation $v=T_{\pi}v$ is $v=r_\pi+\gamma P_\pi v$. It has a
  unique solution :
  \begin{align*}
    v = (I-\gamma P_\pi)^{-1} r_\pi.
  \end{align*}
\end{frame}

\begin{frame}{Convergence of policy iteration}
  The following theorem shows that the policies found by the algorithm
  are always better. As there is a finite number of policies, this
  shows that policy iteration converges.

  \begin{theorem}[Policy improvement (Proposition 2.3.1 of Bertsekas)]
    Let $v$ be the value of a policy that let $\pi\in\argmax_{\pi}
    T_{\pi}v$. then $v_\pi$ (the value of $\pi$) satisfies
    \begin{align*}
      v_\pi \ge v 
    \end{align*}
    with equality if and only if $\pi$ is the optimal policy. 
  \end{theorem}
  
  Proof. By definition : for each state $s$, once has
  \begin{align*}
     v(s) \le T_\pi v(s)
  \end{align*}
  It should be clear that the operator $T_\pi$ is monotone. Hence,
  applying repetitively $T_\pi$ on both side of the equation shows
  that :
  \begin{align*}
    v(s) \le T_\pi v(s) \le [(T_\pi)^2]v (s) \le \lim_{k\to\infty}
    [(T_\pi)^k] v (s) =  v_\pi(s).
  \end{align*}
\end{frame}

\begin{frame}{Policy iteration : illustration}{On the same grid
    example}
  
    \only<2>{\includegraphics[width=\linewidth]{gridPolicyIteration_0}\\
    Iteration $0$}%
  \only<3>{\includegraphics[width=\linewidth]{gridPolicyIteration_1}\\
    Iteration $1$}%
  \only<4>{\includegraphics[width=\linewidth]{gridPolicyIteration_2}\\
    Iteration $2$}%
  \only<5>{\includegraphics[width=\linewidth]{gridPolicyIteration_3}\\
    Iteration $3$}%
  \only<6>{\includegraphics[width=\linewidth]{gridPolicyIteration_4}\\
    Iteration $4$}%
  \only<7>{\includegraphics[width=\linewidth]{gridPolicyIteration_5}\\
    Iteration $5$}%

\end{frame}



% \begin{frame}{Idea of the proofs}
%   For a value function $V$, define $L_{\pi}V$ and $LV$ as :
%   \begin{align*}
%     L_\pi V &= C^{\pi} + \delta P^{\pi}V\\
%     L V &= \min_{\pi} L_\pi V & \text{(component-wise)}
%   \end{align*}
  
%   \begin{tabular}{c|c}
%     Value iteration&\uncover<2>{Policy iteration}\\\hline
%     \mpage{.45}{

%     For any policy $\pi$, $L_\pi$ is contracting (for the 
%     norm): 
%     \begin{align*}
%       \norm{L_{\pi}V-L_\pi V'} = \norm{\delta P^\pi(V-V')}\\
%       \le \delta\hspace{2.4cm}
%     \end{align*}
%     This implies that $L$ is contracting.

%     Value iteration is $v_{k+1} = L v_k$. 
%     }&\mpage{.5}{
%        \pause
%        Let $v_{\pi}=(I-\delta P^{\pi})^{-1}c^{\pi}$ and $\pi'$ such
%        that $L_{\pi'} v_{\pi}= L v_{\pi}$. Hence, for $k\ge1$
%        \begin{align*}
%          L_{\pi'}^k v_{\pi} \le v_{\pi}. 
%        \end{align*}
%        By letting $k\to\infty$, this shows that : 
%        \begin{align*}
%          v_{\pi‘}\le v_{\pi} \text{ (component-wise)} 
%        \end{align*}
%        This shows that each iteration of PI reduces the cost. Hence,
%        PI converges in at most $|\calA|^{|\calS|}$ iterations. 
%        }
%   \end{tabular}
% \end{frame}

\begin{frame}{Difference between VI and PI, generalizations}
  The general idea of PI / VI is to proceed by iterations:
  \begin{align*}
    \pi_0 \underbrace{\leadsto}_{\text{evaluation}} v_0
    \underbrace{\leadsto}_{\text{policy improvement}} \pi_1
    \underbrace{\leadsto}_{\text{evaluation}} v_1
    \underbrace{\leadsto}_{\text{policy improvement}} \pi_2  \leadsto \dots 
  \end{align*}\pause 
  
  \begin{itemize}
  \item For VI and PI, ``policy improvement'' is the same (= best
    response): $\pi_k\in\argmax T_\pi v_{k-1}$.
  \item Evaluation differs:
    \begin{tabular}{|c|c|}
      \hline
      VI = one-step
      & PI = full evaluation\\
      $v_{k}=T_{\pi_k}v_{k-1}$
      &$v_k=(T_{\pi_k})^\infty v_{k-1}$.\\
      \hline
    \end{tabular}
  \end{itemize}\bigskip\pause 
  
  It is possible to do something in between, like:
  \begin{itemize}
  \item $v_k=(T_{\pi_k})^S v_{k-1}$ for any $S\in\{2,3,\dots\}$.
  \end{itemize}
  Evaluation can also be changed.
\end{frame}
\begin{frame}{Exercise}

  \begin{tabular}{cc}
    \mpage{.3}{
    \begin{tikzpicture}
      \node at (0,0) (R)
      {\includegraphics[width=\linewidth]{wheel_fortune}};%
      \node at (0,-4) (C) {};%
      \node at (0,-2.8) (X) {$X_t$ euros};%
      \draw (R) edge[->] (X) (X) edge[->] node {keep or draw again?}
      (C);
    \end{tikzpicture}
    }
    &\mpage{.65}{
      You can draw the wheel as much as you want. 
      \begin{itemize}
      \item Each time, the wheel stops on
        $x\in\{100,200,\dots,1000\text{ euros}\}$
      \item You can either stop and earn $x$ or draw the
        wheel again.
      \end{itemize}
      \bigskip
      
      Your goal is to maximize the discounted cost : 
      \begin{align*}
        \esp{\sum_{t=1}^\infty
        \delta^tX_tS_t},
      \end{align*}
      where $S_t=1$ if stop at time $t$ ($S_t=0$ otherwise). 
      }
  \end{tabular}\bigskip

  \begin{itemize}
  \item Model the problem as a MDP
  \item Solve the problem using value iteration and then using policy
    iteration for $\delta=0.9$ and $\delta=0.99$.
  \end{itemize}
\end{frame}

\begin{frame}{Solution}
  Bellman's equation is : 
  \begin{align*}
    v_x = \max \left(x, \delta \sum_{y=1}^{10}\frac1{10}v_y\right) 
  \end{align*}
  \pause {\tiny
    \url{https://gitlab.inria.fr/gast/courseOptimizationUncertainties/blob/master/slides/simu/Wheel_of_fortune.ipynb}}

  Illustration of the convergence of value iteration : 
  \begin{tabular}{@{}c@{}c@{}}
    \includegraphics[width=.5\linewidth]{valueIteration90}
    &\includegraphics[width=.5\linewidth]{valueIteration99}\\
    Policy ($\delta=0.9$) : draw while $<7$& Policy $\delta=0.99$: draw while $<10$
  \end{tabular}
  
  \red{Policy iteration terminates in $5$ iterations.}
\end{frame}


\begin{frame}{Exercise 2 : a discounted inventory problem}
  The stock level $X_t$ evolves according to :
  \begin{align*}
    X_{t+1} = \max(X_t + O_t - D_t,0),
  \end{align*}
  where $O_t\in\{0,1,2,\dots\}$ is our control variable (the number of
  order that we place) and $D_t$ is a random variable (the demand).
  The cost is :
  \begin{align*}
    C(X_t,O_t) = b O_t - s\min(D_t,X_t+O_t). 
  \end{align*}
  where $b$ is the cost to order one unit and $s$ the selling cost.
  \bigskip
  
  We assume that we can choose $O_t$ after observing $X_t$ but before
  observing $D_t$.
  \begin{itemize}
  \item Write down Bellman's equation for this problem
  \item Assume $b=1$, $s=2$ and $\delta=0.95$. Compute and
    describe the optimal policy (via VI or PI) when $O_t$ is
    geometrically distributed with mean $2$.
  \item Redo the computation with $\delta=0.5$ and $\delta=0.99$. What
    does it change? 
  \end{itemize}
\end{frame}


\subsection{Total reward}

\begin{frame}{Outline}
  \tableofcontents[currentsubsection]
\end{frame}

\begin{frame}
  \frametitle{Total reward: definition and examples}

  The \alert{total reward} is :
  \begin{align*}
    \esp{\sum_{t=0}^\infty R_{t+1}}
    % = \sum_{t=0}^\infty \sum_{x\in\calS}\Proba{X_t=x} c_x. 
  \end{align*}
  
  Typical examples: 
  \begin{itemize}
    \item Optimal stopping problems
    \begin{itemize}
      \item Optimal parking 
      \item Gambling
    \end{itemize}
    \item Games
  \end{itemize}
\end{frame}


\begin{frame}{The total cost is similar to the discounted cost with $\delta=1$}
  In particular, Bellman's equation holds :
  \begin{align*}
    V = \inf_\pi C^{\pi} + P^{\pi} V. 
  \end{align*}

  To solve a total reward problem, one can use: 
  \begin{itemize}
    \item Linear programming
    \item Value or policy iteration. 
  \end{itemize}

  \bigskip
  Examples :
  \begin{itemize}
  \item Gambler's problem
  \item Stochastic shortest paths
  \end{itemize}
\end{frame}

\subsection{Average reward}

\begin{frame}{The average reward criteria}
  
  Recall that the total reward is $\esp{\sum_{t=0}^\infty R_{t+1}}$. Problem: this might be infinite. 
  \bigskip

  The \alert{average reward} is :
  \begin{align*}
    \lim_{T\to\infty}\frac1T\esp{\sum_{t=0}^{T-1} R_{t+1}} 
  \end{align*} 
\end{frame}

\begin{frame}{The average cost is more complicated}
  % Recall that $v_{T}(s) = \sup_{\pi} \esp{\sum_{t=0}^T R_t\mid X_0=s}$. 
  Let $V_{0\dots T}(s) := \sup_\pi \esp{\sum_{t=1}^TR_t \mid X_0=s}$

  \alert{Theorem.} Assume that the MDP is unichain (\emph{i.e.}, any policy
  leads to an ergodic Markov chain). Then there exists a constant $g$ and a
  vector $H$ such that
  \begin{align*}
    V_{0\dots T}(s) = T\underbrace{g}_{\text{gain}} +
    \underbrace{H(s)}_{\text{~bias~}} + o_{T\to\infty}(1). 
  \end{align*}  
  \pause 

  This gives Bellman's equation: 
  \begin{align*}
    V_{0\dots T+1}(s) &= \sup_a R(s,a) + \sum_{s'} V_{1\dots T+1}(s')P(s'\mid s,a)\\
    &= \sup_a R(s,a) + \sum_{s'} V_{0\dots T}(s')P(s'\mid s,a)
  \end{align*}
  This suggests that:
  \begin{align*}
    g + H(s) = \sup_a R(s,a) + \sum_{s'} H(s')P(s'\mid s,a). 
  \end{align*} 
\end{frame}

\begin{frame}
  \frametitle{Methods to compute the average reward}
  You can use linear programming: 
  \begin{align*}
    \min g\\
    \text{ such that }&\forall s, a: g + H(s) \ge R(s,a) + \sum_{s'} H(s')P(s'\mid s,a).
  \end{align*}

  You can also use relative value iteration or policy iteration (see Puterman, chapter 8.5 and 8.6).
\end{frame}

\begin{frame}
  \frametitle{Example}

  Consider the following example
  
  \begin{center}
    \begin{tikzpicture}[scale=2]
      \tikzstyle{state} = [circle, draw]
      \tikzstyle{action} = [rectangle, draw]
      \tikzstyle{proba} = [fill=white, inner sep=1pt]
      \node[state] at (0,0) (s1) {G};
      \node[state] at (2,0) (s2) {B};
      \node[action] at (0,1) (a1) {a};
      \node[action] at (0,-1) (a2) {b};
      \node[action] at (2,1) (a3) {c};
      \draw (s1) edge[bend right,->,dashed]  node[right] {$+1$} (a1);
      \draw (s1) edge[bend left,->,dashed] node[right] {$+3$} (a2);
      \draw (s2) edge[bend right,->,dashed] node[right] {$+0$} (a3);
      \draw (a1) edge[bend right,->] node[proba] {$1$} (s1);
      \draw (a2) edge[bend right,->] node[proba] {$1$} (s2);
      \draw (a3) edge[->] node[proba] {$p$} (s1);
      \draw (a3) edge[bend right,->] node[proba] {$1-p$} (s2);
    \end{tikzpicture}
  \end{center}

  \begin{itemize}
    \item What is the optimal policy (as a function of $p$)?
    \item What is the bias and the average gain? 
  \end{itemize}

\end{frame}
\begin{frame}{Exercise : an inventory problem}
  The stock level $X_t$ evolves according to :
  \begin{align*}
    X_{t+1} = \max(X_t + A_t - D_t,0),
  \end{align*}
  where $A_t\in\{0,1,2,\dots\}$ is our control variable (the number of
  order that we place) and $D_t$ is a random variable (the demand).

  Assume that the reward is : 
  \begin{align*}
    R(X_t,A_t) = s\min(D_t,X_t+A_t) - a X_t - b A_t. 
  \end{align*}
  where $a$ is a holding cost, $b$ is the cost to order one unit and
  $s$ the selling price.

  We assume that we can choose $A_t$ before observing $D_t$. 
  \begin{itemize}
  \item Formulate the problem as a Markov decision process. 
  \item Assume that $a=0.1$, $b=1$, $s=2$. Compute and describe the
    optimal policy when $A_t$ is geometrically distributed with mean
    $10$.
  \item If we have to choose $A_t$ after observing $D_t$, what does
    it change? 
  \end{itemize}
\end{frame}

\section{Conclusion}

\begin{frame}{Conclusion on MDP}

  In this course, we have seen:
  \begin{itemize}
  \item Bellman's equation for finite horizon problem
  \begin{itemize}
    \item Dynamic programming
  \end{itemize}
  \item Infinite horizon problems
  \begin{itemize}
    \item Discounted, total and average rewards. 
  \end{itemize}

  \item Approximation and non-discounted problems
    \begin{itemize}
    \item Again, Bellman's equation plays a big role. 
    \end{itemize}
  \end{itemize}
  \bigskip
  
  Two solutions:
  \begin{itemize}
    \item \alert{Bellman's equation} and \alert{backward
    induction}.
    \item \alert{Linear programming}. 
  \end{itemize}

  \bigskip Next: bandits and reinforcement learning.
\end{frame}


% \begin{frame}{Conclusion on the course}
%   Orgzanization of the course chapter : 
%   \begin{itemize}
%   \item A few motivating examples and the value of information 
%   \item Markov chains 
%   \item Queuing theory 
%   \item Markov decision processes 
%   \end{itemize}
%   \bigskip
  
%   The goal of this part was to
%   \begin{itemize}
%   \item Give you theoretical tools to think about uncertainties in
%     terms of probabilistic uncertainties.
%   \item Give you algorithmic tools to solve some optimization
%     problems. 
%   \end{itemize}

%   Next : robust and online optimization. 
% \end{frame}

\end{document}