\input{latex_entete}

\title{Master ORCO : Optimization under uncertainty : Markov Chains}%
\author{Nicolas Gast}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}{Reference ($\approx$ 100 pages)}
  \centering  \includegraphics[width=.4\linewidth]{haggstrom}
\end{frame}
\section{Probability reminders and stochastic processes}

\subsection{Probability and conditional probabilities}

\begin{frame}{Probability space and random variable}
  A probability space is $(\Omega,\calF,\Proba{})$, where:
  \begin{itemize}
  \item $\Omega$ is the set of possible outcomes
  \item A set $E\subset\Omega$ is called an event. 
  \item $\Proba{}:\calF\to[0,1]$ is a a probability measure (the set
    $\calF$ is the set of measurable events). 
  \end{itemize}
  \begin{align*}
    \Proba{E} = \text{ probability of event $E$ occurring. }
  \end{align*}\pause

  \bigskip A $\calX$-valued random variable is a measurable function
  $X:\Omega\to\calX$, where
  \begin{align*}
    \Proba{X\in A} = \Proba{ \{\omega : X(\omega)\in A\} }.
  \end{align*}
  In general, we will never write $X(\omega)$. 
\end{frame}

\begin{frame}{Conditional probability}
  If $A\in\calF$ is a non-zero probability event, then the conditional
  probability is : 
  \begin{align*}
    \Proba{B\mid A} = \frac{\Proba{B\cap A}}{\Proba{A}}
  \end{align*}

  Two events are independent if $\Proba{A\cap B}=\Proba{A}\Proba{B}$.

  Two random variables are independent if for all values $A,B$:
  \begin{align*}
    \Proba{X\in A \land Y\in B} = \Proba{X\in A}\Proba{Y\in B}. 
  \end{align*}
  \bigskip\bigskip\pause
  
  \textbf{Examples}: $X$ and $Y$ are the outcomes of two (independent)
  6-face dice.
  \begin{itemize}
  \item What is $\Proba{X=6 | X\text{ is even}}$? \uncover<3->{$=1/3$}
  \item Are $X+Y$ and $X-Y$ independent? \uncover<3->{No:
      $\Proba{X+Y=11\land X-Y=0}=0$}
  % \item What is $\Proba{X=6 | X+Y = 8}$? \uncover<3->{$=1/5$ (why? See
  %     next slide)}
  \end{itemize}
\end{frame}

\begin{frame}{Total probability law and Bayes rule}
  If $A_1\dots A_n$ partition the state $\Omega$, then:
  \begin{align*}
    \Proba{B} = \sum_i \Proba{B | A_i} \Proba{A_i}
  \end{align*}
  
  \begin{center}
    \mpage{.7}{\begin{exampleblock}{Bayes rule}
        \begin{align*}
          \Proba{B|A} = \frac{\Proba{A|B}\Proba{B}}{\Proba{A}}.
        \end{align*}
      \end{exampleblock}}
  \end{center}
\end{frame}

\begin{frame}{Illustration of Bayes rule : reliability of a test}
  \textbf{Example}: A test is used to diagnose a rare disease. The
  test is $95\%$ accurate, meaning that it reports the right diagnose
  with probability $0.95$ and a false one otherwise.

  \textbf{Question} -- A child does a test and the test is
  positive. What is the probability that this child has the disease?
  \bigskip\pause
  
  \textbf{Answer} -- We need additional information: according to
  Bayes rule:
  \begin{align*}
    \Proba{Disease | Positive} &= \frac{\Proba{Positive | Disease
                                 }\Proba{Disease}}{\Proba{Positive}}\\
                               &\uncover<3->{=\frac{0.95 x}{0.95x + 0.05(1-x)},}
  \end{align*}
  \pause
  where $x$ is the proportion of sick persons in the population.

  
  \red{It depends on $x$}:
  \begin{itemize}
  \item If $x=1/1000$, then the probability is less than $2\%$!
  \end{itemize}
\end{frame}

% \begin{frame}{Bayesian update in more detail}
%   Suppose that $X$ is a random variable pick according to a
%   distribution with an unknown parameter $\theta$.  Assume that you
%   have some ``prior'' knowledge about $\theta$, for example you
%   $\Proba{\theta}$.

%   Bayes rule can be written as follows:
%   \begin{align*}
%     \Proba{ \theta | observation } &= \Proba{ observation | \theta }
%                                      \frac{\Proba{\theta}}{\Proba{observation}}. 
%   \end{align*}
%   $\Proba{\theta}$ is called the ``prior'' distribution. \pause

  
%   \begin{exampleblock}{}
%     For example : Assume that $X$ is a Bernoulli random variable with
%     parameter $\mu\in[0,1]$. Assume a uniform prior.  If you draw $n$
%     samples of $X$ and observe $p$ times ``1'', how does your
%     knowledge on $\mu$ evolve?
  
%   \begin{align*}
%     \Proba{\mu=\theta \mid X} &=
%                                 \only<2>{?}\uncover<3->{\frac{\Proba{X
%                                 \mid \mu=\theta} 
%                                 \Proba{\mu=\theta}}{\Proba{X=1}}\\
%                               &\propto \theta^p(1-\theta)^{n-p}.}
%   \end{align*}
% \end{exampleblock}

  
% \end{frame}

\subsection{Random variables and stochastic processes}
\begin{frame}{Outline}
  \tableofcontents[currentsubsection]
\end{frame}
\begin{frame}{Random variable}
  $X$ is a real-valued function of the outcome of an experiment. It
  can be described by its cumulative distribution function $F_X$,
  where
  \begin{equation*}
    F_X(x)=\Proba{X\ge x} 
  \end{equation*}

  \begin{itemize}
  \item A discrete random variable can take at most a countable number
    of possible values. Example: Dice
    \begin{align*}
      \Proba{X=1}=\dots=\Proba{X=6}=1/6. 
    \end{align*}
  \item A continuous random variable can take an uncountable number of
    possible values. Examples
    \begin{itemize}
    \item Exponential random variable :
      $F_X(x) = \Proba{X\ge x} = min(1,e^{-\lambda x})$
    \item Normal random variable :
      $F_X(x) = \frac{1}{\sqrt{2\rho}}\int_{-\infty}^{+x}e^{-x^2/2}dx$.
    \end{itemize}
  \end{itemize}
\end{frame}

% \begin{frame}{Filtration}
%   Let $(\Omega,\calF,\Proba{})$ is an increase sequence of
%   $\sigma$-algebra:
%   \begin{itemize}
%   \item for each $n$, $\calF_n\subset\calF_{t+1}\subset\calF$.
%   \end{itemize}

%   A sequence of random variable $(X_n)\ge0$ is a $(\calF_n)$-adapted
%   stochastic process if for each $n$, the variable $X_n$ is
%   $\calF_n$-adapted.
% \end{frame}


\begin{frame}{Stochastic process}

  A stochastic process is a sequence of random variables
  $X=X_0,X_1,X_2,\dots$ on the same probability space $\calF$ such
  that $X_t$ is $\calF_t$ measurable with
  $\calF_0\subset\calF_1\subset\calF_2\subset\calF_3\dots\subset\calF$.
  \bigskip\pause
  
  For example, $X$, $S$ and $A$ are three stochastic processes:
  \begin{itemize}
  \item $X_t$ = $n$th draw of a biased coin
    ($\Proba{X_t=0}=p = 1-\Proba{X_t=1}$).
  \item $S_t=\frac1t\sum_{i=1}^t X_i$ = mean of the $n$ first draw
  \item $A_t = X_1 \oplus X_2\dots \oplus X_t$
  \end{itemize}

  % $X_t$, $S_t$ and $A_t$ are three examples of stochastic processes.
  \bigskip\pause 
  
  In what follows, we will study and explore a particular type of
  stochastic processes (Markov chains) and define what
  $\lim_{t\to\infty}X_t$ means.
  \begin{itemize}
  \item Note that $X$, $S$ and $A$ are three examples of Markov
    chains. 
  \end{itemize}
\end{frame}


\subsection{Random Generation}

\begin{frame}{How to Generate Randomness}
  Is a computer random? \pause
  \begin{itemize}
  \item Yes or no (\emph{c.f.}, \texttt{/dev/random}
    v.s. \texttt{/dev/urandom})

    \begin{center}
      \includegraphics[width=.15\linewidth]{lancer_de}
    \end{center}
  \end{itemize}
  
  \vspace{1cm} \pause

  In practice, we mostly use pseudo-random generator (sufficient for
  simulation, not for cryptography):
  \begin{center}
    \mpage{.3}{
      \begin{exampleblock}{}
        \texttt{import numpy as np\\
          np.random.rand()}
      \end{exampleblock}
    }
  \end{center}
  This generates numbers between $0$ and $1$ (and this is sufficient).   
\end{frame}

\begin{frame}{How to generate numbers from a given distribution?}
  \includegraphics[width=.7\linewidth]{generation_distribution}
\end{frame}

\begin{frame}{Method 1: inverse method}

  Let $F$ be the CDF of the distribution $F(x)=\Proba{X\le x}$.  Pick
  a $U$ uniformly distributed between $0$ and $1$ and output: 
  \begin{align*}
    F^{-1}(U) = \sup \{  x \mid F(x)\le U \}. 
  \end{align*}
  This outputs a distribution with CDF $F$.
  
  \only<1>{\includegraphics[width=.7\linewidth]{generation_inverseMethod1}}%
  \only<2>{\includegraphics[width=.7\linewidth]{generation_inverseMethod2}}
\end{frame}

\begin{frame}{Method 2: reject}
  Let $f$ be the probability density from which you want to generate:
  \begin{enumerate}
  \item Pick $U_1\in\mathrm{supp}(X)$ (uniformly)
  \item Pick $U_2\in[0,\max_x f(x)]$ (uniformly)
  \item If $U_2 \ge f(U_1)$, output $U_1$, otherwise return to step
    $1$. 
  \end{enumerate}
  
  \only<1>{\includegraphics[width=.7\linewidth]{generation_reject1}}%
  \only<2>{\includegraphics[width=.7\linewidth]{generation_reject2}}%
  \only<3>{\includegraphics[width=.7\linewidth]{generation_reject3}}
\end{frame}

\begin{frame}{How to generate from a Circle}
  \begin{tabular}{cc}
    \mpage{.45}{\includegraphics[width=\linewidth]{generation_circle}}\pause
    &\mpage{.45}{\includegraphics[width=\linewidth]{generation_wrongcircle}}\\
    \mpage{.45}{Good method: Reject:\\
    Repeat
    \begin{itemize}
    \item Generate $X,Y$
    \item While $X,Y \not\in \mathrm{Circle}$
    \end{itemize}
    }
    &\mpage{.45}{Naive (\red{wrong}) method:\\
    generate radius + angle}
  \end{tabular}
\end{frame}

\begin{frame}{Take-home message}

  \begin{itemize}
  \item Be careful, not all peudo-number generators are good. \bigskip
  \item Reject methods are very powerful \bigskip
  \item There are many libraries to generate random numbers: do not
    reprogram everything.

    \begin{center}
      \includegraphics[width=\linewidth]{python_random}
      
      {\scriptsize\url{https://docs.scipy.org/doc/numpy-1.14.0/reference/routines.random.html}}
    \end{center}
  \end{itemize}
\end{frame}


\section{Markov chain}

\subsection{Definition}

\begin{frame}{Definition}

  A sequence of $\calX$-valued random variables $(X_t)_{t\ge0}$ is
  called a Markov chain with initial distribution $\rho_0$ if
  \begin{itemize}
  \item $X_0\sim \rho_0$ 
  \item
    $\Proba{X_{t+1}=j | X_t=i,x_{t-1}=i_{t-1}\dots,X_0=i_0} =
    \Proba{X_{t+1}=j| X_t=i}$.
  \end{itemize}
  \bigskip
  
  The chain is time-homogeneous if $P_{ij}:=\Proba{X_{t+1}=j| X_t=i}$
  does not depend on $t$.
\end{frame}

\newcommand\graph[2]{\only<#1>{\includegraphics[width=.7\linewidth]{simu/outputs/random_walk_#2}}}

\begin{frame}{Example: a random walk on a directed graph}
  \only<1>{\includegraphics[width=.7\linewidth]{simu/outputs/initial_graph}}%
  \graph{2}{1}
  \graph{3}{2}
  \graph{4}{3}
  \graph{5}{4}
  \graph{6}{5}
  \graph{7}{100}
  \graph{8}{1000}
  % initial_graph.pdf     random_walk_1000.pdf

  \pause
  We denote by:
  \begin{itemize}
  \item $X_t$ the value of the chain after $n$ iterations. 
  \begin{equation*}
    X = (A, \only<2->{C}, \only<3->{D}, \only<4->{A}, \only<5->{B}, \only<6->{C},\dots\only<7->{,A,\dots}\only<8->{,C,\dots})
  \end{equation*}
  \item $\rho_t(i)$ the probability of being in state $i$ at time $n$: $\rho_t(i) = \Proba{X_t=i}$. 
  \end{itemize}
\end{frame}


\begin{frame}{Markov chain representation : Stochastic Automata}
  A Markov chain is given by: 
  \begin{itemize}
    \item A set of states $\calS$
    \item An ititial state $x_0\in\calS$
    \item The \red{transition matrix} $P$:
    \begin{equation*}
      P_{ij}:=\Proba{X_{t+1}=j| X_t=i}.      
    \end{equation*}
    $P$ is such that for all $i,j\in\calS$: $P_{ij}\ge0$, $\sum_{j\in\calS}P_{ij}=1$.
  \end{itemize}

  \centering
  We often represent the Markov chain as a stochastic automata:
  \includegraphics[width=.5\linewidth]{simu/outputs/random_walk_1}
\end{frame}

% \begin{frame}{These definitions are equivalent}
%   \begin{tabular}{@{}c@{}cc}
%     Automata (graph) & Stochastic recurrence & Algebraic approach
%     \\
%     \mpage{.4}{\includegraphics[width=\linewidth]{erenfest_model}}
%     &\mpage{.3}{\includegraphics[width=\linewidth]{syst_dynamique}}
%     &\mpage{.2}{\includegraphics[width=\linewidth]{matrice}}
%   \end{tabular}
% \end{frame}

% \subsection{Formal definition}

\begin{frame}{Algebraic representation}
  \red{Chapman-Kolmogorov Equation}: 
  \begin{align*}
    \rho_{t+1}(j) &= \Proba{X_{t+1}=j} \\
    \uncover<2->{&= \sum_{i}\Proba{X_{t}=i} \Proba{X_{t+1}=j |
                X_{t}=i}\\}
    \uncover<3->{&=\sum_i \rho_t(i) p_{ij} \\\pause
                &=(\rho_t P)(j) \\}
    \uncover<4->{\Proba{X_t=j|X_0 = i} &= (P^n)_{ij} }
  \end{align*}
\end{frame}

\begin{frame}{Problem that we will learn how to solve}
  \begin{itemize}
  \item How to estimate $\rho_t$
  \item How to compute a stopping time
  \item How to compute $\lim_{t\to\infty}\rho_t$? 
  \item How can we compute optimal control policies for Markov chains? 
  \end{itemize}
\end{frame}

\begin{frame}{Applications of Markov chains (in CS or OR)}
  \begin{itemize}
  \item Algorithmic tool
    \begin{itemize}
    \item Numerical method (ex: Monte-Carlo)
    \item Randomized algorithm (ex: TCP, searching, pageRank,...) 
    \item Machine learning \& artificial intelligence
    \item \dots
    \end{itemize} \bigskip
    
  \item Tool for modeling 
    \begin{itemize}
    \item Performance evaluation 
    \item Program verification
    \item Stochastic optimization 
    \item \dots 
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{Computation of hitting times}
  
  Let $T_i$ be the expected time to reach a state $x$ given that we
  state in $i$:
  \begin{align*}
    T_i = \esp{ \min\left\{t\ge0 : X_t = x\right\} \mid X_0 = i}.
  \end{align*}
  \bigskip
  
  The variables $T_i$ satisfy
  \begin{align*}
    \left\{
    \begin{array}{ll}
      T_x = 0\\
      T_i = 1+\sum_{j} P_{ij}T_j & \qquad\text{ for all $i\ne x$}
    \end{array}
                                   \right.
  \end{align*}
\end{frame}

\begin{frame}{Application : Exercise}
  You draw a $6$-face unbiased dice multiple time. We want to know
  which of the patterns ``5-6'' and ``6-6'' occurs first (on
  average). \bigskip
  
  For the pattern ``66'', consider the following Markov chain :
  \begin{center}
    \begin{tabular}{cc}
      \mpage{.45}{
      \begin{tikzpicture}[scale=2]
        \node[draw,circle] at (0,0) (X) {$\varepsilon$};%
        \node[draw,circle] at (1,0) (6) {6};%
        \node[draw,circle] at (2,0) (66) {66};%
        \draw[->] (X) edge[loop] node[above]{$5/6$} (X);
        \draw[->] (X) edge node[above]{$1/6$} (6); %
        \draw[->] (6) edge node[above]{$1/6$} (66); %
        \draw[->] (6) edge[bend left] node[below]{$5/6$} (X); %
        % \draw[->] (66) edge[bend left] node[below]{$5/6$} (X); %
        \draw[->] (66) edge[loop] node[above]{$1$} (66);
      \end{tikzpicture}} &\qquad The initial state is $\varepsilon$.
    \end{tabular}
  \end{center}

  \begin{enumerate}
  \item Explain to your neighbor the relation between this chain and
    the problem. 
  \item Write down $P$ and computes the expected time to reach ``66''
    from $\varepsilon$. 
  \item Answer the problem.
  \end{enumerate}
\end{frame}

\subsection{Steady-state analysis}
\begin{frame}{Outline}
  \tableofcontents[currentsubsection]
\end{frame}

\begin{frame}{\textbf{Exercise}: Inventory control problem}
  \label{exo:inventory_control}
 
  We are managing an single-item inventory that can store up to $10$
  items. Let $X_{2t}$ be the number of items at the beginning of day
  $t$ and $X_{2t+1}$ be the number of items at the end of day $t$
  (before order). Assume that:
  \begin{itemize}
  % \item $X_0=10$. 
  \item You sell $\min(X_{2t},D_t)$ during the day, where
    $D_t\sim\mathrm{Unif}\{0,\dots,10\}$.
  \item At the end of the day, you buy $A_t$ items.
  \end{itemize}
  \bigskip
  
  Questions: 
  \begin{enumerate}[(a)]
  \item Assume that $A_t=4$.
    \begin{itemize}
    \item Why is $X_t$ a Markov chain? Draw its transition graph. 
    \item Let $Y_t = X_{2t}$. Is $Y_t$ a Markov chain? 
    \end{itemize}
  \item Same question with $A_t=\min(7-X_{2t+1}, 0)$.
  \item Assume that the selling price is $1$, the buying price is
    $0.5$ and the storing cost is $0.1$. Which of the policy (a) or
    (b) is best?  Does it depend on $X_0$?
  \end{enumerate}

  {\tiny \hyperlink{exo:inventory_control_later}{\beamergotobutton{See
        also later}}}
\end{frame}



\begin{frame}{State classification}
  \begin{tabular}{cc}
    \mpage{.45}{
    \only<1>{\includegraphics[width=\linewidth]{compo_analysis}}%
    \only<2>{\includegraphics[width=\linewidth]{compo_irred}}%
    \only<3>{\includegraphics[width=\linewidth]{compo_irred2}}%
    \only<4>{\includegraphics[width=\linewidth]{compo_irred3}}
    }
    &\mpage{.5}{
      \begin{itemize}
      \item A set of state $C$ is an \red{irreducible component} if
        \begin{itemize}
        \item For all $i,j\in C$, there exists a path of positive
          probability from $i$ to $j$. 
        \item For all $i\in C$, $j\ne C$, there is no path of positive
          probability from $i$ to $j$.
        \end{itemize}
      \end{itemize}
      \pause \pause 

      A irreducible component is \red{aperiodic} if the GCD of all the
      lengths of all paths is equal to $1$.  }
      \pause 
      
  \end{tabular}
  \bigskip\bigskip\bigskip
  
  A Markov chain is \red{irreducible} if it its state space is an
  irreducible component.

\end{frame}

\begin{frame}{Quiz}
  \mpage{.4}{
    \only<1>{\includegraphics[width=\linewidth]{non_irred}}%
    \only<2>{\includegraphics[width=\linewidth]{non_irred2}}%
    \only<3>{\includegraphics[width=\linewidth]{non_irred3}}%
  }\qquad%
  \mpage{.4}{
    \only<1>{Is this chain :
    \begin{itemize}
    \item[A.] Irreducible? 
    \item[B.] Aperiodic? 
    \end{itemize}
  }
  }%
  \only<2>{\textbf{Answer} : not irreducible because there is no path
    from $4$ to $3$. 
    \bigskip
    
    How many irreducible components does this chain have? 
    \begin{enumerate}
    \item $1$
    \item $2$
    \item $3$
    \item I do not know. 
    \end{enumerate}
  }%
  \only<3>{ \mpage{.35}{\textbf{Answer} : $2$ irreducible components :
      $\{1\}$ and $\{4\}$.}  }
  
\end{frame}


\begin{frame}{Stationary distribution}
  
  Reminder :  
  \begin{itemize}
  \item $P$ is the transition matrix : 
    $P_{ij}:=\Proba{X_{t+1}=j| X_t=i}$.
  \item $\rho_t$ is the probability distribution at time $t$
    $\rho_{t}(i) = \Proba{X_{t}=i}$.
  \item Chapman-Kolmogorov Equation : 
  \begin{align*}
    \rho_{t+1}(j) &= (\rho_t P)(j)
  \end{align*}

  \end{itemize}
  \bigskip
  
  \begin{center}
    \mpage{.9}{
      \begin{exampleblock}{Definition.} We say that a distribution
        $\rho$ is a stationary distribution if $\rho P = \rho$. 
      \end{exampleblock}
    }
  \end{center}
\end{frame}

\begin{frame}{Stationary distribution }
  \textbf{Definition.} We say that a distribution $\rho$ is a
  stationary distribution if $\rho P = \rho$.

  \begin{theorem}
    \begin{enumerate}
    \item A finite-state Markov chain has at least one stationary
      distribution. \pause
    \item An \red{irreducible} finite-state Markov chain has a unique
      stationary distribution. \pause
    \item If this chain is also \red{aperiodic}, then for any initial
      distribution $\rho_0$:
      \begin{equation*}
        \lim_{t\to\infty}\rho_t = \rho. 
      \end{equation*}
    \end{enumerate}
  \end{theorem}
  
  \textbf{Note}: $X_t$ converges \emph{in law} to $\rho$. 
\end{frame}

\begin{frame}{Practical question : how to compute the stationary measure}
  \begin{enumerate}
  \item Verify that this chain is irreducible and aperiodic
  \item Solve the system of equation $\rho P=\rho$ and
    $\sum_{i}\rho(i)=1$.
  \end{enumerate}
  \pause 
  \textbf{Exercise} :
  
  \begin{center}
    \begin{tikzpicture}
      \node[draw,circle,inner sep=0pt] at (0,0) (0) {Sunny day};
      \node[draw,circle,inner sep=1pt] at (3,0) (1) {Rainy day};
      \draw[->] (0) edge[bend left] node[above] {0.2} (1); \draw[->]
      (0) edge[loop above] node[above] {0.8} (0); \draw[->] (1)
      edge[bend left] node[below] {0.5} (0); \draw[->] (1) edge[loop
      above] node[above] {0.5} (1);
    \end{tikzpicture}
  \end{center}
  \begin{itemize}
  \item Is this chain irreducible and aperiodic? 
  \item Compute its stationary distribution.
  \item On average, how likely is a day sunny in this city?
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Exercise: inventory control}
  \label{exo:inventory_control_later}
  
  %\ref{exo:inventory_control}
  
  
  Go back to the exercise
  \hyperlink{exo:inventory_control}{\beamergotobutton{Inventory
      control}} and answer the last question. \pause

  \includegraphics[width=\linewidth]{inventory_strategy_+4}
  \includegraphics[width=\linewidth]{inventory_treshold_7}
\end{frame}

% \begin{frame}{Exercice}
%   \begin{center}
%     \begin{tikzpicture}
%       \node[draw,circle] at (0,0) (0) {A};
%       \node[draw,circle] at (2,0) (1) {B}; 
%       \node[draw,circle] at (4,0) (2) {C};
%       \draw[->] (0) edge[bend left] node[above] {0.5} (1);
%       \draw[->] (0) edge[loop above] node[above] {0.5} (0);
%       \draw[->] (1) edge[bend left] node[above] {0.5} (2);
%       \draw[->] (1) edge[loop above] node[above] {0.5} (1);
%       \draw[->] (2) edge[bend left] node[below] {0.25} (0);
%       \draw[->] (2) edge[bend left] node[below] {0.5} (1);
%       \draw[->] (2) edge[loop above] node[above] {0.25} (2);
%     \end{tikzpicture}
%   \end{center}

%   \begin{enumerate}
%   \item Cette chaîne est-elle irréductible et apériodique? 
%   \item Calculer la mesure stationnaire de cette chaîne. 
%   \item On pose $\rho_0=(1,0,0)$. Calculer $\rho_2$. Que remarquez vous?
%   \end{enumerate}
% \end{frame}


% \begin{frame}{Ergodicity in practice}
%   Ergodicity means that :
%   \begin{align*}
%     \underbrace{\lim_{t\to\infty} \frac{1}{n} \sum_{k=1}^n
%     X_t}_{\text{time average}} =
%     \underbrace{\lim_{t\to\infty} \esp{X_t}}_{\text{ensemble average}}
%   \end{align*}
%   (reminder : this is not true in general!)
%   \bigskip \pause 
  
%   Consequence : 
%   \begin{itemize}
%   \item One can simulate a single trajectory (over a long enough time
%     horizon) to obtain the performance of any trajectory, and also the
%     stationary performance
%   \end{itemize}
% \end{frame}

\begin{frame}{Ergodic theorem}
  \begin{theorem}
    Let $(X_t)$ be a finite-state \red{irreducible} Markov chain. Then for all $i\in\calS$:
    \begin{equation*}
      \frac1n\sum_{k=1}^n \mathbf{1}_{\{X_k=i\}} \to \rho(i) \qquad \text{
        almost surely}
    \end{equation*}
  \end{theorem}
  %\pause\bigskip
  %\begin{itemize}
  %\item In practice, it means that one can simulate a single
  %  trajectory (over a long enough time horizon) to obtain the
  %  performance of any trajectory, and also the stationary performance
  %\end{itemize}

  \centering \includegraphics[width=.7\linewidth]{simu/outputs/random_walk_1000}
\end{frame}

% \begin{frame}{Ergodic theorem : be careful!}
%   The ergodic theorem is \red{not true} if the chain is not
%   irreducible.

%   For example, if the initial state is $0$ and the transition graph is
%   :
    
%   \begin{center}
%     \begin{tikzpicture}
%       \node[draw,circle] at (0,0) (0) {0}; %
%       \node[draw,circle] at (2,.5) (1) {1}; %
%       \node[draw,circle] at (2,-.5) (2) {2}; %
%       \draw[->] (0) edge node[above] {$1/2$} (1); %
%       \draw[->] (0) edge node[below] {$1/2$} (2); %
%       \draw[->] (1) edge[loop right] node[right]{1} (1); %
%       \draw[->] (2) edge[loop right] node[right]{1} (2); %
%     \end{tikzpicture}
%   \end{center}
%   We have :
%   \begin{align*}
%     \frac1n\sum_{k=1}^n X_k &=\left\{
%     \begin{array}{ll}
%       1&\text{ with probability $1/2$}\\
%       2&\text{ with probability $1/2$.}
%     \end{array}
%          \right. & \text{ (time average)}\\
%     \lim_{t\to\infty} \esp{X_t} &= \frac32 & \text{(ensemble average)}
%   \end{align*}
% \end{frame}




\begin{frame}{Reversibility}
  We say that a chain $X_t$ est reversible with respect to $\rho$ if
  for all state $i,j$ : 
  \begin{equation*}
    \rho(i) P_{ij}  = \rho(j) P_{ji}
  \end{equation*}
  \begin{theorem}
    If such a measure exists, then $\rho$ is a stationary
    distribution. 
  \end{theorem}
  \pause 
  \bigskip
  
  Why reversibility? In stationary regime : 
  \begin{align*}
    \Proba{X_1=i_1,\dots,X_k=i_k} 
    &\uncover<3->{= \rho({i_1})P_{i_1,i_2}\dots
      P_{i_{k-1},i_k} }\\
    &\uncover<4->{=P_{i_2,i_1} \rho(i_2) P_{i_2,i_3}\dots P_{i_{k-1},i_k}}\\
    &\uncover<5->{= P_{i_2,i_1} P_{i_3,i_2}\dots P_{i_{k},i_{k-1}}\rho(k)}\\
    &\uncover<6->{= \rho(k) P_{i_k,i_{k-1}}\dots P_{i_{2},i_{1}}}\\
    &= \Proba{X_1=i_k,\dots,X_k=i_1}
  \end{align*}
\end{frame}

\begin{frame}{Exercise : random walk on graph.}
  Let $G=(V,E)$ be a non-directed graph. Let $X_t\in V$ be the
  position of a walker after $n$ steps. $X_{t+1}$ is picked uniformly
  among all neighbors. 
  
  Let $d_i$ be the degree of node $i$.
  \begin{enumerate}
  \item What is $P_{ij}$? 
  \item Find a measure $\rho$ such that
    $ \rho(i) P_{ij} = \rho(j) P_{ji} $
  \item Deduce the stationary probability of the random walker on the
    following graph :
    
    \begin{center}
      \includegraphics[width=.4\linewidth]{graph}
    \end{center}
    
  \item Consider a knight moving randomly on a chessboard. What
    squares are the less visited? 
  \end{enumerate}
\end{frame}

\begin{frame}{In practice, how to compute or simulate a stationary
    measure? }
  \begin{itemize}
  \item Analytical method (often when $S$ is small or the chain is reversible)
  \item Numerical method : inverse of matrix ($S\le 1000$)
  \item Simulation
    \begin{itemize}
    \item Warm-up + estimation 
    \end{itemize}
  \end{itemize}
\end{frame}

\section{MCMC methods and Simulated Annealing}

\begin{frame}{MCMC methods in a nutshell}

  Motivation : Solve a deterministic optimization problem by using
  stochastic simulation \vspace{1cm}
  
  MCMC and simulated annealing consist in
  \begin{itemize}
  \item Defining a probability measure $\rho$ that puts weight on the
    good solutions.
  \item Construct a Markov chain whose stationary distribution is
    $\rho$. 
  \end{itemize}
  % Examples :
  % \begin{enumerate}
  % \item Independent set 
  % \item Travel salesman problem (TSP)
  % \end{enumerate}

\end{frame}

\begin{frame}{Example 1 : Independent set}

  \begin{tabular}{cc}
    \mpage{.65}{ Given a graph $G=(V,E)$, a set $S\subset V$ is an
    independent set $G$ if for all $(i,j)\in S$: $(i,j)\not\in V$.}
    &\mpage{.3}{\centering \includegraphics[width=\linewidth]{IS}\\
    \tiny (source :
      \url{https://en.wikipedia.org/wiki/Maximal_independent_set})
      }
  \end{tabular}
  \bigskip\bigskip
  
  The following problems are NP-complete :
  \begin{itemize}
  \item Find the maximum\footnote{can be solved in $O(1.2108^n)$, see
      Robson (1986), "Algorithms for maximum independent sets",
      Journal of Algorithms. } independent set.
  \item On average, how much vertices has an independent set?
  \end{itemize}

  \pause

  \alert{We want to sample from $\rho$}, where :
  \begin{align*}
    \rho( \mathrm{S} ) \propto \left\{
    \begin{array}{ll}
      \lambda^{\#vertices}& \text{ if $S$ is an independent set}\\
      0 & \text{ otherwise}.
    \end{array}
          \right.
  \end{align*}
\end{frame}


\begin{frame}{Example 2: Traveling salesman problem}
  
  \begin{tabular}{cc}
    \mpage{.5}{
    Given a complete weighted graph $W$
    \begin{itemize}
    \item Find a permutation $\sigma$ that minimizes the total
      distance to visit all vertices:
      \begin{align*}
        \sum_{i}W_{\sigma(i),\sigma(i+1)}
      \end{align*}
      
    \end{itemize}\pause\bigskip
    
    \red{Solution : } We want to sample from $\rho$, where :
    \begin{align*}
      \mathrm{cost}(\sigma)=\rho( \sigma ) \propto e^{-\beta \mathrm{cost}(\sigma)}.
    \end{align*}
    }
    &\mpage{.45}{\includegraphics[width=\linewidth]{tsp2}}
  \end{tabular}
\end{frame}

\begin{frame}{Independent set : sampling from the uniform distribution
  }
  We want to create a Markov chain whose stationary distribution is
  $\rho$ :
  \begin{align*}
    \rho(S) \propto \left\{
    \begin{array}{ll}
      1 &\text{ if $S$ is an independent set of $G$}\\
      0 & \text{ otherwise}.
    \end{array}
          \right.
  \end{align*}
  \bigskip \pause
  
  We define the Markov chain $(X_t)_{t\ge1}$ on
  $\calX=\{0,1\}^{\abs{V}}$: initially $X_0=(0\dots0)$. Then at each
  time step : 
  \begin{itemize}
  \item Pick a vertex $v\in V$ (uniformly)
  \item With probability $1/2$, do $a$ or $b$:
    \begin{itemize}
    \item[(a)] If all neighbors of $v$ are at $0$, then set 
      $X_{t+1}(v) = 1$.
    \item[(b)] Set $X_{t+1}(v):=0$. 
    \end{itemize}
  \item For $w\ne v$: $X_{t+1}(w)=X_t(w)$:
  \end{itemize}
  \bigskip\pause 
  
  Exercise (at home) : By reversibility, show that $\rho$ is the unique
  stationary measure of $(X_t)$.
\end{frame}

\begin{frame}{Biased sampling}
  We want to create a Markov chain whose stationary distribution is
  $\rho^{(\lambda)}$ :
  \begin{align*}
    \rho^{(\lambda)}(S) \propto \left\{
    \begin{array}{ll}
      \lambda^{\abs{S}} &\text{ if $S$ is an independent set of $G$ with
                       $\abs{S}$ vertices}\\ 
      0 & \text{ otherwise}.
    \end{array}
          \right.
  \end{align*}
  \bigskip \pause
  
  We define the Markov chain $(X_t)_{t\ge1}$ on
  $\calX=\{0,1\}^{\abs{V}}$: initially $X_0(v)=0$ for all $v\in
  V$. Then at each time step :
  \begin{itemize}
  \item Pick a vertex $v\in V$ (uniformly)
  \item With probability $\red{\lambda/\lambda+1}$, do $a$ or $b$:
    \begin{itemize}
    \item[(a)] If all neighbors of $v$ are at $0$, then set 
      $X_{t+1}(v) = 1$.
    \item[(b)] Set $X_{t+1}(v):=0$. 
    \end{itemize}
  \item For $w\ne v$: $X_{t+1}(w)=X_t(w)$:
  \end{itemize}
  \bigskip\pause 
  
  Exercise (at home) : By reversibility, show that $\rho^{(\lambda)}$ is the unique
  stationary measure of $(X_t)$.
  
\end{frame}

\begin{frame}{The previous algorithm is a particular case of an MCMC
    algorithm called ``Gibbs sampling''}

  \textbf{Gibbs sampling} : 
  \begin{itemize}
  \item Pick a vertex $v\in V$ (uniformly)
  \item Choose $X_{t+1}(v)$ according to the conditional distribution
    (under $\rho$) of the others.
  \item For $w\ne v$: set $X_{t+1}(w)=X_t(w)$:
  \end{itemize}
\end{frame}

\begin{frame}{Other MCMC algorithm : Metropolis-Hastings}
  Given a distribution $\rho$ and a stochastic matrix $Q$ (where
  $Q_{ij}>0$ \emph{iif} $Q_{ji}>0$), we define the following dynamics
  :
  \begin{itemize}
  \item Pick a new state $Y$ according to $Q$ and compute
    \begin{equation*}
      a=\frac{\rho(Y) Q_{Y,X_t}}{\rho(X) Q_{X_t,Y}}.
    \end{equation*}
  \item Do $X_{t+1} := Y$ with probability $\min(a,1)$ and
    $X_{t+1}=X_t$ otherwise.
  \end{itemize}
  \bigskip\pause
  
  \textbf{Proof}. $\rho$ satisfy the reversibility equations : for two
  neighbors $x,y$ such that $ {\rho(y) Q_{y,x}} \le {\rho(x) Q_{x,y}}$
  we have
  
  \begin{center}
    \begin{tikzpicture}
      \node[circle,draw] (X) at (0,0) {x}; \node[circle,draw] (Y) at
      (4,0) {y};%
      \draw (X) edge[->,bend left]  node[above]{$Q_{x,y}\frac{\rho(y)
          Q_{y,x}}{\rho(x) Q_{x,y}}$} (Y); % 
      \draw (Y) edge[->,bend left] node[below]{$Q_{y,x}$} (X);
    \end{tikzpicture}
  \end{center}
\end{frame}


\begin{frame}{Simulated annealing}
  Idea :
  \begin{itemize}
  \item You are given a function $f(X)$ and you want to sample from
    $\rho(X) \propto e^{-\frac1Tf(X)}$.
  \item Apply Metropolis-Hastings algorithm. 
  \end{itemize}
  \bigskip For example, if we know how to sample \alert{uniformly} from
  a neighbor, we can do :
  
  \begin{itemize}
  \item Pick a neighbor $Y$ of $X_t$ (uniformly)
  \item Set $X_{t+1}:=Y$ with probability
    $\min(1,e^{(f(X_t)-f(Y))/T_t})$ (and $X_{t+1}=X_t$ otherwise). 
  \end{itemize}
  \bigskip\pause

  \textbf{Remarks} : 
  \begin{itemize}
  \item If $T_t$ is fixed, this is Metropolis-Hastings
    algorithm. $\rho$ concentrates on the minimizers of $f$ as $T$ goes
    to $0$.
  \item We can choose to decrease $T_t$ (but this choice is often
    arbitrary). 
  \end{itemize}
\end{frame}

\begin{frame}{Simulated annealing applied to the TSP}

  \begin{center}
    \pdfmovie{simu/simulated_annealing.mp4}{12cm}{7cm}{\includegraphics[width=\linewidth]{simulated_annealing_image}}
  \end{center}

\end{frame}

\section{Conclusion}
\begin{frame}{Where do we stand?}
  \begin{itemize}
  \item Definition of Markov chains
    \begin{itemize}
    \item How to compute time to reach some states
    \item What is and how to compute a stationary distribution
    \end{itemize}\bigskip
    
  \item Monte-Carlo Markov chains algorithms
    \begin{itemize}
    \item Stochastic methods to solve deterministic problems 
    \end{itemize}
  \end{itemize}

  \bigskip

  Next : Markov reward processes and Markov decision processes. 
\end{frame}

% \begin{frame}{Application : processus de naissance et de mort}
%   \begin{center}
%     \begin{tikzpicture}[xscale=1.8]
%       \foreach \x/\y in {0/0,1/1,2/2,3/3,5/K-1,6/K}{%
%         \node[draw,circle,inner sep=0pt,minimum width=.8cm] at (\x,0) (\y) {\y}; 
%         \draw[->] (\y) edge[loop above] node{$p_{\y}$} (\y);
%       } %
%       \node at (4,0) (4) {\dots};
%       \node at (4,0) (K-2) {\dots};
%       \foreach \x/\y in {0/1,1/2,2/3,3/4,K-2/K-1,K-1/K}{%
%         \Adroite{\x}{\y}{$d_{\x}$}\Agauche{\y}{\x}{$g_{\y}$}%
%       }%
%     \end{tikzpicture}
%   \end{center}
  
%   Soit $x_{i+1} = x_i \frac{d_{i}}{g_{i+1}}$.  $x$ satisfait
%   $x_iP_{ij}=x_jP_{ji}$.\bigskip
  
%   La mesure stationnaire est donc:
%   \begin{equation*}
%     \rho({i}) \propto x_i,
%   \end{equation*}
%   avec $\sum_{i}\rho(i)=1$. 
% \end{frame}

% \begin{frame}{Exemple / exercice : retour sur Erenfest}
%   \begin{itemize}
%   \item Initiallement $N$ particules sont à gauche.
%   \item A chaque pas: une particule prise au hasard change de
%     compartiment.
%   \end{itemize}
  
%   \begin{center}
%     \includegraphics[width=.5\linewidth]{eren_2}
%   \end{center}
  
%   \begin{enumerate}
%   \item Décrire la chaîne de Markov. 
%   \item Trouver une fonction $\rho$ qui satisfait
%     $ \rho(i) P_{ij} = \rho(j) P_{ji} $
%   \item En déduire la mesure stationnaire de la chaîne. 
%   \end{enumerate}
% \end{frame}

% \begin{frame}{ : marche aléatoire sur un graphe}
%   On se donne un graph non-orienté quelconque.  On note $X_t$ la
%   position du marcheur après $n$ étapes. $X_{t+1}$ est choisit
%   uniformément parmi tous les voisins.
  
%   On note $d_i$ le degré du noeud $i$. 
%   \begin{enumerate}
%   \item Que vaut $P_{ij}$? 
%   \item Trouver une mesure $\rho$ telle que
%     $ \rho(i) P_{ij} = \rho(j) P_{ji} $
%   \item En déduire une mesure stationnaire pour un marcheur aléatoire
%     dans le graphe ci dessous: 
    
%     \begin{center}
%       \includegraphics[width=.4\linewidth]{graph}
%     \end{center}
    
%   \item On considère maintenant un cavalier qui se balade sur un
%     échiquier. Quelles cases visite-t-il le moins souvent? Le plus
%     souvent? 
%   \end{enumerate}
% \end{frame}

% *** Steady-state analysis 
% - def irreducibility
% - th 
% - def reversibility 


\end{document}
