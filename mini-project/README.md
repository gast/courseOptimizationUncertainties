# Projects 2020-2021

## List of possible subjects

### Optimal vaccination policies

We consider a discrete-time compartimental model (say SEIR). We want
to compute optimal vaccination strategies.
- Vaccination cost is non-linear (e.g. quadratic) in the number of
  vaccinated people. 
- Each infected people costs you something.
- There is an external source of infection or not. 

- Extensions or variants:
  - you may have two types of population. 
  - What about lockdown? 
  - compare vaccination for a deterministic model and a stochastic
    model
  - Vaccination on a graph
  
### Hospital admissions

You control N hospitals. Each hospital can host at most R "regular"
patients plus C patient in "critical condition". Patient arrive at a
each hospital in state "R" and can move with some probabilty to state
"critical" and/or die. 

Question: How to balance de load of patient between hospital (knowing
that if a hospital is full, any new critical patient will die). 

Exercice:
- model the problem (starting with e.g. 2 hospitals)
- implement an optimal policy
- find parameters to illustrate your findings. 

### Picomino

Picomino (https://www.gigamic.com/jeu/pickomino) is a small board game
with lots of randomness. Each turn, you have to throw some dices and
have to pick some.

Goal: compute an optimal strategy for this game (starting from a 1
player version). 

### Inventory problem 

Model an inventory problem with holding cost. Each day, there is
demand (random). At a given day, you can order more stock that will
arrive two days later. Ordering costs something (constant price +
price per item), there is a holding cost.

You want to maximize your average revenue. 

Exercise:
- model the problem
- Implement at least one way to solve the problem (LP formulation,
  policy iteration or value iteration). 
- Compute and illustrate the structure of the optimal policy with a
  few examples. 


### Bandit problem 

Implement a few bandit algorithms and study their regret
experimentally.

### Resource allocation and Whittle index policy 

Compare Whittle index policies and join the shortest queue.
